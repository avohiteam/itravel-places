<?php
if( is_page() || is_singular( 'portfolio' ) ){
    // Sidebar data for each page
    $current_page_id = get_queried_object_id();
    $sidebar_id = get_post_meta( $current_page_id, 'nfw_sidebar_source', true );
} else{
    // Sidebar data for the rest of the post types (blog sidebar)
    $page_for_posts = get_option( 'page_for_posts' );
    $sidebar_id = get_post_meta( $page_for_posts, 'nfw_sidebar_source', true );
    if($page_for_posts==0) {
        $sidebar_id='nfw-default-sidebar';
    }
}
if( !empty( $sidebar_id ) ){
    if( function_exists( 'dynamic_sidebar' ) ){
        echo '<div class="nfw-span3">';
        dynamic_sidebar( $sidebar_id );
        echo '</div>';
    }
}