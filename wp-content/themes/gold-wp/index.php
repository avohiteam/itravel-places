<?php
/*
 * The main template file
 */
get_header();

// Get Blog related header meta
$page_for_posts_id = get_option( 'page_for_posts' );
$page_header_toggle = get_post_meta( $page_for_posts_id, 'nfw_header_toggle', true );
if( $page_header_toggle == 'on' ):
    // Sets the Page header
    $page_header_title = get_post_meta( $page_for_posts_id, 'nfw_header_title', true );
    ?>
    <div id="page-header">

        <div class="nfw-row">
            <div class="nfw-span12">

                <h3><?php echo esc_html( $page_header_title ); ?></h3>

            </div>
        </div>

    </div>
    <?php
endif;
?> 

<div class="nfw-row">
    <?php
    // Gets blog page related meta for sidebar
    $page_for_posts = get_option( 'page_for_posts' );
    $sidebar_position = get_post_meta( $page_for_posts, 'nfw_sidebar_position', true );
    if( $page_for_posts == 0 ){
        $sidebar_position = 'right';
    }

    if( !empty( $sidebar_position ) ){
        if( $sidebar_position == 'left' ){
            get_sidebar();
        }
    }
    if( !empty( $sidebar_position ) && $sidebar_position != 'none' ){
        // There is a sidebar, 
        if( have_posts() ) :
            ?>
            <div class="nfw-span9">   
                <?php
                while( have_posts() ) : the_post();
                    get_template_part( 'content', get_post_format() );
                endwhile;
                if( function_exists( "nfw_pagination" ) ){
                    nfw_pagination();
                }
                ?>
            </div>

        <?php else :
            ?>
            <div class="nfw-span9">
                <?php
                // If no content, include the "No posts found" template.
                get_template_part( 'content', 'none' );
                ?>
            </div>
        <?php
        endif;
    } else{
        // There is no sidebar, blog posts will be in 3 columns
        if( have_posts() ) :
            ?> 
            <div class="nfw-span12">   
                <?php
                while( have_posts() ) : the_post();
                    get_template_part( 'content', get_post_format() );
                endwhile;

                if( function_exists( "nfw_pagination" ) ){
                    nfw_pagination();
                }
                ?>
            </div>

            <?php
        else :
            ?>
            <div class="nfw-span12">
                <?php
                // If no content, include the "No posts found" template.
                get_template_part( 'content', 'none' );
                ?>
            </div>
        <?php
        endif;
    }

    if( !empty( $sidebar_position ) ){
        if( $sidebar_position == 'right' ){
            get_sidebar();
        }
    }
    ?>
</div> 
<?php
get_footer();
