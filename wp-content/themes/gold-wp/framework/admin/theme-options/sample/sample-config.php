<?php

/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */
if ( !class_exists( 'Redux' ) ) {
    return;
}


// This is your option name where all the Redux data is stored.
$opt_name = "nfw_theme_options";


/*
 *
 * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
 *
 */

$sampleHTML = '';
if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
    Redux_Functions::initWpFilesystem();

    global $wp_filesystem;

    $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
}

// Background Patterns Reader
$sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
$sample_patterns_url = ReduxFramework::$_url . '../sample/patterns/';
$sample_patterns = array();

if ( is_dir( $sample_patterns_path ) ) {

    if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
        $sample_patterns = array();

        while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

            if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                $name = explode( '.', $sample_patterns_file );
                $name = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                $sample_patterns[] = array(
                    'alt' => $name,
                    'img' => $sample_patterns_url . $sample_patterns_file
                );
            }
        }
    }
}

/*
 *
 * --> Action hook examples
 *
 */

// If Redux is running as a plugin, this will remove the demo notice and links
//add_action( 'redux/loaded', 'remove_demo' );
// Function to test the compiler hook and demo CSS output.
// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
//add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);
// Change the arguments after they've been declared, but before the panel is created
//add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );
// Change the default value of a field after it's been set, but before it's been useds
//add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );
// Dynamically add a section. Can be also used to modify sections/fields
//add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');


/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */
$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name' => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name' => $theme->get( 'Name' ),
    // Name that appears at the top of your panel
    'display_version' => $theme->get( 'Version' ),
    // Version that appears at the top of your panel
    'menu_type' => 'menu',
    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu' => true,
    // Show the sections below the admin menu item or not
    'menu_title' => __( 'Theme Options', 'gold_domain' ),
    'page_title' => __( 'Theme Options', 'gold_domain' ),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key' => '',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography' => true,
    // Use a asynchronous font on the front end or font string
    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
    'admin_bar' => true,
    // Show the panel pages on the admin bar
    'admin_bar_icon' => 'dashicons-portfolio',
    // Choose an icon for the admin bar menu
    'admin_bar_priority' => 50,
    // Choose an priority for the admin bar menu
    'global_variable' => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode' => false,
    // Show the time the page took to load, etc
    'update_notice' => false,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer' => true,
    // Enable basic customizer support
    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field
    // OPTIONAL -> Give you extra features
    'page_priority' => 57,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent' => 'themes.php',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions' => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon' => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_admin_icon.png',
    // Specify a custom URL to an icon
    'last_tab' => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon' => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug' => '',
    // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
    'save_defaults' => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show' => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark' => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export' => true,
    // Shows the Import/Export panel when not used as a field.
    // CAREFUL -> These options are for advanced use only
    'transient_time' => 60 * MINUTE_IN_SECONDS,
    'output' => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag' => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database' => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
    'system_info' => false,
    // REMOVE
    //'compiler'             => true,
    // HINTS
    'hints' => array(
        'icon' => 'el el-question-sign',
        'icon_position' => 'right',
        'icon_color' => 'lightgray',
        'icon_size' => 'normal',
        'tip_style' => array(
            'color' => 'red',
            'shadow' => true,
            'rounded' => false,
            'style' => '',
        ),
        'tip_position' => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect' => array(
            'show' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'mouseover',
            ),
            'hide' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'click mouseleave',
            ),
        ),
    )
);

Redux::setArgs( $opt_name, $args );

/*
 * ---> END ARGUMENTS
 */


/*
 * ---> START HELP TABS
 */

$tabs = array(
    array(
        'id' => 'redux-help-tab-1',
        'title' => __( 'Theme Information 1', 'gold_domain' ),
        'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'gold_domain' )
    ),
    array(
        'id' => 'redux-help-tab-2',
        'title' => __( 'Theme Information 2', 'gold_domain' ),
        'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'gold_domain' )
    )
);
Redux::setHelpTab( $opt_name, $tabs );

// Set the help sidebar
$content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'gold_domain' );
Redux::setHelpSidebar( $opt_name, $content );


/*
 * <--- END HELP TABS
 */


/*
 *
 * ---> START SECTIONS
 *
 */

/*

  As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


 */

// -> START Basic Fields
$page_ids = get_all_page_ids();
$pages_array = array();
$pages_array['default'] = 'Default 404';
foreach ( $page_ids as $page_id ) {
    // do not include the Auto Draft pages
    if ( get_the_title( $page_id ) != 'Auto Draft' ) {
        $pages_array[$page_id] = esc_html( get_the_title( $page_id ) );
    }
}
Redux::setSection( $opt_name, array(
    'icon' => 'el-icon-cogs',
    'title' => __( 'General', 'gold_domain' ),
    'subsection' => false,
    'fields' => array(
        array(
            'id' => 'nfw-favicon-selector',
            'type' => 'media',
            'url' => true,
            'title' => __( 'FavIcon Selection', 'gold_domain' ),
            'desc' => __( 'Upload a new icon image', 'gold_domain' ),
            'subtitle' => __( 'Replace or remove the existing icon', 'gold_domain' ),
            'default' => array(
                'url' => get_template_directory_uri() . '/layout/images/default/favicon.png')
        ),
        array(
            'id' => 'nfw-appleicon-selector',
            'type' => 'media',
            'url' => true,
            'title' => __( 'Apple Icon Selection', 'gold_domain' ),
            'desc' => __( 'Upload a new icon image', 'gold_domain' ),
            'subtitle' => __( 'Replace or remove the existing icon', 'gold_domain' ),
            'default' => array(
                'url' => get_template_directory_uri() . '/layout/images/default/apple-touch-icon-144-precomposed.png')
        ),
        array(
            'id' => 'nfw-404-selection',
            'type' => 'select',
            'title' => __( '404 Page Selection', 'gold_domain' ),
            'subtitle' => __( 'Can replace the page displayed when error 404 occurs', 'gold_domain' ),
            'desc' => __( 'Chose a page for 404 error', 'gold_domain' ),
            'options' => $pages_array,
            'default' => 'default',
        ),
        array(
            'id' => 'nfw-custom-css',
            'type' => 'textarea',
            'title' => __( 'Custom CSS', 'gold_domain' ),
            'subtitle' => __( 'Only CSS code allowed', 'gold_domain' ),
            'desc' => __( 'This field is CSS validated.', 'gold_domain' ),
            'validate' => 'css',
        ),
        array(
            'id' => 'nfw-custom-analytics',
            'type' => 'textarea',
            'title' => __( 'Analytics', 'gold_domain' ),
            'subtitle' => __( 'Field for Analytics Javascript code', 'gold_domain' ),
            'desc' => __( 'Insert the Javascript code.', 'gold_domain' ),
        //'validate' => 'js',
        )
    )
) );

Redux::setSection( $opt_name, array(
    'icon' => 'el-icon-home',
    'title' => __( 'Header', 'gold_domain' ),
    'subsection' => false,
    'fields' => array(
        array(
            'id' => 'nfw-header-logo',
            'type' => 'media',
            'url' => true,
            'title' => __( 'Header Logo Selection', 'gold_domain' ),
            'desc' => __( 'Upload a new logo image', 'gold_domain' ),
            'subtitle' => __( 'Replace or remove the existing logo', 'gold_domain' ),
            'default' => array(
                'url' => get_template_directory_uri() . '/layout/images/logo.png')
        ),
        array(
            'id' => 'nfw-header-search-toggle',
            'type' => 'button_set',
            'title' => __( 'Header Search ON/OFF', 'gold_domain' ),
            'subtitle' => __( 'Enable/disable search for header', 'gold_domain' ),
            'desc' => __( 'Toggle ON or OFF', 'gold_domain' ),
            'options' => array(
                '1' => 'ON',
                '2' => 'OFF'
            ),
            'default' => '1'
        ),
        array(
            'id' => 'nfw-header-search-style',
            'type' => 'button_set',
            'title' => __( 'Header Search Style', 'gold_domain' ),
            'subtitle' => __( 'Chose a style', 'gold_domain' ),
            'desc' => __( 'Search box will appear in a centered popup or near the menu', 'gold_domain' ),
            'options' => array(
                '1' => 'Popup search box',
                '2' => 'Small search box'
            ),
            'default' => '1'
        )
    )
) );
Redux::setSection( $opt_name, array(
    'icon' => 'el-icon-credit-card',
    'title' => __( 'Footer', 'gold_domain' ),
    'subsection' => false,
    'fields' => array(
        array(
            'id' => 'nfw-footer-top-layout',
            'type' => 'image_select',
            'title' => __( 'Footer Top Layout', 'gold_domain' ),
            'subtitle' => __( 'Select the number of widget areas', 'gold_domain' ),
            'options' => array(
                '1' => array(
                    'alt' => '1 Area',
                    'img' => get_template_directory_uri() . '/framework/admin/images/icons/l1.png'
                ),
                '2' => array(
                    'alt' => '2 Areas',
                    'img' => get_template_directory_uri() . '/framework/admin/images/icons/l2.png'
                ),
                '3' => array(
                    'alt' => '3 Areas',
                    'img' => get_template_directory_uri() . '/framework/admin/images/icons/l3.png'
                ),
                '4' => array(
                    'alt' => '4 Areas',
                    'img' => get_template_directory_uri() . '/framework/admin/images/icons/l4.png'
                )
            ),
            'default' => '4'
        ),
        array(
            'id' => 'nfw-footer-middle-switch',
            'type' => 'button_set',
            'title' => __( 'Footer Middle ON/OFF', 'gold_domain' ),
            'subtitle' => __( 'Enable or disable footer middle area', 'gold_domain' ),
            'desc' => __( 'Enable or disable footer middle area', 'gold_domain' ),
            'options' => array(
                '1' => 'On',
                '2' => 'OFF'
            ),
            'default' => '1'
        ),
        array(
            'id' => 'nfw-footer-middle-layout',
            'type' => 'image_select',
            'title' => __( 'Footer Middle Layout', 'gold_domain' ),
            'subtitle' => __( 'Select the number of widget areas', 'gold_domain' ),
            'options' => array(
                '1' => array(
                    'alt' => '1 Area',
                    'img' => get_template_directory_uri() . '/framework/admin/images/icons/l1.png'
                ),
                '2' => array(
                    'alt' => '2 Areas',
                    'img' => get_template_directory_uri() . '/framework/admin/images/icons/l2.png'
                ),
                '3' => array(
                    'alt' => '3 Areas',
                    'img' => get_template_directory_uri() . '/framework/admin/images/icons/l3.png'
                ),
                '4' => array(
                    'alt' => '4 Areas',
                    'img' => get_template_directory_uri() . '/framework/admin/images/icons/l4.png'
                )
            ),
            'default' => '4'
        ),
        array(
            'id' => 'nfw-footer-bottom-layout',
            'type' => 'image_select',
            'title' => __( 'Footer Bottom Layout', 'gold_domain' ),
            'subtitle' => __( 'Select the number of widget areas', 'gold_domain' ),
            'options' => array(
                '1' => array(
                    'alt' => '1 Area',
                    'img' => get_template_directory_uri() . '/framework/admin/images/icons/l1.png'
                ),
                '2' => array(
                    'alt' => '2 Areas',
                    'img' => get_template_directory_uri() . '/framework/admin/images/icons/l2.png'
                ),
                '3' => array(
                    'alt' => '3 Areas',
                    'img' => get_template_directory_uri() . '/framework/admin/images/icons/l3.png'
                ),
                '4' => array(
                    'alt' => '4 Areas',
                    'img' => get_template_directory_uri() . '/framework/admin/images/icons/l4.png'
                )
            ),
            'default' => '1'
        )
    )
) );

Redux::setSection( $opt_name, array(
    'icon' => 'el-icon-list',
    'title' => __( 'Color Schemes', 'gold_domain' ),
    'subsection' => false,
    'fields' => array(
        array(
            'id' => 'nfw-accent-colors-enable',
            'type' => 'button_set',
            'title' => __( 'Enable/Disable color scheme', 'gold_domain' ),
            'desc' => __( 'If it is disabled, then the default color scheme will be used.', 'gold_domain' ),
            'options' => array(
                '1' => 'ON',
                '2' => 'OFF'
            ),
            'default' => '1'
        ),
        array(
            'id' => 'nfw-accent-color-scheme-default',
            'type' => 'color',
            'title' => __( 'Color Default', 'gold_domain' ),
            'subtitle' => __( 'Pick a color for the theme menu', 'gold_domain' ),
            'default' => '#80a852',
            'transparent' => false,
            'validate' => 'color',
        ),
        array(
            'id' => 'nfw-accent-color-scheme-2',
            'type' => 'color',
            'title' => __( 'Color 2', 'gold_domain' ),
            'subtitle' => __( 'Pick a color for the theme menu', 'gold_domain' ),
            'default' => '#b1a843',
            'transparent' => false,
            'validate' => 'color',
        ),
        array(
            'id' => 'nfw-accent-color-scheme-3',
            'type' => 'color',
            'title' => __( 'Color 3', 'gold_domain' ),
            'subtitle' => __( 'Pick a color for the theme menu', 'gold_domain' ),
            'default' => '#e18f5f',
            'transparent' => false,
            'validate' => 'color',
        ),
        array(
            'id' => 'nfw-accent-color-scheme-4',
            'type' => 'color',
            'title' => __( 'Color 4', 'gold_domain' ),
            'subtitle' => __( 'Pick a color for the theme menu', 'gold_domain' ),
            'default' => '#f3e686',
            'transparent' => false,
            'validate' => 'color',
        )
    )
) );

Redux::setSection( $opt_name, array(
    'icon' => 'el-icon-website',
    'title' => __( 'Sidebars', 'gold_domain' ),
    'subsection' => false,
    'fields' => array(
        array(
            'id' => 'nfw-sidebars-custom',
            'type' => 'custom_sidebars',
            'title' => __( 'Add/remove multiple sidebars', 'gold_domain' ),
            'validate' => 'no_html',
            'subtitle' => __( 'Add/edit sidebars', 'gold_domain' ),
            'desc' => __( 'No html allowed.', 'gold_domain' )
        )
    )
) );

/*
 * <--- END SECTIONS
 */

/**
 * This is a test function that will let you see when the compiler hook occurs.
 * It only runs if a field    set with compiler=>true is changed.
 * */
function compiler_action ( $options, $css, $changed_values ) {
    echo '<h1>The compiler hook has run!</h1>';
    echo "<pre>";
    print_r( $changed_values ); // Values that have changed since the last save
    echo "</pre>";
    //print_r($options); //Option values
    //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
}

/**
 * Custom function for the callback validation referenced above
 * */
if ( !function_exists( 'redux_validate_callback_function' ) ) {

    function redux_validate_callback_function ( $field, $value, $existing_value ) {
        $error = false;
        $warning = false;

        //do your validation
        if ( $value == 1 ) {
            $error = true;
            $value = $existing_value;
        } elseif ( $value == 2 ) {
            $warning = true;
            $value = $existing_value;
        }

        $return['value'] = $value;

        if ( $error == true ) {
            $return['error'] = $field;
            $field['msg'] = 'your custom error message';
        }

        if ( $warning == true ) {
            $return['warning'] = $field;
            $field['msg'] = 'your custom warning message';
        }

        return $return;
    }

}

/**
 * Custom function for the callback referenced above
 */
if ( !function_exists( 'redux_my_custom_field' ) ) {

    function redux_my_custom_field ( $field, $value ) {
        print_r( $field );
        echo '<br/>';
        print_r( $value );
    }

}

/**
 * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
 * so you must use get_template_directory_uri() if you want to use any of the built in icons
 * */
function dynamic_section ( $sections ) {
    //$sections = array();
    $sections[] = array(
        'title' => __( 'Section via hook', 'gold_domain' ),
        'desc' => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'gold_domain' ),
        'icon' => 'el el-paper-clip',
        // Leave this as a blank section, no options just some intro text set above.
        'fields' => array()
    );

    return $sections;
}

/**
 * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
 * */
function change_arguments ( $args ) {
    //$args['dev_mode'] = true;

    return $args;
}

/**
 * Filter hook for filtering the default value of any given field. Very useful in development mode.
 * */
function change_defaults ( $defaults ) {
    $defaults['str_replace'] = 'Testing filter hook!';

    return $defaults;
}

// Remove the demo link and the notice of integrated demo from the redux-framework plugin
function remove_demo () {

    // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
    if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
        remove_filter( 'plugin_row_meta', array(
            ReduxFrameworkPlugin::instance(),
            'plugin_metalinks'
                ), null, 2 );

        // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
        remove_action( 'admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices') );
    }
}
