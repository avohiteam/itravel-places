<?php

add_action( "wp_ajax_nfw_custom_posts_isotope", "nfw_custom_posts_isotope" );
add_action( "wp_ajax_nopriv_nfw_custom_posts_isotope", "nfw_custom_posts_isotope" );

// Provides the response results for load more grid type portfolio
function nfw_custom_posts_isotope(){

    /*
     * Verifies if the ajax call was made fom the portfolio load more button, 
     * based on the WordPress generated nonce of the request
     */
    if( !wp_verify_nonce( $_REQUEST['nonce'], "nfw_custom_posts_isotope_nonce" ) ){
        exit( "Access Denied!!!!" );
    }

    $custom_post_type = $_REQUEST["custom_post_type"];
    $count_total = $_REQUEST["count_total"];

    $args = array(
        'post_type' => $custom_post_type,
        'post_status' => 'publish',
        'posts_per_page' => $count_total,
        'ignore_sticky_posts' => 1);

    $loop = new WP_Query( $args );

    // get portfolio categories array
    $terms = get_terms( 'portfolio_categories' );

    $output = "";

    $count = 0;
    $categories_index = array();
    $post_categories = array();

    // Counts and records the categories
    if( $terms && !is_wp_error( $terms ) ){
        foreach( $terms as $term ){
            $count++;
            $categories_index[$term->name] = $count;
        }
    }

    $item_count = 0;
    while( $loop->have_posts() ){

        $terms_class = "item";
        $loop->the_post();
        $post_id = get_the_ID();

        $post_categories = get_the_terms( $post_id, 'portfolio_categories' );
        $thumbnail_id = get_post_thumbnail_id();

        // Get custom post feature image link, use different image sizes for the asymmetric portfolio
        switch( $item_count ){
            case 0:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_470_480' );
                $item_count = 1;
                break;
            case 1:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_225' );
                $item_count = 2;
                break;
            case 2:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_370_480' );
                $item_count = 3;
                break;
            case 3:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_225' );
                $item_count = 4;
                break;
            case 4:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_570_225' );
                $item_count = 5;
                break;
            case 5:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_480' );
                $item_count = 6;
                break;
            case 6:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_225' );
                $item_count = 7;
                break;
            case 7:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_225' );
                $item_count = 8;
                break;
            case 8:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_225' );
                $item_count = 9;
                break;
            case 9:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_225' );
                $item_count = 0;
                break;
        }
        $image_url = esc_url( $image_data[0] );

        $post_title = esc_html( get_the_title( $post_id ) );

        $post_link = esc_url( get_post_permalink( $post_id ) );

        $post_date = esc_html( get_the_time( get_option( 'date_format' ) ) );

        if( !empty( $post_categories ) ){
            foreach( $post_categories as $category ){
                $terms_class.=" term-" . esc_attr( $categories_index[$category->name] );
            }
        }
        $view_project = __( 'View Project', 'gold_domain' );
        // Composes the html data for the current loop item and appends it to the output
        $output.="<li class='{$terms_class}'>
                        	
                            <div class='portfolio-item'>
            
                                <div class='portfolio-item-preview'>			
                                
                                    <img src='{$image_url}' alt=''>
                                    
                                    <div class='portfolio-item-overlay'>
                                    
                                        <div class='portfolio-item-overlay-actions'>
                                            
                                            <a class='btn' href='{$post_link}'>{$view_project}</a>
                                            
                                        </div><!-- end .portfolio-item-overlay-actions -->                                                                
                                        
                                        <div class='portfolio-item-description'>
                                
                                            <h4>{$post_title}</h4>
                                            <p>{$post_date}</p>
                                        
                                        </div><!-- end .portfolio-item-description -->
                                        
                                    </div><!-- end .portfolio-item-overlay -->
                                
                                </div><!-- end .portfolio-item-preview -->                                            
                            
                            </div><!-- end .portfolio-item -->
                            
                        </li>";
    }

    wp_reset_query();

    $result['list_elements'] = $output;

    // Verifies the HTTP request
    if( !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' ){
        // Encodes and provides the response output
        $result = json_encode( $result );
        echo $result;
    } else{
        header( "Location: " . $_SERVER["HTTP_REFERER"] );
    }

    die();
}

add_action( "wp_ajax_nfw_posts_portfolio_classic", "nfw_posts_portfolio_classic" );
add_action( 'wp_ajax_nopriv_nfw_posts_portfolio_classic', 'nfw_posts_portfolio_classic' );

// Provides the response results for load more classic portfolio with pagination
function nfw_posts_portfolio_classic(){

    /*
     * Verifies if the ajax call was made fom the portfolio load more button, 
     * based on the wordpress generated nonce of the request
     */
    if( !wp_verify_nonce( $_REQUEST['nonce'], "nfw_posts_portfolio_classic_nonce" ) ){
        exit( "Access Denied!!!!" );
    }

    $custom_post_type = $_REQUEST["custom_post_type"];
    $count_page = $_REQUEST["count_page"];
    $count_increment = $_REQUEST["count_increment"];

    $count_total = $count_increment * $count_page;

    $args = array(
        'post_type' => $custom_post_type,
        'post_status' => 'publish',
        'posts_per_page' => $count_total,
        'ignore_sticky_posts' => 1);

    $loop = new WP_Query( $args );

    $output = "";
    // Determines the elements for the current page
    $posts_count = 0;
    $posts_start = $count_page - 1;
    $posts_start = $posts_start * $count_increment;

    while( $loop->have_posts() ){

        $loop->the_post();

        if( $posts_count >= $posts_start ){
            $post_id = get_the_ID();
            $image_data = wp_get_attachment_image_src( get_post_thumbnail_id(), 'nfw_image_370_380' );
            $image_url = esc_url( $image_data[0] );

            $post_title = esc_html( get_the_title( $post_id ) );

            $post_link = esc_url( get_post_permalink( $post_id ) );

            $post_content = apply_filters( "the_content", get_the_excerpt() );

            $post_date = esc_html( get_the_time( get_option( 'date_format' ) ) );

            $post_categories = get_the_terms( $post_id, 'portfolio_categories' );
            $categories = '';
            if( !empty( $post_categories ) ){
                foreach( $post_categories as $category ){
                    $categories.=' ' . esc_html( $category->name );
                }
            }
            // Composes the html data for the current loop item and appends it to the output
            $output.="<div class='portfolio-item'>
                
                <div class='nfw-row'>
                    <div class='nfw-span4'>
                        
                        <div class='portfolio-item-preview'>
                            
                            <p>
                            	<img src='{$image_url}' alt=''>
                            </p>
                            
                        </div><!-- end .portfolio-item-preview -->
                        
                    </div><!-- end .span4 -->
                    <div class='nfw-span8'>
                        
                      <div class='portfolio-item-description'>
                            
                            <h4>
                                <a href='{$post_link}'>{$post_title}</a>
                            </h4>
                            
                            <p>
                                <span>" . __( 'Date:', 'gold_domain' ) . "</span> {$post_date}<br>
                                <span>" . __( 'Category:', 'gold_domain' ) . "</span>{$categories}
                            </p>
                            
                            {$post_content}
                            
                            <a class='btn' href='{$post_link}'>" . __( 'Read More', 'gold_domain' ) . "</a>
                            
                        </div><!-- end .portfolio-item-description -->
                        
                    </div><!-- end .span8 -->
                </div><!-- end .row -->
                
            </div>";
        } else{
            $posts_count++;
        }
    }

    wp_reset_query();

    $result['list_elements'] = $output;

    // Verifies the HTTP request
    if( !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' ){
        // Encodes and provides the response output
        $result = json_encode( $result );
        echo $result;
    } else{
        header( "Location: " . $_SERVER["HTTP_REFERER"] );
    }

    die();
}

add_action( "wp_ajax_nfw_posts_portfolio_symmetric", "nfw_posts_portfolio_symmetric" );
add_action( 'wp_ajax_nopriv_nfw_posts_portfolio_symmetric', 'nfw_posts_portfolio_symmetric' );

// Provides the response results for load more symmetric portfolio with pagination
function nfw_posts_portfolio_symmetric(){

    /*
     * Verifies if the ajax call was made fom the portfolio load more button, 
     * based on the wordpress generated nonce of the request
     */
    if( !wp_verify_nonce( $_REQUEST['nonce'], "nfw_posts_portfolio_symmetric_nonce" ) ){
        exit( "Access Denied!!!!" );
    }

    $custom_post_type = $_REQUEST["custom_post_type"];
    $count_page = $_REQUEST["count_page"];
    $count_increment = $_REQUEST["count_increment"];

    $count_total = $count_increment * $count_page;

    $args = array(
        'post_type' => $custom_post_type,
        'post_status' => 'publish',
        'posts_per_page' => $count_total,
        'ignore_sticky_posts' => 1);

    $loop = new WP_Query( $args );

    $output = "";
    // Determines the elements for the current page
    $posts_count = 0;
    $posts_start = $count_page - 1;
    $posts_start = $posts_start * $count_increment;

    while( $loop->have_posts() ){

        $loop->the_post();

        if( $posts_count >= $posts_start ){
            $post_id = get_the_ID();

            $image_data = wp_get_attachment_image_src( get_post_thumbnail_id(), 'nfw_image_370_380' );
            $image_url = esc_url( $image_data[0] );

            $post_title = esc_html( get_the_title( $post_id ) );

            $post_link = esc_url( get_post_permalink( $post_id ) );

            $post_date = esc_html( get_the_time( get_option( 'date_format' ) ) );

            // Composes the html data for the current loop item and appends it to the output
            $output.="<div class='nfw-span4'>
                    <div class='portfolio-item'>
            
                        <div class='portfolio-item-preview'>			
                        
                            <img src='{$image_url}' alt=''>
                            
                            <div class='portfolio-item-overlay'>
                            
                                <div class='portfolio-item-overlay-actions'>
                                    
                                    <a class='btn' href='{$post_link}'>" . __( 'View Project', 'gold_domain' ) . "</a>
                                    
                                </div><!-- end .portfolio-item-overlay-actions -->                                                                
                                
                                <div class='portfolio-item-description'>
                        
                                    <h4>{$post_title}</h4>
                                    <p>{$post_date}</p>
                                
                                </div><!-- end .portfolio-item-description -->
                                
                            </div><!-- end .portfolio-item-overlay -->
                        
                        </div><!-- end .portfolio-item-preview -->                                            
                    
                    </div><!-- end .portfolio-item -->
                    </div>";
        } else{
            $posts_count++;
        }
    }

    wp_reset_query();

    $result['list_elements'] = $output;

    // Verifies the HTTP request
    if( !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' ){
        // Encodes and provides the response output
        $result = json_encode( $result );
        echo $result;
    } else{
        header( "Location: " . $_SERVER["HTTP_REFERER"] );
    }

    die();
}
