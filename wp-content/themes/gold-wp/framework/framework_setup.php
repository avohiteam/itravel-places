<?php



// Compatiblity

function nfw_compatible_headers( $headers ){

    if( isset( $_SERVER['HTTP_USER_AGENT'] ) &&

            (strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE' ) !== false) ){

        $headers['X-UA-Compatible'] = 'IE=edge,chrome=1,requiresActiveX=true';

    }

    return $headers;

}



// ENQUEUE SCRIPTS AND STYLES

// CSS and JS for admin panels

function nfw_admin_scripts_and_styles(){

    if( is_admin() ){

        wp_enqueue_media();

        wp_register_script( 'nfw-icons-handle', get_template_directory_uri() . '/framework/admin/js/vc_admin_functions.js' );

        wp_enqueue_script( 'nfw-icons-handle' );



        wp_register_style( 'nfw-font-awesome-admin', get_template_directory_uri() . '/layout/css/fontawesome/font-awesome.min.css' );

        wp_enqueue_style( 'nfw-font-awesome-admin' );



        wp_register_style( 'nfw-admin-styles', get_template_directory_uri() . '/framework/admin/css/admin_styles.css' );

        wp_enqueue_style( 'nfw-admin-styles' );



        wp_register_style( 'nfw-iconfontcustom-css', get_template_directory_uri() . '/layout/css/iconfontcustom/icon-font-custom.css' );

        wp_enqueue_style( 'nfw-iconfontcustom-css' );



        wp_enqueue_script( 'wp-link' );

    }

}



add_action( 'admin_enqueue_scripts', 'nfw_admin_scripts_and_styles' );



// CSS and JS files for the site

function nfw_front_styles_scripts(){



    wp_register_style( 'nfw_main_style_css', get_template_directory_uri() . '/style.css' );

    wp_register_style( 'nfw_font_awesome', get_template_directory_uri() . '/layout/css/fontawesome/font-awesome.min.css' );



    wp_enqueue_style( 'nfw_font_awesome' );

    wp_enqueue_style( 'nfw_main_style_css' );



    wp_register_style( 'nfw_base_css', get_template_directory_uri() . '/layout/css/base.css' );

    wp_register_style( 'nfw_grid_css', get_template_directory_uri() . '/layout/css/grid.css' );

    wp_register_style( 'nfw_elements_css', get_template_directory_uri() . '/layout/css/elements.css' );

    wp_register_style( 'nfw_layout_css', get_template_directory_uri() . '/layout/css/layout.css' );

    wp_register_style( 'nfw_iconfontcustom_css', get_template_directory_uri() . '/layout/css/iconfontcustom/icon-font-custom.css' );

    wp_register_style( 'nfw_magnific_popup_css', get_template_directory_uri() . '/layout/js/magnificpopup/magnific-popup.css' );

    wp_register_style( 'my_css', get_template_directory_uri() . '/my_css.css' );

    wp_enqueue_style( 'nfw_base_css' );

    wp_enqueue_style( 'nfw_grid_css' );

    wp_enqueue_style( 'nfw_elements_css' );

    wp_enqueue_style( 'nfw_iconfontcustom_css' );

    wp_enqueue_style( 'nfw_layout_css' );

    wp_enqueue_style( 'nfw_magnific_popup_css' );

    wp_enqueue_style( 'my_css' );


    wp_enqueue_script( 'jquery' );



    // Register scripts   

    // EasyPieChart

    wp_register_script( 'nfw_easypiechart_js', get_template_directory_uri() . '/layout/js/easypiechart/jquery.easypiechart.min.js', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_easypiechart_js' );



    // Easytabs

    wp_register_script( 'nfw_easytabs_js', get_template_directory_uri() . '/layout/js/easytabs/jquery.easytabs.min.js', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_easytabs_js' );



    // gMap 

    wp_register_script( 'nfw_gmap_sensor_js', '//maps.google.com/maps/api/js?sensor=false', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_gmap_sensor_js' );

    wp_register_script( 'nfw_gmap_js', get_template_directory_uri() . '/layout/js/gmap/jquery.gmap.min.js', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_gmap_js' );



    // Isotope

    wp_register_script( 'nfw_imagesloaded_js', get_template_directory_uri() . '/layout/js/isotope/imagesloaded.pkgd.min.js', array('jquery'), false, true );

    wp_register_script( 'nfw_isotope_min_js', get_template_directory_uri() . '/layout/js/isotope/isotope.pkgd.min.js', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_imagesloaded_js' );

    wp_enqueue_script( 'nfw_isotope_min_js' );



    // Magnific Popup

    wp_register_script( 'nfw_magnific_popup_js', get_template_directory_uri() . '/layout/js/magnificpopup/jquery.magnific-popup.min.js', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_magnific_popup_js' );



    // Parallax

    wp_register_script( 'nfw_parallax_js', get_template_directory_uri() . '/layout/js/parallax/jquery.parallax.min.js', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_parallax_js' );



    // Simpleplaceholder

    wp_register_script( 'nfw_simpleplaceholder_js', get_template_directory_uri() . '/layout/js/simpleplaceholder/jquery.simpleplaceholder.js', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_simpleplaceholder_js' );



    // Superfish Menu

    wp_register_script( 'nfw_hoverintent_js', get_template_directory_uri() . '/layout/js/superfish/hoverIntent.js', array('jquery'), false, true );

    wp_register_script( 'nfw_superfish_js', get_template_directory_uri() . '/layout/js/superfish/superfish.js', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_hoverintent_js' );

    wp_enqueue_script( 'nfw_superfish_js' );



    // Twitter

    wp_register_script( 'nfw_twitterfetcher_js', get_template_directory_uri() . '/layout/js/twitter/twitterfetcher.js' );

    wp_enqueue_script( 'nfw_twitterfetcher_js' );



    // ViewPort

    wp_register_script( 'nfw_viewport_js', get_template_directory_uri() . '/layout/js/viewport/jquery.viewport.js', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_viewport_js' );



    // Waypoints

    wp_register_script( 'nfw_waypoints_sticky_js', get_template_directory_uri() . '/layout/js/waypoints/waypoints-sticky.min.js', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_waypoints_sticky_js' );

    wp_register_script( 'nfw_waypoints_js', get_template_directory_uri() . '/layout/js/waypoints/waypoints.min.js', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_waypoints_js' );



    // custom scripts

    wp_register_script( 'nfw_custom_plugins_js', get_template_directory_uri() . '/layout/js/plugins.js', array('jquery'), false, true );

    wp_register_script( 'nfw_custom_scripts_js', get_template_directory_uri() . '/layout/js/scripts.js', array('jquery'), false, true );

    wp_enqueue_script( 'nfw_custom_plugins_js' );

    wp_enqueue_script( 'nfw_custom_scripts_js' );



    // scripts for ajax calls

    wp_register_script( 'ajax_calls_script', get_template_directory_uri() . '/layout/js/ajax_calls.js', array('jquery'), false, true );

    wp_localize_script( 'ajax_calls_script', 'nfw_ajax', array('ajaxurl' => admin_url( 'admin-ajax.php' )) );

    wp_enqueue_script( 'ajax_calls_script' );



    if( is_singular() && comments_open() && get_option( 'thread_comments' ) ){

        wp_enqueue_script( 'comment-reply' );

    }



    wp_enqueue_script( 'nfw_fitvids_js', get_template_directory_uri() . '/layout/js/jquery.fitvids.js', array('jquery'), false, true );

    

    // Google Fonts

    wp_register_style( 'nfw_googleFonts_OpenSans', '//fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic' );

    wp_enqueue_style( 'nfw_googleFonts_OpenSans' );

    wp_register_style( 'nfw_googleFonts_Dosis', '//fonts.googleapis.com/css?family=Dosis:200,400,300' );

    wp_enqueue_style( 'nfw_googleFonts_Dosis' );

}



if( !is_admin() ){

    add_action( 'wp_enqueue_scripts', 'nfw_front_styles_scripts' );

}



// Set up the content width value based on the theme's design.

if( !isset( $content_width ) ){

    $content_width = 1170;

}



if( !function_exists( 'nfw_setup' ) ){



    function nfw_setup(){



        // translation file

        // Localization Support

        load_theme_textdomain( 'gold_domain', get_template_directory() . '/languages' );



        $locale = get_locale();

        $locale_file = get_template_directory() . "/languages/$locale.php";

        if( is_readable( $locale_file ) ){

            require_once($locale_file);

        }

        // Add RSS feed links to <head> for posts and comments.

        add_theme_support( 'automatic-feed-links' );



        add_theme_support( 'title-tag' );

        // Enable support for Post Thumbnails...

        add_theme_support( 'post-thumbnails' );

        set_post_thumbnail_size( 1170, 578 );



        // Crops the uploaded images to certain sizes used for different components and elements

        if( function_exists( 'add_image_size' ) ){

            add_image_size( 'nfw_image_70_70', 70, 70, true );

            add_image_size( 'nfw_image_100_100', 100, 100, true );

            add_image_size( 'nfw_image_135_135', 135, 135, true );

            add_image_size( 'nfw_image_140_130', 140, 130, true );

            add_image_size( 'nfw_image_200_200', 200, 200, true );

            add_image_size( 'nfw_image_470_480', 470, 480, true );

            add_image_size( 'nfw_image_270_225', 270, 225, true );

            add_image_size( 'nfw_image_370_480', 370, 480, true );

            add_image_size( 'nfw_image_570_225', 570, 225, true );

            add_image_size( 'nfw_image_270_480', 270, 480, true );

            add_image_size( 'nfw_image_370_380', 370, 380, true );

            add_image_size( 'nfw_image_870_430', 870, 380, true );

        }



        add_theme_support( 'menus' );

        register_nav_menu( 'nfw-gold-menu', __( 'Gold Menu', 'gold_domain' ) );



        /*

         * Switch default core markup for search form, comment form, and comments

         * to output valid HTML5.

         */

        add_theme_support( 'html5', array(

            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'

        ) );



        // Allows to link a custom stylesheet file to the TinyMCE visual editor

        add_editor_style();



        // disables frontend editor of visual composer

        if( function_exists( 'vc_disable_frontend' ) ){

            vc_disable_frontend();

        }



        // Add support for featured content.

        add_theme_support( 'featured-content', array(

            'featured_content_filter' => 'nfw_get_featured_content',

            'max_posts' => 6,

        ) );

    }



}



add_action( 'after_setup_theme', 'nfw_setup' );



function nfw_custom_wp_title ( $title, $sep ) {

    global $paged, $page;



    if ( is_feed() ) {

        return $title;

    } // end if

    // Add the site name.

    $title = get_bloginfo( 'name' );

    $sep = '::';

    // Add the site description for the home/front page.

    $site_description = get_bloginfo( 'description', 'display' );

    if ( $site_description && ( is_home() || is_front_page() ) ) {

        $title = "$title $sep $site_description";

    } // end if

    // Add a page number if necessary.

    if ( $paged >= 2 || $page >= 2 ) {

        $title = sprintf( __( 'Page %s', 'gold_domain' ), max( $paged, $page ) ) . " $sep $title";

    } // end if



    return esc_html( $title );

}



add_filter( 'wp_title', 'nfw_custom_wp_title', 10, 2 );



// Adds the custom CSS and the custom color schemes 

add_action( 'wp_head', 'nfw_color_scheme_styles' );



function nfw_color_scheme_styles(){

    if( !is_admin() ){

        global $nfw_theme_options;



        echo '<style type="text/css">' . "\n";



        if( is_404() ){

            echo '.wpb_content_element {margin-bottom: 35px;}';

        }



        if( is_admin_bar_showing() ){

            echo '#header.stuck { padding-top: 45px; }';

        }



        $nfw_accent_colors_enable = 0;



        if( isset( $nfw_theme_options['nfw-accent-colors-enable'] ) ){

            $nfw_accent_colors_enable = $nfw_theme_options['nfw-accent-colors-enable'];

        }

        if( $nfw_accent_colors_enable == 1 ){



            $color_default = "#80a852";

            if( isset( $nfw_theme_options['nfw-accent-color-scheme-default'] ) ){

                if( preg_match( '/(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i', $nfw_theme_options['nfw-accent-color-scheme-default'] ) ){

                    $color_default = $nfw_theme_options['nfw-accent-color-scheme-default'];

                }

            }



            $color_2 = "#b1a843";

            if( isset( $nfw_theme_options['nfw-accent-color-scheme-2'] ) ){

                if( preg_match( '/(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i', $nfw_theme_options['nfw-accent-color-scheme-2'] ) ){

                    $color_2 = $nfw_theme_options['nfw-accent-color-scheme-2'];

                }

            }



            $color_3 = "#e18f5f";

            if( isset( $nfw_theme_options['nfw-accent-color-scheme-3'] ) ){

                if( preg_match( '/(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i', $nfw_theme_options['nfw-accent-color-scheme-3'] ) ){

                    $color_3 = $nfw_theme_options['nfw-accent-color-scheme-3'];

                }

            }



            $color_4 = "#f3e686";

            if( isset( $nfw_theme_options['nfw-accent-color-scheme-4'] ) ){

                if( preg_match( '/(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i', $nfw_theme_options['nfw-accent-color-scheme-3'] ) ){

                    $color_4 = $nfw_theme_options['nfw-accent-color-scheme-4'];

                }

            }



            if( $color_default != "#80a852" ){

                echo '  /* Color Default */

            .blockquote.accent-color-1,

            table th,

            .accordion-item-toggle i,

            .toggle-item-toggle i,

            .accordion-item.accent-color-1 .accordion-item-toggle i,

            .toggle-item.accent-color-1 .toggle-item-toggle i,

            .alert.error,

            #back-to-top,

            .fullwidth-section,

            a.play,

            .icon-box-1 > i,

            .icon-box-1.accent-color-1 > i,

            .icon-box-2 > span i,

            .icon-box-2.accent-color-1 > span i,

            .icon-box-5 > i,

            .icon-box-5.accent-color-1 > i,

            .icon-box-6 > span i,

            .icon-box-6.accent-color-1 > span i,

            .icon-box-6.accent-color-4:hover > span i,

            .pricing-table-header h1,

            .pricing-table .btn,

            .pricing-table.accent-color-1 .pricing-table-header h1,

            .pricing-table.accent-color-1 .btn,

            .progress-bar .progress-bar-outer,

            .progress-bar.accent-color-1 .progress-bar-outer,

            .tabs-container .tabs-menu li a span i,

            .tabs-container .tabs-menu li.accent-color-1 a span i,

            .vertical-tabs-container .tabs-menu li span i,

            .vertical-tabs-container .tabs-menu li.accent-color-1 span i,

            .widget_tag_cloud a,

            .footer-top,

            .portfolio-item-overlay-actions .zoom

            { background-color: ' . $color_default . '; }



            a, 

            a:visited,

            .btn,

            a.btn,

            .btn.accent-color-1,

            a.btn.accent-color-1,

            ul.fill-circle li:before,

            ul.check li:before,

            ul.fill-circle.accent-color-1 li:before,

            ul.check.accent-color-1 li:before,

            .fullwidth-section .btn:hover,

            .icon-box-3 > i,

            .icon-box-4 > i,

            .icon-box-3.accent-color-1 > i,

            .icon-box-3.accent-color-1 a,

            .icon-box-4.accent-color-1 > i,

            .icon-box-4.accent-color-1 > i,

            .horizontal-process-builder-description h3 span,

            .pricing-table-offer ul li:before,

            .pricing-table .btn:hover,

            .pricing-table.accent-color-1 .pricing-table-offer ul li:before,

            .pricing-table.accent-color-1 .btn:hover,

            .testimonial blockquote h4 span,

            .testimonial.accent-color-1 blockquote h4 span,

            .widget_recent_entries ul li .post-date,

            .widget_pages ul li a:before,

            .widget_archive ul li a:before,

            .widget_categories ul li a:before,

            .widget_meta ul li a:before,

            .widget_tag_cloud a:hover,

            .widget_nav_menu ul li a:before,

            .nfw_widget_navigation li.current a,

            .nfw_widget_contact_info ul li i,

            .form-submit input#submit,

            .portfolio-item-description p span,

            .portfolio-filter ul li a,

            input.wpcf7-submit,

            #nfw-post-categories li:before

            { color: ' . $color_default . '; }

                                                    

            .accordion-item-toggle.active span,

            .toggle-item-toggle.active span,

            .accordion-item-toggle:hover span,

            .toggle-item-toggle:hover span,

            .toggle-item.accent-color-1 .toggle-item-toggle:hover span:before,

            .accordion-item.accent-color-1 .accordion-item-toggle.active span,

            .toggle-item.accent-color-1 .toggle-item-toggle.active span,

            .accordion-item.accent-color-1 .accordion-item-toggle.active span:before,

            .toggle-item.accent-color-1 .toggle-item-toggle.active span:before,

            .accordion-item.accent-color-1 .accordion-item-toggle:hover span,

            .toggle-item.accent-color-1 .toggle-item-toggle:hover span,

            .accordion-item.accent-color-1 .accordion-item-toggle:hover span:before,

            .toggle-item.accent-color-1 .toggle-item-toggle:hover span:before,

            .alert.error,

            .btn,

            .btn.accent-color-1,

            ul.check li:before,

            ul.check.accent-color-1 li:before,

            a.play,

            .icon-box-2:hover > span,

            .icon-box-2.accent-color-1:hover > span,

            .icon-box-2.accent-color-1:hover > span:after,

            .icon-box-6 > span,

            .icon-box-6.accent-color-1 > span,

            .icon-box-6.accent-color-1 > span:after,

            .icon-box-6.accent-color-4:hover > span,

            .icon-box-6.accent-color-4:hover > span:after,

            .pricing-table .btn,

            .pricing-table:hover .pricing-table-header,

            .pricing-table.accent-color-1 .btn,

            a.social-icon,

            .tabs-container .tabs-menu li a:hover span,

            .tabs-container .tabs-menu li.active a span,

            .tabs-container .tabs-menu li.accent-color-1 a:hover span,

            .tabs-container .tabs-menu li.accent-color-1.active a span,

            .tabs-container .tabs-menu li.accent-color-1 a:hover span:after,

            .tabs-container .tabs-menu li.accent-color-1.active a span:after,

            .vertical-tabs-container .tabs-menu li a:hover span,

            .vertical-tabs-container .tabs-menu li.active a span,

            .vertical-tabs-container .tabs-menu li.accent-color-1 a:hover span,

            .vertical-tabs-container .tabs-menu li.accent-color-1.active a span,

            .vertical-tabs-container .tabs-menu li.accent-color-1 a:hover span:before,

            .vertical-tabs-container .tabs-menu li.accent-color-1.active a span:before,

            .testimonial > span,

            .testimonial.accent-color-1 > span,

            .testimonial.accent-color-1 > span:after,

            .widget_pages ul li a:hover:before,

            .widget_archive ul li a:hover:before,

            .widget_categories ul li a:hover:before,

            .widget_meta ul li a:hover:before,

            .widget_tag_cloud a,

            .widget_nav_menu ul li a:hover:before,

            .commentlist .vcard img.photo,

            .form-submit input#submit,

            .team-member:hover > span,

            .team-member.accent-color-1:hover > span,

            .team-member.accent-color-1:hover > span:after,

            .portfolio-item-overlay-actions .zoom,

            .pagination a,

            .portfolio-filter ul li a,

            input.wpcf7-submit,

            .sticky,

            .nfw-search-popup-box

            { border-color: ' . $color_default . '; }

                                                    

            .accordion-item-toggle.active span:before,

            .toggle-item-toggle.active span:before,

            .accordion-item-toggle:hover span:before,

            .toggle-item-toggle:hover span:before,

            .tabs-container .tabs-menu li a:hover span:after,

            .tabs-container .tabs-menu li.active a span:after,

            .vertical-tabs-container .tabs-menu li a:hover span:before,

            .vertical-tabs-container .tabs-menu li.active a span:before,

            .testimonial > span:after,

            .widget_pages ul li a:hover:after,

            .widget_archive ul li a:hover:after,

            .widget_categories ul li a:hover:after,

            .widget_meta ul li a:hover:after,

            .widget_nav_menu ul li a:hover:after

            { border-top-color: ' . $color_default . '; }

                                                    

            .icon-box-2:hover > span:after,

            .icon-box-6 > span:after

            { border-left-color: ' . $color_default . '; }

                                                    

            .btn:hover,

            .btn.accent-color-1:hover,

            .form-submit input#submit:hover,

            .portfolio-item-overlay-actions .btn:hover,

            .pagination li.current a,

            .pagination li:hover a,

            .portfolio-filter ul li a.active,

            .portfolio-filter ul li a:hover,

            input.wpcf7-submit:hover

            { box-shadow: inset 0 100px 0 0 ' . $color_default . '; }';

            }

            if( $color_2 != "#b1a843" ){

                echo '/* Color 2 */

            blockquote.accent-color-2,

            .accordion-item.accent-color-2 .accordion-item-toggle i,

            .toggle-item.accent-color-2 .toggle-item-toggle i,

            .icon-box-1.accent-color-2 > i,

            .icon-box-2.accent-color-2 > span i,

            .icon-box-5.accent-color-2 > i,

            .icon-box-6.accent-color-2 > span i ,

            .pricing-table.accent-color-2 .pricing-table-header h1,

            .pricing-table.accent-color-2 .btn,

            .progress-bar.accent-color-2 .progress-bar-outer,

            table.accent-color-2 th,

            .tabs-container .tabs-menu li.accent-color-2 a span i,

            .vertical-tabs-container .tabs-menu li.accent-color-2 span i,

            .alert.warning

            { background-color: ' . $color_2 . '; }

                                                    

            .accordion-item.accent-color-2 .accordion-item-toggle.active span,

            .toggle-item.accent-color-2 .toggle-item-toggle.active span,

            .accordion-item.accent-color-2 .accordion-item-toggle.active span:before,

            .toggle-item.accent-color-2 .toggle-item-toggle.active span:before,	

            .accordion-item.accent-color-2 .accordion-item-toggle:hover span,

            .toggle-item.accent-color-2 .toggle-item-toggle:hover span,

            .accordion-item.accent-color-2 .accordion-item-toggle:hover span:before,

            .toggle-item.accent-color-2 .toggle-item-toggle:hover span:before,

            .btn.accent-color-2,

            ul.check.accent-color-2 li:before,

            .icon-box-2.accent-color-2:hover > span,

            .icon-box-2.accent-color-2:hover > span:after,

            .icon-box-6.accent-color-2 > span,

            .icon-box-6.accent-color-2 > span:after,

            .pricing-table.accent-color-2:hover .pricing-table-header,

            .pricing-table.accent-color-2 .btn,

            .tabs-container .tabs-menu li.accent-color-2 a:hover span,

            .tabs-container .tabs-menu li.accent-color-2.active a span,

            .tabs-container .tabs-menu li.accent-color-2 a:hover span:after,

            .tabs-container .tabs-menu li.accent-color-2.active a span:after,

            .vertical-tabs-container .tabs-menu li.accent-color-2 a:hover span,

            .vertical-tabs-container .tabs-menu li.accent-color-2.active a span,

            .vertical-tabs-container .tabs-menu li.accent-color-2 a:hover span:before,

            .vertical-tabs-container .tabs-menu li.accent-color-2.active a span:before,

            .testimonial.accent-color-2 > span,

            .testimonial.accent-color-2 > span:after,

            .team-member.accent-color-2:hover > span,

            .team-member.accent-color-2:hover > span:after,

            .alert.warning

            { border-color: ' . $color_2 . '; }

                                                    

            .btn.accent-color-2,

            a.btn.accent-color-2,

            ul.fill-circle.accent-color-2 li:before,

            ul.check.accent-color-2 li:before,

            .icon-box-3.accent-color-2 > i,

            .icon-box-3.accent-color-2 a,

            .icon-box-4.accent-color-2 > i,

            .pricing-table.accent-color-2 .pricing-table-offer ul li:before,

            .pricing-table.accent-color-2 .btn:hover,

            .testimonial.accent-color-2 blockquote h4 span

            { color: ' . $color_2 . '; }

                                                    

            .btn.accent-color-2:hover

            { box-shadow: inset 0 100px 0 0 ' . $color_2 . '; }';

            }



            if( $color_3 != "#e18f5f" ){

                echo '/* Color 3 */

            blockquote.accent-color-3,

            .accordion-item.accent-color-3 .accordion-item-toggle i,

            .toggle-item.accent-color-3 .toggle-item-toggle i,

            .alert.success,

            .icon-box-1.accent-color-3 > i,

            .icon-box-2 h1.error span,

            .icon-box-2.accent-color-3 > span i,

            .icon-box-5.accent-color-3 > i,

            .icon-box-6.accent-color-3 > span i,

            .pricing-table.accent-color-3 .pricing-table-header h1,

            .pricing-table.accent-color-3 .btn,

            .progress-bar.accent-color-3 .progress-bar-outer,

            table.accent-color-3 th,

            .tabs-container .tabs-menu li.accent-color-3 a span i,

            .vertical-tabs-container .tabs-menu li.accent-color-3 span i

            { background-color: ' . $color_3 . '; }

                                                    

            .accordion-item.accent-color-3 .accordion-item-toggle.active span,

            .toggle-item.accent-color-3 .toggle-item-toggle.active span,

            .accordion-item.accent-color-3 .accordion-item-toggle.active span:before,

            .toggle-item.accent-color-3 .toggle-item-toggle.active span:before,

            .accordion-item.accent-color-3 .accordion-item-toggle:hover span,

            .toggle-item.accent-color-3 .toggle-item-toggle:hover span,

            .accordion-item.accent-color-3 .accordion-item-toggle:hover span:before,

            .toggle-item.accent-color-3 .toggle-item-toggle:hover span:before,

            .alert.success,

            .btn.accent-color-3,

            ul.check.accent-color-3 li:before,

            .icon-box-2 h1.error,

            .icon-box-2.accent-color-3:hover > span,

            .icon-box-2.accent-color-3:hover > span:after,

            .icon-box-6.accent-color-3 > span,

            .icon-box-6.accent-color-3 > span:after,

            .pricing-table.accent-color-3:hover .pricing-table-header,

            .pricing-table.accent-color-3 .btn,

            .tabs-container .tabs-menu li.accent-color-3 a:hover span,

            .tabs-container .tabs-menu li.accent-color-3.active a span,

            .tabs-container .tabs-menu li.accent-color-3 a:hover span:after,

            .tabs-container .tabs-menu li.accent-color-3.active a span:after,

            .vertical-tabs-container .tabs-menu li.accent-color-3 a:hover span,

            .vertical-tabs-container .tabs-menu li.accent-color-3.active a span,

            .vertical-tabs-container .tabs-menu li.accent-color-3 a:hover span:before,

            .vertical-tabs-container .tabs-menu li.accent-color-3.active a span:before,

            .testimonial.accent-color-3 > span,

            .testimonial.accent-color-3 > span:after,

            .sf-menu > li.current-menu-item > a span,

            .sf-menu li.menu-item ul li a:hover:before,

            .team-member.accent-color-3:hover > span,

            .team-member.accent-color-3:hover > span:after

            { border-color: ' . $color_3 . '; }

                                                    

            .btn.accent-color-3,

            a.btn.accent-color-3,

            ul.fill-circle.accent-color-3 li:before,

            ul.check.accent-color-3 li:before,

            .icon-box-3.accent-color-3 > i,

            .icon-box-3.accent-color-3 a,

            .icon-box-4.accent-color-3 > i,

            .pricing-table.accent-color-3 .pricing-table-offer ul li:before,

            .pricing-table.accent-color-3 .btn:hover,

            .testimonial.accent-color-3 blockquote h4 span,

            .sf-menu > li.current-menu-item > a span,

            .sf-menu > li.current-menu-item > a small,

            .sf-menu > li > a:hover span,

            .sf-menu > li > a:hover small,

            .sf-menu li.menu-item ul li a:before,

            .sf-menu li.menu-item ul li a:hover,

            .sf-mega-section li a:hover 

            { color: ' . $color_3 . '; }

                                                    

            .btn.accent-color-3:hover,

            #mc_signup input[type="submit"]:hover,

            .slider2-link .btn:hover

            { box-shadow: inset 0 100px 0 0 ' . $color_3 . '; }

                                                    

            .icon-box-2 h1.error:before

            { border-left-color: ' . $color_3 . '; }

                                                    

            .horizontal-process-builder:before,

            .sf-menu li.menu-item ul li a:hover:after

            { border-top-color: ' . $color_3 . '; }';

            }



            if( $color_4 != "#f3e686" ){

                echo '/* Color 4 */

            blockquote.accent-color-4,

            .accordion-item.accent-color-4 .accordion-item-toggle i,

            .toggle-item.accent-color-4 .toggle-item-toggle i,

            .icon-box-1.accent-color-4 > i,

            .icon-box-2.accent-color-4 > span i,

            .icon-box-5.accent-color-4 > i,

            .icon-box-6:hover,

            .icon-box-6.accent-color-4 > span i,

            .pricing-table.accent-color-4 .pricing-table-header h1,

            .pricing-table.accent-color-4 .btn,

            .progress-bar.accent-color-4 .progress-bar-outer,

            table.accent-color-4 th,

            .tabs-container .tabs-menu li.accent-color-4 a span i,

            .vertical-tabs-container .tabs-menu li.accent-color-4 span i,

            #mc_signup input[type="submit"],

            #mobile-menu li,

            .tp-leftarrow.default,

            .tp-rightarrow.default,

            .slider2-link .btn,

            .tp-bullets.simplebullets.round .bullet

            { background-color: ' . $color_4 . '; }

                    

            .accordion-item.accent-color-4 .accordion-item-toggle.active span,

            .toggle-item.accent-color-4 .toggle-item-toggle.active span,

            .accordion-item.accent-color-4 .accordion-item-toggle.active span:before,

            .toggle-item.accent-color-4 .toggle-item-toggle.active span:before,

            .accordion-item.accent-color-4 .accordion-item-toggle:hover span,

            .toggle-item.accent-color-4 .toggle-item-toggle:hover span,

            .accordion-item.accent-color-4 .accordion-item-toggle:hover span:before,

            .toggle-item.accent-color-4 .toggle-item-toggle:hover span:before,

            .alert.info,

            .btn.accent-color-4,

            ul.check.accent-color-4 li:before,

            .icon-box-2.accent-color-4:hover > span,

            .icon-box-2.accent-color-4:hover > span:after,

            .icon-box-6,

            .icon-box-6.accent-color-4 > span,

            .icon-box-6.accent-color-4 > span:after,

            .pricing-table.accent-color-4:hover .pricing-table-header,

            .pricing-table.accent-color-4 .btn,

            .tabs-container .tabs-menu li.accent-color-4 a:hover span,

            .tabs-container .tabs-menu li.accent-color-4.active a span,

            .tabs-container .tabs-menu li.accent-color-4 a:hover span:after,

            .tabs-container .tabs-menu li.accent-color-4.active a span:after,

            .vertical-tabs-container .tabs-menu li.accent-color-4 a:hover span,

            .vertical-tabs-container .tabs-menu li.accent-color-4.active a span,

            .vertical-tabs-container .tabs-menu li.accent-color-4 a:hover span:before,

            .vertical-tabs-container .tabs-menu li.accent-color-4.active a span:before,

            .testimonial.accent-color-4 > span,

            .testimonial.accent-color-4 > span:after,

            .team-member.accent-color-4:hover > span,

            .team-member.accent-color-4:hover > span:after,

            .slider2-link .btn

            { border-color: ' . $color_4 . '; }

                    

            .btn.accent-color-4,

            a.btn.accent-color-4,

            ul.fill-circle.accent-color-4 li:before,

            ul.check.accent-color-4 li:before,

            .icon-box-3.accent-color-4 > i,

            .icon-box-3.accent-color-4 a,

            .icon-box-4.accent-color-4 > i,

            .pricing-table.accent-color-4 .pricing-table-offer ul li:before,

            .pricing-table.accent-color-4 .btn:hover,

            .testimonial.accent-color-4 blockquote h4 span,

            .slider3-title span,

            #mobile-menu-trigger

            { color: ' . $color_4 . '; }

                    

            .btn.accent-color-4:hover,

            #mc_signup input[type="submit"]

            { box-shadow: inset 0 100px 0 0 ' . $color_4 . '; }

                    

            #wrap

            { border-top-color: ' . $color_4 . '; }';

            }



            echo '.sf-menu a { color: #69675b; }

            a.social-icon { color: #69675b; }';

        }

        /* Theme options custom css related field */

        $nfw_custom_css = "";

        if( isset( $nfw_theme_options['nfw-custom-css'] ) ){

            $nfw_custom_css = $nfw_theme_options['nfw-custom-css'];

        }

        if( trim( $nfw_custom_css ) != '' ){

            echo $nfw_custom_css;

        }



        echo '</style>' . "\n";

    }

}



function nfw_header_scripts(){

    if( !is_admin() ){

        global $nfw_theme_options;

        if( isset( $nfw_theme_options['nfw-custom-analytics'] ) ){

            if( trim( $nfw_theme_options['nfw-custom-analytics'] ) != "" ){

                echo '<script type="text/javascript">' . "\n",

                $nfw_theme_options['nfw-custom-analytics'] . "\n",

                '</script>' . "\n";

            }

        }

    }

}



add_action( 'wp_head', 'nfw_header_scripts' );



/*

 * conditional statement will show the featured content if at least there is one present

 * and while the page is not being paged (it is not in the second page onwards).

 */



function nfw_get_featured_content( $num = 1 ){

    global $featured;

    $featured = apply_filters( 'nfw_featured_content', array() );



    if( is_array( $featured ) || $num >= count( $featured ) )

        return true;



    return false;

}



// A helper conditional function that returns a boolean value.

function nfw_has_featured_posts(){

    return !is_paged() && (bool) nfw_get_featured_content();

}



// Widget areas

function nfw_widgets_init(){

    require get_template_directory() . '/framework/widgets.php';

    register_widget( 'nfw_contact_widget' );

    register_widget( 'nfw_connect_social_widget' );

    register_widget( 'nfw_twitter_widget' );

    register_widget( 'nfw_flickr_widget' );

    register_widget( 'nfw_goldposts_widget' );



    // creates the default sidebar

    register_sidebar( array(

        'name' => __( 'Default Sidebar', 'gold_domain' ),

        'id' => 'nfw-default-sidebar',

        'description' => __( 'Default widget area', 'gold_domain' ),

        'before_widget' => '<div id="%1$s" class="widget %2$s">',

        'after_widget' => '</div>',

        'before_title' => '<h3 class="widget-title">',

        'after_title' => '</h3>',

    ) );



    // Generates the the top widget areas of the footer

    for( $count = 1; $count <= 4; $count++ ){

        register_sidebar( array(

            'name' => __( 'Footer Top Widget Area ', 'gold_domain' ) . $count,

            'id' => 'nfw-footer-top-sidebar-' . $count,

            'description' => __( 'Top widget area', 'gold_domain' ),

            'before_widget' => '<div id="%1$s" class="widget %2$s">',

            'after_widget' => '</div>',

            'before_title' => '<h3 class="widget-title">',

            'after_title' => '</h3>',

        ) );

    }



    // Generates the the middle widget areas of the footer

    for( $count = 1; $count <= 4; $count++ ){

        register_sidebar( array(

            'name' => __( 'Footer Middle Widget Area ', 'gold_domain' ) . $count,

            'id' => 'nfw-footer-middle-sidebar-' . $count,

            'description' => __( 'Middle widget area', 'gold_domain' ),

            'before_widget' => '<div id="%1$s" class="widget %2$s">',

            'after_widget' => '</div>',

            'before_title' => '<h3 class="widget-title">',

            'after_title' => '</h3>',

        ) );

    }



    // Generates the the bottom widget areas of the footer

    for( $count = 1; $count <= 4; $count++ ){

        register_sidebar( array(

            'name' => __( 'Footer Bottom Widget Area ', 'gold_domain' ) . $count,

            'id' => 'nfw-footer-bottom-sidebar-' . $count,

            'description' => __( 'Bottom widget area', 'gold_domain' ),

            'before_widget' => '<div id="%1$s" class="widget %2$s">',

            'after_widget' => '</div>',

            'before_title' => '<h3 class="widget-title">',

            'after_title' => '</h3>',

        ) );

    }

}



add_action( 'widgets_init', 'nfw_widgets_init' );



// Default custom portfolio

add_action( 'init', 'nfw_create_portfolio_post' );



function nfw_create_portfolio_post(){

    register_post_type( 'portfolio', array(

        'labels' => array(

            'name' => __( 'Portfolio', 'gold_domain' ),

            'singular_name' => __( 'Portfolio', 'gold_domain' )

        ),

        'public' => true,

        'has_archive' => true,

        'show_in_nav_menus' => true,

        'supports' => array(

            'title', 'editor', 'excerpt',

            'thumbnail'),

            )

    );

}



// Registers the portfolio category

function nfw_create_portfolio_taxonomies(){

    $labels = array(

        'name' => __( 'Categories', 'gold_domain' ),

        'singular_name' => __( 'Category', 'gold_domain' ),

        'search_items' => __( 'Search Categories', 'gold_domain' ),

        'all_items' => __( 'All Categories', 'gold_domain' ),

        'parent_item' => __( 'Parent Category', 'gold_domain' ),

        'parent_item_colon' => __( 'Parent Category:', 'gold_domain' ),

        'edit_item' => __( 'Edit Category', 'gold_domain' ),

        'update_item' => __( 'Update Category', 'gold_domain' ),

        'add_new_item' => __( 'Add New Category', 'gold_domain' ),

        'new_item_name' => __( 'New Category Name', 'gold_domain' ),

        'menu_name' => __( 'Categories', 'gold_domain' ),

    );



    $args = array(

        'hierarchical' => true,

        'labels' => $labels,

        'show_ui' => true,

        'show_admin_column' => true,

        'query_var' => true,

        'rewrite' => array('slug' => 'categories'),

    );



    register_taxonomy( 'portfolio_categories', array('portfolio'), $args );

}



add_action( 'init', 'nfw_create_portfolio_taxonomies', 0 );



// Pagination function

function nfw_pagination( $pages = '', $range = 3 ){

    $showitems = ($range * 2) + 1;

    //$showitems = $range-1;



    global $paged;

    if( empty( $paged ) )

        $paged = 1;



    if( $pages == '' ){

        global $wp_query;

        $pages = $wp_query->max_num_pages;

        if( !$pages ){

            $pages = 1;

        }

    }



    if( 1 != $pages ){

        echo "<ul class=\"pagination fixed\">";



        if( $paged > 2 && $paged > $range + 1 && $showitems < $pages ){

            echo "<li><span title='First page'><a href='" . esc_url( get_pagenum_link( 1 ) ) . "'>&lt;</a></span></li>";

        }



        for( $i = 1; $i <= $pages; $i++ ){

            if( 1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems ) ){

                echo ($paged == $i) ? "<li class=\"current\"><a href='" . esc_url( get_pagenum_link( $i ) ) . "'>" . $i . "</a></li>" :

                        "<li><a href='" . esc_url( get_pagenum_link( $i ) ) . "'>" . $i . "</a></li>";

            }

        }



        if( $paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages ){

            echo "<li><span title='Last page'><a href='" . esc_url( get_pagenum_link( $pages ) ) . "'>&gt;</a></span></li>";

        }



        echo "</ul>\n";

    }

}



// Add Formats Dropdown Menu To MCE

if( !function_exists( 'nfw_style_select' ) ){



    function nfw_style_select( $buttons ){

        array_push( $buttons, 'styleselect' );

        return $buttons;

    }



}

add_filter( 'mce_buttons', 'nfw_style_select' );



// Add new styles to the TinyMCE "formats" menu dropdown

if( !function_exists( 'nfw_styles_dropdown' ) ){



    function nfw_styles_dropdown( $settings ){



        // Create array of new styles

        $new_styles = array(

            array(

                'title' => __( 'List Styles', 'gold_domain' ),

                'items' => array(

                    array(

                        'title' => __( 'Circle color 1', 'gold_domain' ),

                        'selector' => 'ul',

                        'classes' => 'fill-circle accent-color-1'

                    ),

                    array(

                        'title' => __( 'Circle color 2', 'gold_domain' ),

                        'selector' => 'ul',

                        'classes' => 'fill-circle accent-color-2'

                    ),

                    array(

                        'title' => __( 'Circle color 3', 'gold_domain' ),

                        'selector' => 'ul',

                        'classes' => 'fill-circle accent-color-3'

                    ),

                    array(

                        'title' => __( 'Circle color 4', 'gold_domain' ),

                        'selector' => 'ul',

                        'classes' => 'fill-circle accent-color-4'

                    ),

                    array(

                        'title' => __( 'Check color 1', 'gold_domain' ),

                        'selector' => 'ul',

                        'classes' => 'check accent-color-1'

                    ),

                    array(

                        'title' => __( 'Check color 2', 'gold_domain' ),

                        'selector' => 'ul',

                        'classes' => 'check accent-color-2'

                    ),

                    array(

                        'title' => __( 'Check color 3', 'gold_domain' ),

                        'selector' => 'ul',

                        'classes' => 'check accent-color-3'

                    ),

                    array(

                        'title' => __( 'Check color 4', 'gold_domain' ),

                        'selector' => 'ul',

                        'classes' => 'check accent-color-4'

                    ),

                )

            )

        );



        // Merge old & new styles

        $settings['style_formats_merge'] = true;



        // Add new styles

        $settings['style_formats'] = json_encode( $new_styles );



        // Return New Settings

        return $settings;

    }



}

add_filter( 'tiny_mce_before_init', 'nfw_styles_dropdown' );



$prefix = 'nfw_';

// Meta box structure for custom page_headers field in the edit page section of the admin

$metabox_page_header = array(

    'id' => 'nfw-page-header',

    'title' => __( 'Page Header', 'gold_domain' ),

    'page' => 'page',

    'context' => 'normal',

    'priority' => 'high',

    'fields' => array(

        array(

            'name' => __( 'Header :', 'gold_domain' ),

            'id' => $prefix . 'header_toggle',

            'type' => 'radio',

            'options' => array(

                array('name' => 'On', 'value' => 'on'),

                array('name' => 'Off', 'value' => 'off')

            )

        ),

        array(

            'name' => __( 'Page header title :', 'gold_domain' ),

            'id' => $prefix . 'header_title',

            'type' => 'text'

        )

    )

);



add_action( 'admin_menu', 'nfw_page_header_add_box' );



// Add the meta box

function nfw_page_header_add_box(){

    global $metabox_page_header;

    add_meta_box( $metabox_page_header['id'], $metabox_page_header['title'], 'nfw_page_header_show_box', 'page', $metabox_page_header['context'], $metabox_page_header['priority'] );

    add_meta_box( $metabox_page_header['id'], $metabox_page_header['title'], 'nfw_page_header_show_box', 'portfolio', $metabox_page_header['context'], $metabox_page_header['priority'] );

}



// Callback function to show fields in meta box

function nfw_page_header_show_box(){

    global $metabox_page_header, $post;

    // Use nonce for verification



    echo '<input type="hidden" name="nfw_page_header_meta_box_nonce" value="', wp_create_nonce( basename( __FILE__ ) ), '" />';

    echo '<table class="form-table">';

    foreach( $metabox_page_header['fields'] as $field ){

        // Get current post meta data

        $meta = get_post_meta( $post->ID, $field['id'], true );

        echo '<tr>',

        '<th><label for="', esc_attr( $field['id'] ), '">', esc_attr( $field['name'] ), '</label></th>',

        '<td>';

        switch( $field['type'] ){

            case 'radio':



                foreach( $field['options'] as $option ){

                    // Prepare the default option

                    if( $meta == null && $option['value'] == 'off' ){

                        $default_check = 'checked="checked"';

                    } else{

                        $default_check = '';

                    }

                    echo ' <input type="radio" id="toggle-' . esc_attr( $option['name'] ) . '" class="toggle toggle-' . esc_attr( $option['name'] ) . '" name="', esc_attr( $field['id'] ), '" '

                    . 'value="', esc_attr( $option['value'] ), '"', $meta == $option['value'] ? ' checked="checked"' : '', ' ' . $default_check . ' />'

                    . '<label for="toggle-' . esc_attr( $option['name'] ) . '">' . esc_html( $option['name'] ) . '</label>';

                }

                break;

            case 'text':

                echo '<input type="text" name="' . esc_attr( $field['id'] ) . '" id="' . esc_attr( $field['id'] ) . '" value="' . esc_attr( $meta ) . '" size="30" /><br>';

                break;

        }

        echo '</td><td>',

        '</td></tr>';

    }

    echo '</table>';

}



add_action( 'save_post', 'nfw_save_page_header_data' );



// Save data from meta box

function nfw_save_page_header_data( $post_id ){

    global $metabox_page_header;

    if( isset( $_POST['nfw_page_header_meta_box_nonce'] ) ){

        // Verify nonce

        if( !wp_verify_nonce( $_POST['nfw_page_header_meta_box_nonce'], basename( __FILE__ ) ) ){

            return $post_id;

        }

        // Check autosave

        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){

            return $post_id;

        }

        // Check permissions

        if( 'page' == $_POST['post_type'] ){

            if( !current_user_can( 'edit_page', $post_id ) ){

                return $post_id;

            }

        } elseif( !current_user_can( 'edit_post', $post_id ) ){

            return $post_id;

        }

        // Populates the meta box fields with the appropriate data

        foreach( $metabox_page_header['fields'] as $field ){

            $old = get_post_meta( $post_id, $field['id'], true );

            $new = $_POST[$field['id']];

            if( $new && $new != $old ){

                update_post_meta( absint( $post_id ), sanitize_text_field( $field['id'] ), sanitize_text_field( $new ) );

            } elseif( '' == $new && $old ){

                delete_post_meta( absint( $post_id ), sanitize_text_field( $field['id'] ), sanitize_text_field( $old ) );

            }

        }

    }

}



// Check autosave

if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){

    return $post_id;

}



class nfw_Nav_Menu_Walker extends Walker_Nav_Menu {



    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){

        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';



        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $classes[] = 'menu-item-' . $item->ID;



        /**

         * Filter the CSS class(es) applied to a menu item's <li>.

         *

         * @since 3.0.0

         *

         * @see wp_nav_menu()

         *

         * @param array  $classes The CSS classes that are applied to the menu item's <li>.

         * @param object $item    The current menu item.

         * @param array  $args    An array of wp_nav_menu() arguments.

         */

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';



        /**

         * Filter the ID applied to a menu item's <li>.

         *

         * @since 3.0.1

         *

         * @see wp_nav_menu()

         *

         * @param string $menu_id The ID that is applied to the menu item's <li>.

         * @param object $item    The current menu item.

         * @param array  $args    An array of wp_nav_menu() arguments.

         */

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args );

        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';



        $output .= $indent . '<li' . $id . $class_names . '>';



        $atts = array();

        $atts['title'] = !empty( $item->attr_title ) ? $item->attr_title : '';

        $atts['target'] = !empty( $item->target ) ? $item->target : '';

        $atts['rel'] = !empty( $item->xfn ) ? $item->xfn : '';

        $atts['href'] = !empty( $item->url ) ? $item->url : '';



        /**

         * Filter the HTML attributes applied to a menu item's <a>.

         *

         * @since 3.6.0

         *

         * @see wp_nav_menu()

         *

         * @param array $atts {

         *     The HTML attributes applied to the menu item's <a>, empty strings are ignored.

         *

         *     @type string $title  Title attribute.

         *     @type string $target Target attribute.

         *     @type string $rel    The rel attribute.

         *     @type string $href   The href attribute.

         * }

         * @param object $item The current menu item.

         * @param array  $args An array of wp_nav_menu() arguments.

         */

        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );



        $attributes = '';

        foreach( $atts as $attr => $value ){

            if( !empty( $value ) ){

                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );

                $attributes .= ' ' . $attr . '="' . $value . '"';

            }

        }





        $item_output = $args->before;

        $item_output .= '<a' . $attributes . '>';

        /** This filter is documented in wp-includes/post-template.php */

        if( $depth == 0 ){

            $item_output .= $args->link_before . '<span>' . apply_filters( 'the_title', $item->title, $item->ID ) . '<small>' . $atts['title'] . '</small></span>' . $args->link_after;

        } else{

            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

        }

        $item_output .= '</a>';

        $item_output .= $args->after;



        /**

         * Filter a menu item's starting output.

         *

         * The menu item's starting output only includes $args->before, the opening <a>,

         * the menu item's title, the closing </a>, and $args->after. Currently, there is

         * no filter for modifying the opening and closing <li> for a menu item.

         *

         * @since 3.0.0

         *

         * @see wp_nav_menu()

         *

         * @param string $item_output The menu item's starting HTML output.

         * @param object $item        Menu item data object.

         * @param int    $depth       Depth of menu item. Used for padding.

         * @param array  $args        An array of wp_nav_menu() arguments.

         */

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

    }



}

