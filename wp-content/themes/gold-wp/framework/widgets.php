<?php

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       Contact Widget
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class nfw_contact_widget extends WP_Widget {

    function __construct(){
        parent::__construct(
                'nfw_contact_widget', __( 'Gold - Contact', 'gold_domain' ), array(
            'description' => __( 'contact widget', 'gold_domain' ),)
        );
    }

    public function widget( $args, $instance ){
        $title = apply_filters( 'widget_title', $instance['title'] );
        $email = apply_filters( 'widget_email', $instance['email'] );
        $address = apply_filters( 'widget_address', $instance['address'] );
        $phone = apply_filters( 'widget_phone', $instance['phone'] );
        $site = apply_filters( 'widget_site', $instance['site'] );

        echo wp_kses_post( $args['before_widget'] );
        ?>
        <div class="widget nfw_widget_contact_info">

            <?php
            if( trim( $title ) != '' ){
                echo wp_kses_post( $args['before_title'] );
                echo esc_html( $title );
                echo wp_kses_post( $args['after_title'] );
            }
            ?>

            <ul>
                <?php if( trim( $address ) != '' ): ?>
                    <li>
                        <i class="ifc-home"></i>
                        <?php
                        $address = esc_html( $address );
                        $address_parts = preg_split( "/\r\n|\n|\r/", $address );

                        foreach( $address_parts as $address_part ){
                            echo esc_html( $address_part ) . '<br>';
                        }
                        ?>
                    </li>
                    <?php
                endif;
                if( trim( $phone ) != '' ):
                    ?>
                    <li>
                        <i class="ifc-phone1"></i>
                        <?php
                        echo esc_html( $phone );
                        ?><br>
                    </li>
                    <?php
                endif;
                if( trim( $email ) != '' ):
                    ?>
                    <li>
                        <i class="ifc-message"></i>
                        <a href="mailto:<?php esc_attr_e( $email ); ?>">
                            <?php echo esc_html( $email ); ?></a>
                    </li>
                    <?php
                endif;
                if( trim( $site ) != '' ):
                    ?>
                    <li>
                        <i class="ifc-internet_explorer"></i>
                        <a href="<?php
                        echo esc_html( $site );
                        ?>"><?php
                               echo esc_html( $site );
                               ?></a>
                    </li>
                <?php endif; ?>
            </ul>

        </div>

        <?php
        echo wp_kses_post( $args['after_widget'] );
    }

    public function form( $instance ){
        if( isset( $instance['title'] ) ){
            $title = $instance['title'];
        } else{
            $title = __( 'Contact', 'gold_domain' );
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'title' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'title' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $title ); ?>" />
        </p>

        <?php
        if( isset( $instance['email'] ) ){
            $email = $instance['email'];
        } else{
            $email = __( 'email', 'gold_domain' );
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'email' ) ); ?>"><?php _e( 'Email:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'email' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'email' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $email ); ?>" />
        </p>

        <?php
        if( isset( $instance['address'] ) ){
            $address = $instance['address'];
        } else{
            $address = __( 'address', 'gold_domain' );
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'address' ) ); ?>"><?php _e( 'Address:', 'gold_domain' ); ?></label>
            <textarea class="widefat" 
                      id="<?php esc_attr_e( $this->get_field_id( 'address' ) ); ?>" 
                      name="<?php esc_attr_e( $this->get_field_name( 'address' ) ); ?>" ><?php echo esc_textarea( $address ); ?> </textarea>
        </p>

        <?php
        if( isset( $instance['phone'] ) ){
            $phone = $instance['phone'];
        } else{
            $phone = __( 'phone', 'gold_domain' );
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'phone' ) ); ?>"><?php _e( 'Phone:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'phone' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'phone' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $phone ); ?>" />
        </p>

        <?php
        if( isset( $instance['site'] ) ){
            $site = $instance['site'];
        } else{
            $site = __( 'site', 'gold_domain' );
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'site' ) ); ?>"><?php _e( 'Site:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'site' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'site' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $site ); ?>" />
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ){
        $instance = array();
        $instance['title'] = (!empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['email'] = (!empty( $new_instance['email'] ) ) ? strip_tags( $new_instance['email'] ) : '';
        $instance['address'] = (!empty( $new_instance['address'] ) ) ? strip_tags( $new_instance['address'] ) : '';
        $instance['phone'] = (!empty( $new_instance['phone'] ) ) ? strip_tags( $new_instance['phone'] ) : '';
        $instance['site'] = (!empty( $new_instance['site'] ) ) ? strip_tags( $new_instance['site'] ) : '';
        return $instance;
    }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       Connect Social Widget
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class nfw_connect_social_widget extends WP_Widget {

    function __construct(){
        parent::__construct(
                'nfw_connect_social_widget', __( 'Gold - Connect With Us', 'gold_domain' ), array(
            'description' => __( 'Connect With Us widget', 'gold_domain' ),)
        );
    }

    public function widget( $args, $instance ){
        $title = apply_filters( 'widget_title', $instance['title'] );
        $facebook = apply_filters( 'widget_facebook', $instance['facebook'] );
        $googleplus = apply_filters( 'widget_googleplus', $instance['googleplus'] );
        $twitter = apply_filters( 'widget_twitter', $instance['twitter'] );
        $pinterest = apply_filters( 'widget_pinterest', $instance['pinterest'] );
        $youtube = apply_filters( 'widget_youtube', $instance['youtube'] );
        $linkedin = apply_filters( 'widget_linkedin', $instance['linkedin'] );
        $xing = apply_filters( 'widget_xing', $instance['xing'] );

        echo wp_kses_post( $args['before_widget'] );
        ?>
        <div class="widget nfw_widget_social_media">

            <?php
            if( trim( $title ) != '' ){
                echo wp_kses_post( $args['before_title'] );
                echo esc_html( $title );
                echo wp_kses_post( $args['after_title'] );
            }
            ?>

            <?php if( trim($facebook)!='' ): ?>
                <a href="<?php echo esc_url( $facebook ); ?>" class="facebook-icon social-icon" target="_blank">
                    <i class="fa fa-facebook"></i>
                </a>
            <?php endif; ?>

            <?php if( trim($twitter)!='' ): ?>
                <a href="<?php echo esc_url( $twitter ); ?>" class="twitter-icon social-icon" target="_blank">
                    <i class="fa fa-twitter"></i>
                </a>
            <?php endif; ?>

            <?php if( trim($googleplus)!='' ): ?>
                <a href="<?php echo esc_url( $googleplus ); ?>" class="googleplus-icon social-icon" target="_blank">
                    <i class="fa fa-google-plus"></i>
                </a>
            <?php endif; ?>

            <?php if( trim($linkedin)!='' ): ?>
                <a href="<?php echo esc_url( $linkedin ); ?>" class="linkedin-icon social-icon" target="_blank">
                    <i class="fa fa-linkedin"></i>
                </a>
            <?php endif; ?>

           <?php if( trim($youtube)!='' ): ?>
                <a href="<?php echo esc_url( $youtube ); ?>" class="youtube-icon social-icon" target="_blank">
                    <i class="fa fa-youtube"></i>
                </a>
            <?php endif; ?>

            <?php if( trim($pinterest)!='' ): ?>
                <a href="<?php echo esc_url( $pinterest ); ?>" class="pinterest-icon social-icon" target="_blank">
                    <i class="fa fa-pinterest"></i>
                </a>
            <?php endif; ?>
            
            <?php if( trim($xing)!='' ): ?>
                <a href="<?php echo esc_url( $xing ); ?>" class="xing-icon social-icon" target="_blank">
                    <i class="fa fa-xing"></i>
                </a>
            <?php endif; ?>

        </div>

        <?php
        echo wp_kses_post( $args['after_widget'] );
    }

    public function form( $instance ){
        if( isset( $instance['title'] ) ){
            $title = $instance['title'];
        } else{
            $title = __( 'Connect with us', 'gold_domain' );
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'title' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'title' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $title ); ?>" />
        </p>

        <?php
        if( isset( $instance['facebook'] ) ){
            $facebook = $instance['facebook'];
        } else{
            $facebook = '';
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'facebook' ) ); ?>"><?php _e( 'Facebook link:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'facebook' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'facebook' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $facebook ); ?>" />
        </p>
        <?php
        if( isset( $instance['twitter'] ) ){
            $twitter = $instance['twitter'];
        } else{
            $twitter = '';
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'twitter' ) ); ?>"><?php _e( 'Twitter link:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'twitter' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'twitter' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $twitter ); ?>" />
        </p>

        <?php
        if( isset( $instance['googleplus'] ) ){
            $googleplus = $instance['googleplus'];
        } else{
            $googleplus = '';
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'googleplus' ) ); ?>"><?php _e( 'Googleplus link:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'googleplus' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'googleplus' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $googleplus ); ?>" />
        </p>
        <?php
        if( isset( $instance['linkedin'] ) ){
            $linkedin = $instance['linkedin'];
        } else{
            $linkedin = '';
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'linkedin' ) ); ?>"><?php _e( 'LinkedIn link:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'linkedin' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'linkedin' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $linkedin ); ?>" />
        </p>

        <?php
        if( isset( $instance['youtube'] ) ){
            $youtube = $instance['youtube'];
        } else{
            $youtube = '';
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'youtube' ) ); ?>"><?php _e( 'Youtube link:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'youtube' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'youtube' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $youtube ); ?>" />
        </p>

        <?php
        if( isset( $instance['pinterest'] ) ){
            $pinterest = $instance['pinterest'];
        } else{
            $pinterest = '';
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'pinterest' ) ); ?>"><?php _e( 'Pinterest link:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'pinterest' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'pinterest' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $pinterest ); ?>" />
        </p>

        <?php
        if( isset( $instance['xing'] ) ){
            $xing = $instance['xing'];
        } else{
            $xing = '';
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'xing' ) ); ?>"><?php _e( 'Xing link:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'xing' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'xing' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $xing ); ?>" />
        </p>
        <?php
    }

    public function update( $new_instance, $old_instance ){
        $instance = array();
        $instance['title'] = (!empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['facebook'] = (!empty( $new_instance['facebook'] ) ) ? strip_tags( $new_instance['facebook'] ) : '';
        $instance['googleplus'] = (!empty( $new_instance['googleplus'] ) ) ? strip_tags( $new_instance['googleplus'] ) : '';
        $instance['twitter'] = (!empty( $new_instance['twitter'] ) ) ? strip_tags( $new_instance['twitter'] ) : '';
        $instance['pinterest'] = (!empty( $new_instance['pinterest'] ) ) ? strip_tags( $new_instance['pinterest'] ) : '';
        $instance['linkedin'] = (!empty( $new_instance['linkedin'] ) ) ? strip_tags( $new_instance['linkedin'] ) : '';
        $instance['youtube'] = (!empty( $new_instance['youtube'] ) ) ? strip_tags( $new_instance['youtube'] ) : '';
        $instance['xing'] = (!empty( $new_instance['xing'] ) ) ? strip_tags( $new_instance['xing'] ) : '';
        return $instance;
    }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       Twitter Widget 
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class nfw_twitter_widget extends WP_Widget {

    function __construct(){
        parent::__construct(
                'nfw_twitter_widget', __( 'Gold - Twitter', 'gold_domain' ), array(
            'description' => __( 'Twitter widget', 'gold_domain' ),)
        );
    }

    public function widget( $args, $instance ){
        $title = apply_filters( 'widget_title', $instance['title'] );
        $id = apply_filters( 'widget_id', $instance['id'] );
        $count = apply_filters( 'widget_count', $instance['count'] );

        echo wp_kses_post( $args['before_widget'] );
        ?>

        <div class="widget nfw_widget_twitter">

            <?php
            if( trim( $title ) != '' ){
                echo wp_kses_post( $args['before_title'] );
                echo esc_html( $title );
                echo wp_kses_post( $args['after_title'] );
            }
            ?>
            <?php
            if( trim( $id ) != '' ):
                if( preg_match( "/[^0-9]/", $count ) ){
                    $count = 1;
                }
                ?>
                <div class="tweet-widget" data-account-id="<?php esc_attr_e( $id ); ?>" data-items="<?php esc_attr_e( $count ); ?>"></div>
            <?php endif; ?>

        </div>        

        <?php
        echo wp_kses_post( $args['after_widget'] );
    }

    public function form( $instance ){
        if( isset( $instance['title'] ) ){
            $title = $instance['title'];
        } else{
            $title = __( 'Twitter', 'gold_domain' );
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'title' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'title' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $title ); ?>" />
        </p>

        <?php
        if( isset( $instance['id'] ) ){
            $id = $instance['id'];
        } else{
            $id = '';
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'id' ) ); ?>"><?php _e( 'ID:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'id' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'id' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $id ); ?>" />
        </p>

        <?php
        if( isset( $instance['count'] ) ){
            $count = $instance['count'];
        } else{
            $count = '1';
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'count' ) ); ?>"><?php _e( 'Number of tweets:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'count' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'count' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $count ); ?>" />
        </p>

        <?php
    }

    public function update( $new_instance, $old_instance ){
        $instance = array();
        $instance['title'] = (!empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['id'] = (!empty( $new_instance['id'] ) ) ? strip_tags( $new_instance['id'] ) : '';
        $instance['count'] = (!empty( $new_instance['count'] ) ) ? strip_tags( $new_instance['count'] ) : '';
        return $instance;
    }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       Flickr Widget 
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class nfw_flickr_widget extends WP_Widget {

    function __construct(){
        parent::__construct(
                'nfw_flickr_widget', __( 'Gold - Flickr', 'gold_domain' ), array(
            'description' => __( 'Flickr gallery widget', 'gold_domain' ),)
        );
    }

    public function widget( $args, $instance ){
        $user = apply_filters( 'widget_user', $instance['user'] );
        $title = apply_filters( 'widget_title', $instance['title'] );
        $images = apply_filters( 'widget_images', $instance['images'] );

        if( trim( $images ) != '' ){
            if( preg_match( "/[^0-9]/", $images ) ){
                $images = 1;
            }
        } else{
            $images = 1;
        }

        echo wp_kses_post( $args['before_widget'] );
        ?>
        <div class="widget nfw_widget_flickr">
            <?php
            if( trim( $title ) != '' ){
                echo wp_kses_post( $args['before_title'] );
                echo esc_html( $title );
                echo wp_kses_post( $args['after_title'] );
            }
            ?>
            <div id="flickr-feed">
                <?php if( trim( $user ) != '' ): ?>
                    <script type="text/javascript" src="//www.flickr.com/badge_code_v2.gne?count=<?php echo $images; ?>&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=<?php esc_attr_e( $user ); ?>"></script>

                <?php endif; ?>
            </div>
        </div>

        <?php
        echo wp_kses_post( $args['after_widget'] );
    }

    public function form( $instance ){
        if( isset( $instance['user'] ) ){
            $user = $instance['user'];
        } else{
            $user = '';
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'user' ) ); ?>"><?php _e( 'User:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'user' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'user' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $user ); ?>" />
        </p>
        <?php
        if( isset( $instance['title'] ) ){
            $title = $instance['title'];
        } else{
            $title = __( 'Flickr', 'gold_domain' );
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'title' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'title' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $title ); ?>" />
        </p>

        <?php
        if( isset( $instance['images'] ) ){
            $images = $instance['images'];
        } else{
            $images = '';
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'images' ) ); ?>"><?php _e( 'Number of images:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'images' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'images' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $images ); ?>" />
        </p>

        <?php
    }

    public function update( $new_instance, $old_instance ){
        $instance = array();
        $instance['user'] = (!empty( $new_instance['user'] ) ) ? strip_tags( $new_instance['user'] ) : '';
        $instance['title'] = (!empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['images'] = (!empty( $new_instance['images'] ) ) ? strip_tags( $new_instance['images'] ) : '';
        return $instance;
    }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       Gold posts Widget 
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class nfw_goldposts_widget extends WP_Widget {

    function __construct(){
        parent::__construct(
                'nfw_goldposts_widget', __( 'Gold - Recent posts', 'gold_domain' ), array(
            'description' => __( 'Displays recent posts with a thumbnail', 'gold_domain' ),)
        );
    }

    public function widget( $args, $instance ){
        $title = apply_filters( 'widget_title', $instance['title'] );
        $count = apply_filters( 'widget_count', $instance['count'] );

        if( trim( $count ) != '' ){
            if( preg_match( "/[^0-9]/", $count ) ){
                $count = 1;
            }
        } else{
            $count = 1;
        }

        echo wp_kses_post( $args['before_widget'] );
        ?>
        <div class="widget widget_recent_entries">
            <?php
            if( trim( $title ) != '' ){
                echo wp_kses_post( $args['before_title'] );
                echo esc_html( $title );
                echo wp_kses_post( $args['after_title'] );
            }
            ?>

            <ul>
                <?php
                $recent_posts = wp_get_recent_posts( array('numberposts' => $count, 'suppress_filters' => false,'post_status' => 'publish') );
                foreach( $recent_posts as $recent ):
                    ?>
                    <li>
                        <?php echo get_the_post_thumbnail( $recent["ID"], 'nfw_image_70_70' ); ?>
                        <a href="<?php echo esc_url( get_the_permalink( $recent["ID"] ) ); ?>"><?php echo esc_html( substr( $recent["post_title"], 0, 40 ) ); ?></a>
                        <span class="post-date"><?php echo esc_html( get_the_time( get_option( 'date_format' ), $recent["ID"] ) ); ?></span>
                    </li>
                    <?php
                endforeach;
                ?>

            </ul>
        </div>

        <?php
        echo wp_kses_post( $args['after_widget'] );
    }

    public function form( $instance ){
        if( isset( $instance['title'] ) ){
            $title = $instance['title'];
        } else{
            $title = __( 'Blog', 'gold_domain' );
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'title' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'title' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $title ); ?>" />
        </p>

        <?php
        if( isset( $instance['count'] ) ){
            $count = $instance['count'];
        } else{
            $count = '';
        }
        ?>
        <p>
            <label for="<?php esc_attr_e( $this->get_field_id( 'count' ) ); ?>"><?php _e( 'Number of posts:', 'gold_domain' ); ?></label>
            <input class="widefat" 
                   id="<?php esc_attr_e( $this->get_field_id( 'count' ) ); ?>" 
                   name="<?php esc_attr_e( $this->get_field_name( 'count' ) ); ?>" 
                   type="text" 
                   value="<?php esc_attr_e( $count ); ?>" />
        </p>

        <?php
    }

    public function update( $new_instance, $old_instance ){
        $instance = array();
        $instance['title'] = (!empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['count'] = (!empty( $new_instance['count'] ) ) ? strip_tags( $new_instance['count'] ) : '';
        return $instance;
    }

}
