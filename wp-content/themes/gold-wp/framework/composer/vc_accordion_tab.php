<?php

$output = $title = '';

extract( shortcode_atts( array(
    'title' => __( "Section", "gold_domain" ),
    'nfw_accordion_toggles_icon' => '',
    'nfw_accordion_toggles_color' => 'accent-color-1'
                ), $atts ) );

$container_class = 'nfwaccordiontoggleelementsclass';
$accordion_tab_icon = '';
if( $nfw_accordion_toggles_icon != '' ){
    $accordion_tab_icon = '<span><i class="' . $nfw_accordion_toggles_icon . '"></i></span>';
}

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_accordion_section group', $this->settings['base'], $atts );
$output .= "\n\t\t\t" . '<div class="' . $container_class . '-item ' . esc_attr( $nfw_accordion_toggles_color ) . '">';
$output .= "\n\t\t\t\t" . '<a href="#" class="' . $container_class . '-item-toggle">' . $accordion_tab_icon . wp_kses_post( $title ) . '</a>';
$output .= "\n\t\t\t\t" . '<div class="' . $container_class . '-item-content">';
$output .= ($content == '' || $content == ' ') ? __( "Empty section. Edit page to add content here.", "gold_domain" ) : "\n\t\t\t\t" . wpb_js_remove_wpautop( $content );
$output .= "\n\t\t\t\t" . '</div>';
$output .= "\n\t\t\t" . '</div> ' . $this->endBlockComment( '.wpb_accordion_section' ) . "\n";

echo $output;
