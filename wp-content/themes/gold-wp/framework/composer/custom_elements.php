<?php

// Directs VC to the templates used for accordion and tabs
vc_set_shortcodes_templates_dir( get_stylesheet_directory() . '/framework/composer/' );

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM ACCORDION TOGGLES ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$settings_accordion_tab = array(
    "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
    "category" => __( "Gold Elements", 'gold_domain' ),
    "params" => array(
        array(
            'type' => 'textfield',
            'heading' => __( 'Title', 'gold_domain' ),
            'param_name' => 'title',
            'description' => __( 'Accordion section title.', 'gold_domain' )
        ),
        array(
            "type" => "nfw_icomoon_icons_param",
            "heading" => __( "Select icon", 'gold_domain' ),
            "param_name" => "nfw_accordion_toggles_icon",
            "description" => __( "Select the icon you want", 'gold_domain' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Icon Color', 'gold_domain' ),
            'param_name' => 'nfw_accordion_toggles_color',
            'value' => array(
                __( 'Default', 'gold_domain' ) => 'accent-color-1',
                __( 'Color 2', 'gold_domain' ) => 'accent-color-2',
                __( 'Color 3', 'gold_domain' ) => 'accent-color-3',
                __( 'Color 4', 'gold_domain' ) => 'accent-color-4'
            ),
            'description' => __( 'Specify the color of the icon', 'gold_domain' )
        )
    )
);

$settings_accordion = array(
    "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
    "category" => __( "Gold Elements", 'gold_domain' ),
    "weight" => -1,
    "params" => array(
        array(
            'type' => 'nfw_radio_accordion_toggle_param',
            'heading' => __( 'Select Type', 'gold_domain' ),
            'param_name' => 'nfw_accordion_toggles_container_type',
            'value' => '1'
        )
    )
);

vc_map_update( 'vc_accordion_tab', $settings_accordion_tab );
vc_map_update( 'vc_accordion', $settings_accordion );

// Generates the radio options parameters
function nfw_radio_accordion_toggle_param_settings_field( $settings, $value ){
    $dependency = vc_generate_dependencies_attributes( $settings );
    $check_1 = $check_2 = "";

    switch( $value ){
        case 1:
            $check_1 = " checked";
            break;
        case 2:
            $check_2 = " checked";
            break;
    }

    // Generates the radio options
    return '<div class="nfw_radio_style_param_block">'
            . '<input id="nfw_selected_radio_style" type="hidden" name="' . esc_attr( $settings ['param_name'] ) . '" value="' . esc_attr( $value ) . '" ' . esc_html( $dependency ) . ''
            . 'class="wpb_vc_param_value wpb-radio ' . esc_attr( $settings['param_name'] ) . ' ' . esc_attr( $settings['type'] ) . '_field" >'
            . '<input type="radio" name="radio_stylecheck" value="1" ' . $check_1 . ' onclick="nfw_radio_style_option(this)">Accordion'
            . '<input type="radio" name="radio_stylecheck" value="2" ' . $check_2 . ' onclick="nfw_radio_style_option(this)">Toggle</div>';
}

add_shortcode_param( 'nfw_radio_accordion_toggle_param', 'nfw_radio_accordion_toggle_param_settings_field' );

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM TABS ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$settings_tab = array(
    "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
    "category" => __( "Gold Elements", 'gold_domain' ),
    "params" => array(
        array(
            'type' => 'textfield',
            'heading' => __( 'Title', 'gold_domain' ),
            'param_name' => 'title',
            'description' => __( 'Tab title.', 'gold_domain' )
        ),
        array(
            "type" => "nfw_icomoon_icons_param",
            "heading" => __( "Select tab icon", 'gold_domain' ),
            "param_name" => "nfw_vctabs_icon",
            "description" => __( "Select the icon you want", 'gold_domain' )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __( 'Icon Color', 'gold_domain' ),
            'param_name' => 'nfw_vctabs_color',
            'value' => array(
                __( 'Default', 'gold_domain' ) => 'accent-color-1',
                __( 'Color 2', 'gold_domain' ) => 'accent-color-2',
                __( 'Color 3', 'gold_domain' ) => 'accent-color-3',
                __( 'Color 4', 'gold_domain' ) => 'accent-color-4'
            ),
            'description' => __( 'Specify the color of the icon', 'gold_domain' )
        ),
        array(
            'type' => 'tab_id',
            'heading' => __( 'Tab ID', 'gold_domain' ),
            'param_name' => "tab_id"
        )
    )
);

$settings_tabs = array(
    "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
    "category" => __( "Gold Elements", 'gold_domain' ),
    "weight" => -2,
    "params" => array(array(
            'type' => 'nfw_radio_orientation_param',
            'heading' => __( 'Select Tabs orientation', 'gold_domain' ),
            'param_name' => 'nfw_vctabs_container_orientation',
            'value' => '1'
        ))
);

vc_map_update( 'vc_tab', $settings_tab );
vc_map_update( 'vc_tabs', $settings_tabs );

// Generates the radio options parameters
function nfw_radio_orientation_param_settings_field( $settings, $value ){
    $dependency = vc_generate_dependencies_attributes( $settings );
    $check_1 = $check_2 = "";

    switch( $value ){
        case 1:
            $check_1 = " checked";
            break;
        case 2:
            $check_2 = " checked";
            break;
    }

    // Generates the radio options
    return '<div class="nfw_radio_style_param_block">'
            . '<input id="nfw_selected_radio_style" type="hidden" name="' . esc_attr( $settings ['param_name'] ) . '" value="' . esc_attr( $value ) . '" ' . esc_html( $dependency ) . ''
            . 'class="wpb_vc_param_value wpb-radio ' . esc_attr( $settings['param_name'] ) . ' ' . esc_attr( $settings['type'] ) . '_field" >'
            . '<input type="radio" name="radio_stylecheck" value="1" ' . $check_1 . ' onclick="nfw_radio_style_option(this)">Horizontal'
            . '<input type="radio" name="radio_stylecheck" value="2" ' . $check_2 . ' onclick="nfw_radio_style_option(this)">Vertical</div>';
}

add_shortcode_param( 'nfw_radio_orientation_param', 'nfw_radio_orientation_param_settings_field' );

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM ALERT ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function nfw_alert_elements_func( $atts, $content = null ){
    extract( shortcode_atts( array(
        'nfw_alert_type' => 'info',
        'nfw_content' => '',
                    ), $atts ) );

    $content = esc_html( $content );

    // Determines the type of alert
    if( $nfw_alert_type == "info" ){
        return "<div class='alert info'><i class='ifc-info'></i>{$content}</div>";
    } elseif( $nfw_alert_type == "success" ){
        return "<div class='alert success'><i class='ifc-checkmark'></i>{$content}</div>";
    } elseif( $nfw_alert_type == "warning" ){
        return "<div class='alert warning'><i class='ifc-error'></i>{$content}</div>";
    } elseif( $nfw_alert_type == "error" ){
        return "<div class='alert error'><i class='ifc-close'></i>{$content}</div>";
    } else{
        return "<div class='alert'>{$content}</div>";
    }
}

add_shortcode( 'nfw_alert_elements', 'nfw_alert_elements_func' );

add_action( 'init', 'nfw_integrate_vc_alert_elements' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_alert_elements(){
    vc_map(
            array(
                "name" => __( "Message box", 'gold_domain' ),
                "base" => "nfw_alert_elements",
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "category" => __( "Gold Elements", 'gold_domain' ),
                "params" => array(
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Message Type', 'gold_domain' ),
                        'param_name' => 'nfw_alert_type',
                        'value' => array(
                            __( 'Info', 'gold_domain' ) => 'info',
                            __( 'Default', 'gold_domain' ) => 'default',
                            __( 'Success', 'gold_domain' ) => 'success',
                            __( 'Warning', 'gold_domain' ) => 'warning',
                            __( 'Error', 'gold_domain' ) => 'error',
                        ),
                        'description' => __( 'Specify', 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Message", 'gold_domain' ),
                        "param_name" => "content",
                        "value" => "",
                        "description" => __( "Specify the content of the message", 'gold_domain' )
                    )
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM DIVIDER ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function nfw_dividers_func( $atts ){
    extract( shortcode_atts( array(
        'nfw_divider_type' => 'single-line'
                    ), $atts ) );

    $nfw_divider_type = esc_attr( $nfw_divider_type );

    return "<div class='divider {$nfw_divider_type}'></div>";
}

add_shortcode( 'nfw_dividers', 'nfw_dividers_func' );

add_action( 'init', 'nfw_integrate_vc_dividers' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_dividers(){
    vc_map(
            array(
                "name" => __( "Divider", 'gold_domain' ),
                "base" => "nfw_dividers",
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "category" => __( "Gold Elements", 'gold_domain' ),
                "params" => array(
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Divider Type', 'gold_domain' ),
                        'param_name' => 'nfw_divider_type',
                        'value' => array(
                            __( 'Single Line', 'gold_domain' ) => 'single-line',
                            __( 'Double Line', 'gold_domain' ) => 'double-line',
                            __( 'Single Dotted', 'gold_domain' ) => 'single-dotted',
                            __( 'Double Dotted', 'gold_domain' ) => 'double-dotted'
                        ),
                        'description' => __( 'Specify', 'gold_domain' )
                    )
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM MILESTONE ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function nfw_milestones_func( $atts, $content = null ){ // New function parameter $content is added!
    extract( shortcode_atts( array(
        'nfw_milestone_stop' => '1',
        'nfw_milestone_speed' => '2000'
                    ), $atts ) );

    $content = wp_kses_post( $content );

    // Validates the numeric values
    if( preg_match( "/[^0-9]/", $nfw_milestone_stop ) ){
        $nfw_milestone_stop = 0;
    }
    if( preg_match( "/[^0-9]/", $nfw_milestone_speed ) ){
        $nfw_milestone_speed = 0;
    }

    return "<div class='milestone'>

                        <div class='milestone-content'>
                            <span class='milestone-value' data-stop='{$nfw_milestone_stop}' data-speed='{$nfw_milestone_speed}'></span>
                            <div class='milestone-description'>{$content}</div>
                        </div>
                       
                    </div>";
}

add_shortcode( 'nfw_milestones', 'nfw_milestones_func' );

add_action( 'init', 'nfw_integrate_vc_milestones' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_milestones(){
    vc_map(
            array(
                "name" => __( "Milestone", 'gold_domain' ),
                "base" => "nfw_milestones",
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "category" => __( "Gold Elements", 'gold_domain' ),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "heading" => __( "Number", 'gold_domain' ),
                        "param_name" => "nfw_milestone_stop",
                        "value" => "1",
                        "description" => __( "The final value will animate to, from 0 to the number provided by you", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Speed", 'gold_domain' ),
                        "param_name" => "nfw_milestone_speed",
                        "value" => "2000",
                        "description" => __( "Specify the animation speed", 'gold_domain' )
                    ),
                    array(
                        "type" => "textarea_html",
                        "heading" => __( "Milestone Details", 'gold_domain' ),
                        "param_name" => "content"
                    )
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM ICON BOX ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// the short code function for the icon box elements
function nfw_icon_boxes_func( $atts, $content = null ){ // New function parameter $content is added!
    extract( shortcode_atts( array(
        'nfw_icon_box_title' => '',
        'nfw_icon_box_icon' => '',
        'nfw_icon_box_style' => '1',
        'nfw_icon_box_color' => 'accent-color-1',
        'nfw_icon_box_link' => ''
                    ), $atts ) );

    $nfw_icon_box_title = esc_html( $nfw_icon_box_title );
    $nfw_icon_box_icon = esc_attr( $nfw_icon_box_icon );
    $nfw_icon_box_color = esc_attr( $nfw_icon_box_color );
    $newtab = $no_link_a = $no_link_b = "";
    $content_p = wp_kses_post( $content );
    if( trim( $content ) != '' ){
        $content_p = "<p>{$content}</p>";
    }

    $icon_box_icon = "";
    if( trim( $nfw_icon_box_icon ) != "" ){
        $icon_box_icon = "<i class='{$nfw_icon_box_icon}'></i>";
    }
    $link_details = vc_build_link( $nfw_icon_box_link );

    $link_details['target'] = esc_attr( $link_details['target'] );

    if( $link_details['target'] !== '' ){
        $newtab = "target='{$link_details['target']}'";
    }

    $link_details['url'] = esc_url( $link_details['url'] );

    $link_details['title'] = esc_html( $link_details['title'] );

    if( trim( $link_details['title'] ) == '' ){
        $no_link_a = "<div style='display:none;'>";
        $no_link_b = "</div>";
    }

    if( $nfw_icon_box_style == 1 ){
        $output = "<div class='icon-box-1 {$nfw_icon_box_color}'>
                        
                        {$icon_box_icon}
  
                        <div class='icon-box-content'>
                        
                        	<h4>
                            	<a href='{$link_details['url']}' {$newtab}>{$nfw_icon_box_title}</a>
                            </h4>
                        	
                            {$content_p}
                            
                            {$no_link_a}
                            <a href='{$link_details['url']}' {$newtab}>
                            	{$link_details['title']}
                            	<i class='ifc-right'></i>
                            </a>
                            {$no_link_b}
                        </div>
                        
                    </div>";
    } else if( $nfw_icon_box_style == 2 ){
        $output = "<div class='icon-box-2 {$nfw_icon_box_color}'>
                        
                        <span>
                        	{$icon_box_icon}
                        </span>
  
                        <div class='icon-box-content'>
                        
                        	<h2>
                            	<a href='{$link_details['url']}' {$newtab}>{$nfw_icon_box_title}</a>
                            </h2>
                        	
                            {$content_p}
                            {$no_link_a}
                            <a href='{$link_details['url']}' {$newtab}>
                            	{$link_details['title']}
                                <i class='ifc-right'></i>
                            </a>
                            {$no_link_b}
                        </div>
                        
                    </div>";
    } else if( $nfw_icon_box_style == 3 ){
        $output = "<div class='icon-box-3 {$nfw_icon_box_color}'>
                        
                        {$icon_box_icon}
                        
                        <h4>
                            <a href='{$link_details['url']}' {$newtab}>{$nfw_icon_box_title}</a>
                        </h4>
                        
                        <br class='clear'>
                        
                        {$content_p}
                        {$no_link_a}
                        <a href='{$link_details['url']}' {$newtab}>
                            	{$link_details['title']}
                            <i class='ifc-right'></i>
                        </a>
                        {$no_link_b}
                    </div>";
    } else if( $nfw_icon_box_style == 4 ){
        $output = "<div class='icon-box-4 {$nfw_icon_box_color}'>
                        
                        {$icon_box_icon}
  
                        <div class='icon-box-content'>
                        
                        	<h4>
                            	 <a href='{$link_details['url']}' {$newtab}>{$nfw_icon_box_title}</a>
                            </h4>
                        	
                            {$content_p}
                            {$no_link_a}
                            <a href='{$link_details['url']}' {$newtab}>
                            	{$link_details['title']}
                                <i class='ifc-right'></i>
                            </a>
                            {$no_link_b}
                        </div>
                        
                    </div>";
    } else if( $nfw_icon_box_style == 5 ){
        $output = "<div class='icon-box-5 {$nfw_icon_box_color}'>
                                
                        {$icon_box_icon}
  
                        <div class='icon-box-content'>
                        
                            <h4>{$content}</h4>
                        
                        </div>
                        
                    </div>";
    } else{
        $output = "<div class='icon-box-6 {$nfw_icon_box_color}'>
                        
                        <span>
                            {$icon_box_icon}
                        </span>
  
                        <div class='icon-box-content'>
                        
                            <h4>
                                <a href='{$link_details['url']}' {$newtab}>{$nfw_icon_box_title}</a>
                            </h4>
                            
                            {$content_p}
                            {$no_link_a}
                            <p>
                                <a href='{$link_details['url']}' {$newtab}>
                            	{$link_details['title']}
                                    <i class='ifc-right'></i>
                                </a>
                            </p>
                            {$no_link_b}
                        </div>
                        
                    </div>";
    }

    return $output;
}

add_shortcode( 'nfw_icon_boxes', 'nfw_icon_boxes_func' );
add_action( 'init', 'nfw_integrate_vc_icon_boxes' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_icon_boxes(){
    vc_map(
            array(
                "name" => __( "Icon Box", 'gold_domain' ),
                "base" => "nfw_icon_boxes",
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "category" => __( "Gold Elements", 'gold_domain' ),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "heading" => __( "Title", 'gold_domain' ),
                        "param_name" => "nfw_icon_box_title",
                        "value" => __( "Title", 'gold_domain' ),
                        "description" => __( "The service title", 'gold_domain' )
                    ),
                    array(
                        "type" => "nfw_icomoon_icons_param",
                        "heading" => __( "Select icon", 'gold_domain' ),
                        "param_name" => "nfw_icon_box_icon",
                        "description" => __( "Select the icon you want", 'gold_domain' )
                    ),
                    array(
                        "type" => "nfw_radio_style_param",
                        "heading" => __( "Iconbox Style", 'gold_domain' ),
                        "param_name" => "nfw_icon_box_style",
                        "value" => "1",
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Icon Color', 'gold_domain' ),
                        'param_name' => 'nfw_icon_box_color',
                        'value' => array(
                            __( 'Default', 'gold_domain' ) => 'accent-color-1',
                            __( 'Color 2', 'gold_domain' ) => 'accent-color-2',
                            __( 'Color 3', 'gold_domain' ) => 'accent-color-3',
                            __( 'Color 4', 'gold_domain' ) => 'accent-color-4'
                        ),
                        'description' => __( 'Specify the color of the icon', 'gold_domain' )
                    ),
                    array(
                        "type" => "vc_link",
                        "heading" => __( "Read more link", 'gold_domain' ),
                        "param_name" => "nfw_icon_box_link",
                        "description" => __( "Specify an optional link pointing to a details page", 'gold_domain' )
                    ),
                    array(
                        "type" => "textarea_html",
                        "heading" => __( "Content", 'gold_domain' ),
                        "param_name" => "content",
                        "description" => __( "Add description text for the service", 'gold_domain' )
                    )
                )
            )
    );
}

// Generates the radio options parameters
function nfw_radio_style_param_settings_field( $settings, $value ){
    $dependency = vc_generate_dependencies_attributes( $settings );
    $check_1 = $check_2 = $check_3 = $check_4 = $check_5 = $check_6 = "";

    switch( $value ){
        case 1:
            $check_1 = " checked";
            break;
        case 2:
            $check_2 = " checked";
            break;
        case 3:
            $check_3 = " checked";
            break;
        case 4:
            $check_4 = " checked";
            break;
        case 5:
            $check_5 = " checked";
            break;
        case 6:
            $check_6 = " checked";
            break;
    }

    // Generates the radio options
    return '<div class="nfw_radio_style_param_block">'
            . '<input id="nfw_selected_radio_style" type="hidden" name="' . esc_attr( $settings ['param_name'] ) . '" value="' . esc_attr( $value ) . '" ' . esc_html( $dependency ) . ''
            . 'class="wpb_vc_param_value wpb-radio ' . esc_attr( $settings['param_name'] ) . ' ' . esc_attr( $settings['type'] ) . '_field" >'
            . '<input type="radio" name="radio_stylecheck" value="1" ' . $check_1 . ' onclick="nfw_radio_style_option(this)">Style 1 '
            . '<input type="radio" name="radio_stylecheck" value="2" ' . $check_2 . ' onclick="nfw_radio_style_option(this)">Style 2 '
            . '<input type="radio" name="radio_stylecheck" value="3" ' . $check_3 . ' onclick="nfw_radio_style_option(this)">Style 3 '
            . '<input type="radio" name="radio_stylecheck" value="4" ' . $check_4 . ' onclick="nfw_radio_style_option(this)">Style 4 '
            . '<input type="radio" name="radio_stylecheck" value="5" ' . $check_5 . ' onclick="nfw_radio_style_option(this)">Style 5 '
            . '<input type="radio" name="radio_stylecheck" value="6" ' . $check_6 . ' onclick="nfw_radio_style_option(this)">Style 6</div>';
}

add_shortcode_param( 'nfw_radio_style_param', 'nfw_radio_style_param_settings_field' );

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM GOOGLE MAP ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// the short code function for the google maps elements
function nfw_gmaps_func( $atts ){
    extract( shortcode_atts( array(
        'nfw_gmaps_zoom' => '16',
        'nfw_gmaps_address' => '',
        'nfw_gmaps_caption' => ''
                    ), $atts ) );

    // Numeric validation
    if( preg_match( "/[^0-9]/", $nfw_gmaps_zoom ) ){
        $nfw_gmaps_zoom = 0;
    }

    $nfw_gmaps_address = esc_attr( $nfw_gmaps_address );
    $nfw_gmaps_caption = esc_attr( $nfw_gmaps_caption );

    return "<div class='google-map map' 
                    	data-zoom='{$nfw_gmaps_zoom}' 
                    	data-address='{$nfw_gmaps_address}' 
                    	data-caption='{$nfw_gmaps_caption}' >
                        <p>This will be replaced with the Google Map.</p>
                    </div>";
}

add_shortcode( 'nfw_gmaps', 'nfw_gmaps_func' );

add_action( 'init', 'nfw_integrate_vc_gmaps' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_gmaps(){
    vc_map(
            array(
                "name" => __( "Google Map", 'gold_domain' ),
                "base" => "nfw_gmaps",
                "category" => __( "Gold Elements", 'gold_domain' ),
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "params" => array(
                    array(
                        "type" => "textfield",
                        "heading" => __( "Address", 'gold_domain' ),
                        "param_name" => "nfw_gmaps_address",
                        "value" => "",
                        "description" => __( "The address where the map is centered", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Pinpoint caption", 'gold_domain' ),
                        "param_name" => "nfw_gmaps_caption",
                        "value" => "",
                        "description" => __( "Specify text that will appear when you click on the pin", 'gold_domain' )
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Zoom', 'gold_domain' ),
                        'param_name' => 'nfw_gmaps_zoom',
                        'value' => '16',
                        'description' => __( 'Specify a zoom value between 5 and 20', 'gold_domain' )
                    ),
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM PIE CHART ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// the short code function for the pie charts VC elements
function nfw_pie_chart_func( $atts, $content ){ // New function parameter $content is added!
    extract( shortcode_atts( array(
        'nfw_data_barcolor' => '#80a852',
        'nfw_data_trackcolor' => '#d7d7d7',
        'nfw_pie_chart_percent' => '0',
        'nfw_pie_chart_linewidth' => '5',
        'nfw_pie_chart_barsize' => '200',
        'nfw_pie_chart_text' => ''
                    ), $atts ) );

    $nfw_pie_chart_text = esc_html( $nfw_pie_chart_text );
    $content = wp_kses_post( $content );
    $nfw_data_barcolor = esc_attr( $nfw_data_barcolor );
    $nfw_data_trackcolor = esc_attr( $nfw_data_trackcolor );

    if( preg_match( "/[^0-9]/", $nfw_pie_chart_percent ) ){
        $nfw_pie_chart_percent = 0;
    }
    if( preg_match( "/[^0-9]/", $nfw_pie_chart_linewidth ) ){
        $nfw_pie_chart_linewidth = 0;
    }
    if( preg_match( "/[^0-9]/", $nfw_pie_chart_barsize ) ){
        $nfw_pie_chart_barsize = 0;
    }

    $output = "<div class='pie-chart' "
            . "data-percent='{$nfw_pie_chart_percent}' "
            . "data-barColor='{$nfw_data_barcolor}' "
            . "data-trackColor='{$nfw_data_trackcolor}' "
            . "data-lineWidth='{$nfw_pie_chart_linewidth}' "
            . "data-barSize='{$nfw_pie_chart_barsize}'>"
            . "<span class='pie-chart-custom-text'>{$nfw_pie_chart_text}</span>";

    $output.="</div><div class='pie-chart-description'>{$content}</div>";

    return $output;
}

add_shortcode( 'nfw_pie_chart', 'nfw_pie_chart_func' );
add_action( 'init', 'nfw_integrate_vc_pie_chart' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_pie_chart(){
    vc_map(
            array(
                "name" => __( "Pie Chart", 'gold_domain' ),
                "base" => "nfw_pie_chart",
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "category" => __( "Gold Elements", 'gold_domain' ),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "heading" => __( "Chart Text", 'gold_domain' ),
                        "param_name" => "nfw_pie_chart_text",
                        "value" => __( "text", 'gold_domain' ),
                        "description" => __( "Specify chart text", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Chart percent", 'gold_domain' ),
                        "param_name" => "nfw_pie_chart_percent",
                        "value" => "0",
                        "description" => __( "Specify percent", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Line Width", 'gold_domain' ),
                        "param_name" => "nfw_pie_chart_linewidth",
                        "value" => "5",
                        "description" => __( "Specify line width", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Bar Size", 'gold_domain' ),
                        "param_name" => "nfw_pie_chart_barsize",
                        "value" => "200",
                        "description" => __( "Specify bar size", 'gold_domain' )
                    ),
                    array(
                        "type" => "colorpicker",
                        "heading" => __( "Bar Color", 'gold_domain' ),
                        "param_name" => "nfw_data_barcolor",
                        "value" => '#80a852',
                        "description" => __( "Specify Bar-Color", 'gold_domain' )
                    ),
                    array(
                        "type" => "colorpicker",
                        "holder" => "div",
                        "class" => "piechart_options_display_trigger",
                        "heading" => __( "Track Color", 'gold_domain' ),
                        "param_name" => "nfw_data_trackcolor",
                        "value" => '#d7d7d7',
                        "description" => __( "Specify Track-Color", 'gold_domain' )
                    ),
                    array(
                        "type" => "textarea_html",
                        "heading" => __( "Content", 'gold_domain' ),
                        "param_name" => "content",
                        "description" => __( "Add description text for the pie chart", 'gold_domain' )
                    ),
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM PROGRESS BAR ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// the short code function for the progress bar VC elements
function nfw_progress_bar_func( $atts, $content = null ){ // New function parameter $content is added!
    extract( shortcode_atts( array(
        'nfw_progress_bar_data_width' => '90',
        'nfw_progress_bar_color' => 'accent-color-1'
                    ), $atts ) );

    $content = esc_html( $content );
    $nfw_progress_bar_color = esc_attr( $nfw_progress_bar_color );

    if( preg_match( "/[^0-9]/", $nfw_progress_bar_data_width ) ){
        $nfw_progress_bar_data_width = 0;
    }

    $output = "<div class='progress-bar-description'>
                        	{$content}
                            <span style='left:{$nfw_progress_bar_data_width}%'>{$nfw_progress_bar_data_width}%</span>
                        </div>
                       
                        <div class='progress-bar {$nfw_progress_bar_color}'>  
                        	<span class='progress-bar-outer' data-width='{$nfw_progress_bar_data_width}'> 
                            	<span class='progress-bar-inner'></span> 
                            </span>
                        </div>";

    return $output;
}

add_shortcode( 'nfw_progress_bar', 'nfw_progress_bar_func' );

add_action( 'init', 'nfw_integrate_vc_progress_bar' );

// integrates the progess bar element in the visual composer
function nfw_integrate_vc_progress_bar(){
    vc_map(
            array(
                "name" => __( "Progress Bar", 'gold_domain' ),
                "base" => "nfw_progress_bar",
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "category" => __( "Gold Elements", 'gold_domain' ),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "heading" => __( "Title", 'gold_domain' ),
                        "param_name" => "content",
                        "value" => __( "title", 'gold_domain' ),
                        "description" => __( "The title of the progress bar", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Value", 'gold_domain' ),
                        "param_name" => "nfw_progress_bar_data_width",
                        "value" => "90",
                        "description" => __( "Specify a value between 1 and 100, it represents the loaded percentage", 'gold_domain' )
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Bar Color', 'gold_domain' ),
                        'param_name' => 'nfw_progress_bar_color',
                        'value' => array(
                            __( 'Default', 'gold_domain' ) => 'accent-color-1',
                            __( 'Color 2', 'gold_domain' ) => 'accent-color-2',
                            __( 'Color 3', 'gold_domain' ) => 'accent-color-3',
                            __( 'Color 4', 'gold_domain' ) => 'accent-color-4'
                        ),
                        'description' => __( 'Select the color of the bar', 'gold_domain' )
                    )
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM SOCIAL MEDIA ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// the short code function for the custom element
function nfw_social_media_func( $atts ){
    extract( shortcode_atts( array(
        'nfw_social_icon' => '',
        'nfw_social_link' => ''
                    ), $atts ) );

    $output = '';

    $nfw_social_icon = esc_attr( $nfw_social_icon );

    $icon_parts = explode( '-', $nfw_social_icon );

    $icon = $icon_parts[1];

    if( $icon == 'stack' ){
        $icon.='-' . $icon_parts[2];
    }

        $nfw_social_link = esc_url( $nfw_social_link );
        $output.= "<a href='{$nfw_social_link}' class='{$icon}-icon social-icon' target='_blank'>
                                <i class='{$nfw_social_icon}'></i>
                        </a>";
    return $output;
}

add_shortcode( 'nfw_social_media', 'nfw_social_media_func' );
add_action( 'init', 'nfw_integrate_vc_social_media' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_social_media(){
    vc_map( array(
        "name" => __( "Social Media", 'gold_domain' ),
        "base" => "nfw_social_media",
        "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
        "category" => __( "Gold Elements", 'gold_domain' ),
        "params" => array(
            array(
                "type" => "nfw_fa_icons_param",
                "heading" => __( "Social icon", 'gold_domain' ),
                "param_name" => "nfw_social_icon",
                "value" => "",
                "description" => __( "Select social icon", 'gold_domain' )
            ),
            array(
                "type" => "textfield",
                "heading" => __( "Social link", 'gold_domain' ),
                "param_name" => "nfw_social_link",
                "value" => "",
                "description" => __( "Specify social link", 'gold_domain' )
            )
        )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM TESTIMONIAL ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function nfw_testimonial_func( $atts, $content = null ){ // New function parameter $content is added!
    extract( shortcode_atts( array(
        'nfw_testimonial_name' => '',
        'nfw_testimonial_title' => '',
        'nfw_testimonial_color' => 'accent-color-1',
        'nfw_testimonial_large' => '',
        'nfw_testimonial_image' => ''
                    ), $atts ) );

    $content = esc_html( $content );
    $nfw_testimonial_name = esc_html( $nfw_testimonial_name );
    $nfw_testimonial_title = esc_html( $nfw_testimonial_title );
    $nfw_testimonial_color = esc_attr( $nfw_testimonial_color );

    $extra_class = ' no-space';
    $image_size = 'nfw_image_100_100';

    if( $nfw_testimonial_large == "large" ){
        $image_size = 'nfw_image_135_135';
        $extra_class = '';
    }

    if( $nfw_testimonial_image == null || trim( $nfw_testimonial_image ) == '' ){
        $image_area = "";
    } else{
        $nfw_image_id = filter_var( $nfw_testimonial_image, FILTER_SANITIZE_NUMBER_INT );

        $image_data = wp_get_attachment_image_src( $nfw_image_id, $image_size );
        $image_url = esc_url( $image_data[0] );

        $image_area = "<img src='{$image_url}' alt=''>";
    }

    $output = "<div class='testimonial {$nfw_testimonial_color} fixed{$extra_class}'>
                                
                        <span>
                            {$image_area}
                        </span>
                        
                        <blockquote>
                            
                            <h4>
                                <strong>{$nfw_testimonial_name}, </strong>
                                <span>{$nfw_testimonial_title}</span>
                            </h4>
                            
                            <p>{$content}</p>
                            
                        </blockquote>
                        
                    </div>";

    return $output;
}

add_shortcode( 'nfw_testimonial', 'nfw_testimonial_func' );

add_action( 'init', 'nfw_integrate_vc_testimonial' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_testimonial(){
    vc_map(
            array(
                "name" => __( "Testimonial", 'gold_domain' ),
                "base" => "nfw_testimonial",
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "category" => __( "Gold Elements", 'gold_domain' ),
                "params" => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => __( 'Image', 'gold_domain' ),
                        'param_name' => 'nfw_testimonial_image',
                        'description' => __( 'Add an image for the testimonial', 'gold_domain' )
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Image size', 'gold_domain' ),
                        'param_name' => 'nfw_testimonial_large',
                        'value' => array(__( 'Large', 'gold_domain' ) => 'large')
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Name", 'gold_domain' ),
                        "param_name" => "nfw_testimonial_name",
                        "value" => __( "Sample name", 'gold_domain' ),
                        "description" => __( "Specify the name of the testimonial author", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Job Title", 'gold_domain' ),
                        "param_name" => "nfw_testimonial_title",
                        "value" => __( "Sample Title", 'gold_domain' ),
                        "description" => __( "Specify the job of the testimonial author", 'gold_domain' )
                    ),
                    array(
                        "type" => "textarea",
                        "heading" => __( "Testimonial", 'gold_domain' ),
                        "param_name" => "content",
                        "value" => "",
                        "description" => __( "Specify the text of the testimonial", 'gold_domain' )
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Testimonial Color', 'gold_domain' ),
                        'param_name' => 'nfw_testimonial_color',
                        'value' => array(
                            __( 'Default', 'gold_domain' ) => 'accent-color-1',
                            __( 'Color 2', 'gold_domain' ) => 'accent-color-2',
                            __( 'Color 3', 'gold_domain' ) => 'accent-color-3',
                            __( 'Color 4', 'gold_domain' ) => 'accent-color-4'
                        ),
                        'description' => __( 'Specify the color of the testimonial', 'gold_domain' )
                    )
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM PRICING TABLE ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function nfw_pricing_table_func( $atts ){ // New function parameter $content is added!
    extract( shortcode_atts( array(
        'nfw_pricing_table_price' => '30',
        'nfw_pricing_table_name' => __( "Starter edition", 'gold_domain' ),
        'nfw_pricing_table_specifications' => '',
        'nfw_pricing_table_color' => 'accent-color-1',
        'nfw_pricing_table_price_type' => "$/mo",
        'nfw_pricing_table_link' => ''
                    ), $atts ) );

    $link_details = vc_build_link( $nfw_pricing_table_link );

    $specifications_elements = mb_split( '<br />', $nfw_pricing_table_specifications );
    $nfw_pricing_table_name = esc_html( $nfw_pricing_table_name );
    $nfw_pricing_table_price = esc_html( $nfw_pricing_table_price );
    $nfw_pricing_table_color = esc_attr( $nfw_pricing_table_color );
    $nfw_pricing_table_price_type = esc_html( $nfw_pricing_table_price_type );
    $newtab = $nfw_price_type_1 = $nfw_price_type_2 = "";

    $link_details['target'] = esc_attr( $link_details['target'] );
    $link_details['url'] = esc_url( $link_details['url'] );
    $link_details['title'] = esc_html( $link_details['title'] );

    if( $link_details['target'] !== '' ){
        $newtab = "target='{$link_details['target']}'";
    }

    $nfw_price_types = explode( "/", $nfw_pricing_table_price_type );

    // Handles the cases where the price is with , or .
    if( isset( $nfw_price_types[1] ) ){
        $nfw_price_type_1 = "<sup>" . esc_html( $nfw_price_types[0] ) . "</sup>";
        $nfw_price_type_2 = "<span>/" . esc_html( $nfw_price_types[1] ) . "</span>";
    } else{
        $nfw_price_type_1 = esc_html( $nfw_pricing_table_price_type );
    }

    $specifications_list = "";

    // Loops through each specifications row
    foreach( $specifications_elements as $element ){
        $specifications_list.='<li>' . esc_html( $element ) . '</li>';
    }

    $output = "<div class='pricing-table {$nfw_pricing_table_color}'>
                    
                    	<h1>{$nfw_pricing_table_name}</h1>
                        
                        <div class='pricing-table-header'>                          
                            <h1>
                            	{$nfw_pricing_table_price}
                            	{$nfw_price_type_1}
                            	{$nfw_price_type_2}
                            </h1>
                        </div>
                        
                        <div class='pricing-table-offer'>
                            
                            <ul>
                                {$specifications_list}
                            </ul>
                            
                        </div>
                        
                        <a class='btn' href='{$link_details['url']}' {$newtab}>{$link_details['title']}</a>   
                        
                    </div>";

    return $output;
}

add_shortcode( 'nfw_pricing_table', 'nfw_pricing_table_func' );

add_action( 'init', 'nfw_integrate_vc_pricing_table' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_pricing_table(){
    vc_map(
            array(
                "name" => __( "Pricing Table", 'gold_domain' ),
                "base" => "nfw_pricing_table",
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "category" => __( "Gold Elements", 'gold_domain' ),
                "params" => array(
                    array(
                        "type" => "textfield",
                        "heading" => __( "Plan name", 'gold_domain' ),
                        "param_name" => "nfw_pricing_table_name",
                        "value" => __( "Starter edition", 'gold_domain' ),
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Cost", 'gold_domain' ),
                        "param_name" => "nfw_pricing_table_price",
                        "value" => "30",
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Cost Type", 'gold_domain' ),
                        "param_name" => "nfw_pricing_table_price_type",
                        "value" => "$/mo",
                    ),
                    array(
                        "type" => "textarea",
                        "heading" => __( "Plan features (add one per line)", 'gold_domain' ),
                        "param_name" => "nfw_pricing_table_specifications",
                        "value" => "",
                        "description" => __( "Write the specifications", 'gold_domain' )
                    ),
                    array(
                        "type" => "vc_link",
                        "heading" => __( "Read more button", 'gold_domain' ),
                        "param_name" => "nfw_pricing_table_link",
                        "description" => __( "Specify an optional link pointing to a details page", 'gold_domain' )
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Plan Color', 'gold_domain' ),
                        'param_name' => 'nfw_pricing_table_color',
                        'value' => array(
                            __( 'Default', 'gold_domain' ) => 'accent-color-1',
                            __( 'Color 2', 'gold_domain' ) => 'accent-color-2',
                            __( 'Color 3', 'gold_domain' ) => 'accent-color-3',
                            __( 'Color 4', 'gold_domain' ) => 'accent-color-4'
                        ),
                        'description' => __( 'Specify the color of the plan', 'gold_domain' )
                    )
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM PORTFOLIO ASYMMETRIC ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function nfw_portfolio_func( $atts ){
    extract( shortcode_atts( array(
        'nfw_portfolio_max_number' => '1',
        'nfw_portfolio_loadmore' => '',
        'nfw_portfolio_load_number' => '1',
        'nfw_portfolio_loadmore_text' => __( "Load More", 'gold_domain' ),
        'nfw_portfolio_filter_disable' => ''
                    ), $atts ) );

    $type = 'portfolio';
    // Numeric validation
    if( preg_match( "/[^0-9]/", $nfw_portfolio_max_number ) ){
        $nfw_portfolio_max_number = 1;
    }
    if( preg_match( "/[^0-9]/", $nfw_portfolio_load_number ) ){
        $nfw_portfolio_load_number = 1;
    }
    $args = array(
        'post_type' => $type,
        'post_status' => 'publish',
        'posts_per_page' => $nfw_portfolio_max_number,
        'ignore_sticky_posts' => 1);

    $loop = new WP_Query( $args );
    $output = '';

    $count = 0;
    $category_name = array();
    $categories_index = array();

    $portfolio_container_id = esc_attr( uniqid() );
    // Get portfolio categories array
    $terms = get_terms( 'portfolio_categories' );
    $nfw_portfolio_loadmore_text=esc_html($nfw_portfolio_loadmore_text);

    // Composes the portfolio category selection list
    if( $nfw_portfolio_filter_disable != 'yes' ){
        $output = "<div class='nfw-row'>
        <div class='nfw-span12'>
        <div class='portfolio-filter'>
                    <ul>
			<li>
                            <a class='active' data-filter='*' href='#' >All</a>
			</li>";


        if( $terms && !is_wp_error( $terms ) ){
            foreach( $terms as $term ){
                $count++;
                $category_name = esc_html( $term->name );
                $output.="<li>
                         <a href='#' data-filter='.term-{$count}' >{$category_name}</a>
                    </li>";
                $categories_index[$category_name] = $count;
            }
        }

        $output.="</ul>"
                . "</div></div>"
                . "</div><!-- end .portfolio-filter -->";
    }

    $output.= "<ul id='{$portfolio_container_id}' class='portfolio-items'>";

    $item_count = 0;
    while( $loop->have_posts() ){

        $terms_class = "item";
        $loop->the_post();
        $post_id = get_the_ID();

        $post_categories = get_the_terms( $post_id, 'portfolio_categories' );

        $thumbnail_id = get_post_thumbnail_id();

        // Get custom post feature image link, use different image sizes for the asymmetric portfolio
        switch( $item_count ){
            case 0:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_470_480' );
                $item_count = 1;
                break;
            case 1:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_225' );
                $item_count = 2;
                break;
            case 2:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_370_480' );
                $item_count = 3;
                break;
            case 3:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_225' );
                $item_count = 4;
                break;
            case 4:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_570_225' );
                $item_count = 5;
                break;
            case 5:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_480' );
                $item_count = 6;
                break;
            case 6:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_225' );
                $item_count = 7;
                break;
            case 7:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_225' );
                $item_count = 8;
                break;
            case 8:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_225' );
                $item_count = 9;
                break;
            case 9:
                $image_data = wp_get_attachment_image_src( $thumbnail_id, 'nfw_image_270_225' );
                $item_count = 0;
                break;
        }
        $image_url = esc_url( $image_data[0] );

        $post_title = esc_html( get_the_title( $post_id ) );

        $post_link = esc_url( get_post_permalink( $post_id ) );

        $post_date = esc_html( get_the_time( get_option( 'date_format' ) ) );

        if( !empty( $post_categories ) && !empty( $categories_index ) ){
            foreach( $post_categories as $category ){
                $terms_class.=" term-" . esc_attr( $categories_index[$category->name] );
            }
        }
        $view_project = __( 'View Project', 'gold_domain' );
        // Composes the html data for the current loop item and appends it to the output
        $output.="<li class='{$terms_class}'>
                        	
                            <div class='portfolio-item'>
            
                                <div class='portfolio-item-preview'>			
                                
                                    <img src='{$image_url}' alt=''>
                                    
                                    <div class='portfolio-item-overlay'>
                                    
                                        <div class='portfolio-item-overlay-actions'>
                                            
                                            <a class='btn' href='{$post_link}'>{$view_project}</a>
                                            
                                        </div><!-- end .portfolio-item-overlay-actions -->                                                                
                                        
                                        <div class='portfolio-item-description'>
                                
                                            <h4>{$post_title}</h4>
                                            <p>{$post_date}</p>
                                        
                                        </div><!-- end .portfolio-item-description -->
                                        
                                    </div><!-- end .portfolio-item-overlay -->
                                
                                </div><!-- end .portfolio-item-preview -->                                            
                            
                            </div><!-- end .portfolio-item -->
                            
                        </li>";
    }

    $output.="</ul>";

    if( $nfw_portfolio_loadmore == "yes" ){

        // Prepares the load more button and the ajax link
        $total_images_count = $nfw_portfolio_max_number + $nfw_portfolio_load_number;
        $nonce = wp_create_nonce( "nfw_custom_posts_isotope_nonce" );

        $link = esc_url(admin_url( 'admin-ajax.php?action=nfw_custom_posts_isotope'
                . '&count_increment=' . $nfw_portfolio_load_number . ''
                . '&count_total=' . $nfw_portfolio_max_number . ''
                . '&custom_post_type=' . $type . ''
                . '&nonce=' . $nonce ));

        $output.= "<p class='text-center' style='clear:both;'>"
                . "<a class='nfw_load_more_trigger btn btn-large' id='{$portfolio_container_id}loadmore' "
                . "data-nonce='{$nonce}' "
                . "data-count_increment='{$nfw_portfolio_load_number}' "
                . "data-count_total='{$total_images_count}' "
                . "data-custom_post_type='{$type}' "
                . "data-target_container='{$portfolio_container_id}' "
                . "href='{$link}'>{$nfw_portfolio_loadmore_text}</a></p>";
    }
    wp_reset_query();
    return $output;
}

add_shortcode( 'nfw_portfolio', 'nfw_portfolio_func' );
add_action( 'init', 'nfw_integrate_vc_portfolio' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_portfolio(){
    vc_map(
            array(
                "name" => __( "Portfolio Asymmetric", 'gold_domain' ),
                "base" => "nfw_portfolio",
                "category" => __( "Gold Elements", 'gold_domain' ),
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "params" => array(
                    array(
                        "type" => "textfield",
                        "heading" => __( "Number of images", 'gold_domain' ),
                        "param_name" => "nfw_portfolio_max_number",
                        "value" => "1",
                        "description" => __( "Write the maximum number of images", 'gold_domain' )
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Load More button', 'gold_domain' ),
                        'param_name' => 'nfw_portfolio_loadmore',
                        'description' => __( 'Enable/Disable Load More', 'gold_domain' ),
                        'value' => array(__( 'enable', 'gold_domain' ) => 'yes')
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => __( 'Load More Text', 'gold_domain' ),
                        'param_name' => 'nfw_portfolio_loadmore_text',
                        'description' => __( 'Specify the load more text', 'gold_domain' ),
                        'value' => __( "Load More", 'gold_domain' ),
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Number of images to load", 'gold_domain' ),
                        "param_name" => "nfw_portfolio_load_number",
                        "value" => "1",
                        "description" => __( "Write the number of images you wish to load", 'gold_domain' )
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Disable category filter', 'gold_domain' ),
                        'param_name' => 'nfw_portfolio_filter_disable',
                        'value' => array(__( 'disable', 'gold_domain' ) => 'yes')
                    )
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM CLASSIC PORTFOLIO ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function nfw_portfolio_classic_func( $atts ){
    extract( shortcode_atts( array(
        'nfw_portfolio_classic_max_number' => '1'
                    ), $atts ) );

    $type = 'portfolio';
    // Numeric validation
    if( preg_match( "/[^0-9]/", $nfw_portfolio_classic_max_number ) ){
        $nfw_portfolio_classic_max_number = 1;
    }
    $args = array(
        'post_type' => $type,
        'post_status' => 'publish',
        'posts_per_page' => $nfw_portfolio_classic_max_number,
        'ignore_sticky_posts' => 1);

    $loop = new WP_Query( $args );

    $unique = esc_attr( uniqid() );
    $output = "<div id='nfw_{$unique}'>";

    while( $loop->have_posts() ){

        $loop->the_post();
        $post_id = get_the_ID();

        $image_data = wp_get_attachment_image_src( get_post_thumbnail_id(), 'nfw_image_370_380' );
        $image_url = $image_data[0];

        $post_title = esc_html( get_the_title( $post_id ) );

        $post_link = esc_url( get_post_permalink( $post_id ) );

        $post_content = wp_kses_post( apply_filters( "the_content", get_the_excerpt() ) );

        $post_date = esc_html( get_the_time( get_option( 'date_format' ) ) );

        $post_categories = get_the_terms( $post_id, 'portfolio_categories' );
        $categories = '';
        if( !empty( $post_categories ) ){
            foreach( $post_categories as $category ){
                $categories.=' ' . esc_html( $category->name );
            }
        }

        // Composes the html data for the current loop item and appends it to the output
        $output.="<div class='portfolio-item'>
                
                <div class='nfw-row'>
                    <div class='nfw-span4'>
                        
                        <div class='portfolio-item-preview'>
                            
                            <p>
                            	<img src='{$image_url}' alt=''>
                            </p>
                            
                        </div><!-- end .portfolio-item-preview -->
                        
                    </div><!-- end .span4 -->
                    <div class='nfw-span8'>
                        
                      <div class='portfolio-item-description'>
                            
                            <h4>
                                <a href='{$post_link}'>{$post_title}</a>
                            </h4>
                            
                            <p>
                                <span>" . __( 'Date:', 'gold_domain' ) . "</span> {$post_date}<br>
                                <span>" . __( 'Category:', 'gold_domain' ) . "</span>{$categories}
                            </p>
                            
                            {$post_content}
                            
                            <a class='btn' href='{$post_link}'>" . __( 'Read More', 'gold_domain' ) . "</a>
                            
                        </div><!-- end .portfolio-item-description -->
                        
                    </div><!-- end .span8 -->
                </div><!-- end .row -->
                
            </div>";
    }

    wp_reset_query();
    $count_posts = wp_count_posts( 'portfolio' );
    $published_posts = $count_posts->publish;

    $portfolio_container_id = "nfw_{$unique}";

    // Determines the number of pages for classic portfolio
    if( $published_posts % $nfw_portfolio_classic_max_number != 0 ){
        $portfolio_pages = $published_posts / $nfw_portfolio_classic_max_number + 1;
    } else{
        $portfolio_pages = $published_posts / $nfw_portfolio_classic_max_number;
    }

    // Prepares the list and ajax links of each page index
    $total_images_count = $nfw_portfolio_classic_max_number + $nfw_portfolio_classic_max_number;
    $nonce = wp_create_nonce( "nfw_posts_portfolio_classic_nonce" );

    $link = esc_url(admin_url( 'admin-ajax.php?action=nfw_posts_portfolio_classic'
            . '&count_increment=' . $nfw_portfolio_classic_max_number . ''
            . '&count_page=1'
            . '&custom_post_type=' . $type . ''
            . '&nonce=' . $nonce ));

    $output.="</div>
        <div class='nfw-row'>
            	<div class='nfw-span12'>
                
                        <ul class='pagination fixed'>
                            <li id='{$portfolio_container_id}_current' class='current'>
                                <a href='{$link}'>1</a>
                            </li>";

    // Generates each page index                        
    for( $i = 2; $i <= $portfolio_pages; $i++ ){
        $link = esc_url(admin_url( 'admin-ajax.php?action=nfw_posts_portfolio_classic'
                . '&count_increment=' . $nfw_portfolio_classic_max_number . ''
                . '&count_page=' . $i . ''
                . '&custom_post_type=' . $type . ''
                . '&nonce=' . $nonce ));

        $output.="<li><a class='nfw_portfolio_page_trigger' "
                . "data-nonce='{$nonce}' "
                . "data-count_increment='{$nfw_portfolio_classic_max_number}' "
                . "data-count_page='{$i}' "
                . "data-custom_post_type='{$type}' "
                . "data-target_container='{$portfolio_container_id}' "
                . "href='{$link}'>{$i}</a>
                </li>";
    }

    $output.="</ul>               
                </div><!-- end .span12 -->
            </div><!-- end .row -->";

    return $output;
}

add_shortcode( 'nfw_portfolio_classic', 'nfw_portfolio_classic_func' );
add_action( 'init', 'nfw_integrate_vc_portfolio_classic' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_portfolio_classic(){
    vc_map(
            array(
                "name" => __( "Portfolio Classic", 'gold_domain' ),
                "base" => "nfw_portfolio_classic",
                "category" => __( "Square Elements", 'gold_domain' ),
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "params" => array(
                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __( "Number of elements per page", 'gold_domain' ),
                        "param_name" => "nfw_portfolio_classic_max_number",
                        "value" => "1",
                        "description" => __( "Write the maximum number of elements", 'gold_domain' )
                    )
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM PORTFOLIO SYMMETRIC ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function nfw_portfolio_symmetric_func( $atts ){
    extract( shortcode_atts( array(
        'nfw_portfolio_symmetric_max_number' => '1'
                    ), $atts ) );

    $type = 'portfolio';
    // numeric validation
    if( preg_match( "/[^0-9]/", $nfw_portfolio_symmetric_max_number ) ){
        $nfw_portfolio_symmetric_max_number = 1;
    }
    $args = array(
        'post_type' => $type,
        'post_status' => 'publish',
        'posts_per_page' => $nfw_portfolio_symmetric_max_number,
        'ignore_sticky_posts' => 1);

    $loop = new WP_Query( $args );

    $unique = esc_attr( uniqid() );
    $output = "<div class='nfw-row'>
            <div id='nfw_{$unique}' class='nfw_portfolio_symmetric_container'>";

    while( $loop->have_posts() ){

        $loop->the_post();
        $post_id = get_the_ID();

        $image_data = wp_get_attachment_image_src( get_post_thumbnail_id(), 'nfw_image_370_380' );
        $image_url = esc_url( $image_data[0] );

        $post_title = esc_html( get_the_title( $post_id ) );

        $post_link = esc_url( get_post_permalink( $post_id ) );

        $post_date = esc_html( get_the_time( get_option( 'date_format' ) ) );

        // Composes the html data for the current loop item and appends it to the output
        $output.="<div class='nfw-span4'>
                    <div class='portfolio-item'>
            
                        <div class='portfolio-item-preview'>			
                        
                            <img src='{$image_url}' alt=''>
                            
                            <div class='portfolio-item-overlay'>
                            
                                <div class='portfolio-item-overlay-actions'>
                                    
                                    <a class='btn' href='{$post_link}'>" . __( 'View Project', 'gold_domain' ) . "</a>
                                    
                                </div><!-- end .portfolio-item-overlay-actions -->                                                                
                                
                                <div class='portfolio-item-description'>
                        
                                    <h4>{$post_title}</h4>
                                    <p>{$post_date}</p>
                                
                                </div><!-- end .portfolio-item-description -->
                                
                            </div><!-- end .portfolio-item-overlay -->
                        
                        </div><!-- end .portfolio-item-preview -->                                            
                    
                    </div><!-- end .portfolio-item -->
                    </div>";
    }

    wp_reset_query();
    $count_posts = wp_count_posts( 'portfolio' );
    $published_posts = $count_posts->publish;

    $portfolio_container_id = "nfw_{$unique}";

    // Determines the number of pages for classic portfolio
    if( $published_posts % $nfw_portfolio_symmetric_max_number != 0 ){
        $portfolio_pages = $published_posts / $nfw_portfolio_symmetric_max_number + 1;
    } else{
        $portfolio_pages = $published_posts / $nfw_portfolio_symmetric_max_number;
    }

    // Prepares the list and ajax links of each page index
    $total_images_count = $nfw_portfolio_symmetric_max_number + $nfw_portfolio_symmetric_max_number;
    $nonce = wp_create_nonce( "nfw_posts_portfolio_symmetric_nonce" );

    $link = esc_url(admin_url( 'admin-ajax.php?action=nfw_posts_portfolio_symmetric'
            . '&count_increment=' . $nfw_portfolio_symmetric_max_number . ''
            . '&count_page=1'
            . '&custom_post_type=' . $type . ''
            . '&nonce=' . $nonce ));

    $output.="</div></div>
        <div class='nfw-row'>
            	<div class='nfw-span12'>
                
                        <ul class='pagination fixed'>
                            <li id='{$portfolio_container_id}_current' class='current'>
                                <a href='{$link}'>1</a>
                            </li>";

    // Generates each page index                        
    for( $i = 2; $i <= $portfolio_pages; $i++ ){
        $link = esc_url(admin_url( 'admin-ajax.php?action=nfw_posts_portfolio_symmetric'
                . '&count_increment=' . $nfw_portfolio_symmetric_max_number . ''
                . '&count_page=' . $i . ''
                . '&custom_post_type=' . $type . ''
                . '&nonce=' . $nonce ));

        $output.="<li><a class='nfw_portfolio_symmetric_page_trigger' "
                . "data-nonce='{$nonce}' "
                . "data-count_increment='{$nfw_portfolio_symmetric_max_number}' "
                . "data-count_page='{$i}' "
                . "data-custom_post_type='{$type}' "
                . "data-target_container='{$portfolio_container_id}' "
                . "href='{$link}'>{$i}</a>
                </li>";
    }

    $output.="</ul>               
                </div><!-- end .span12 -->
            </div><!-- end .row -->";

    return $output;
}

add_shortcode( 'nfw_portfolio_symmetric', 'nfw_portfolio_symmetric_func' );
add_action( 'init', 'nfw_integrate_vc_portfolio_symmetric' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_portfolio_symmetric(){
    vc_map(
            array(
                "name" => __( "Portfolio Symmetric", 'gold_domain' ),
                "base" => "nfw_portfolio_symmetric",
                "category" => __( "Square Elements", 'gold_domain' ),
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "params" => array(
                    array(
                        "type" => "textfield",
                        "holder" => "div",
                        "class" => "",
                        "heading" => __( "Number of elements per page", 'gold_domain' ),
                        "param_name" => "nfw_portfolio_symmetric_max_number",
                        "value" => "1",
                        "description" => __( "Write the maximum number of elements", 'gold_domain' )
                    )
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM BUTTON FORMATS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function nfw_button_formats_func( $atts ){
    extract( shortcode_atts( array(
        'nfw_button_formats_link' => '',
        'nfw_button_formats_icon' => '',
        'nfw_button_formats_color' => 'accent-color-1',
        'nfw_button_formats_large' => '',
        'nfw_button_formats_center' => ''
                    ), $atts ) );

    $nfw_button_formats_icon = esc_attr( $nfw_button_formats_icon );
    $nfw_button_formats_color = esc_attr( $nfw_button_formats_color );
    $link_details = vc_build_link( $nfw_button_formats_link );
    $newtab = "";

    $button_formats_icon = "";
    if( trim( $nfw_button_formats_icon ) != "" ){
        $button_formats_icon = "<i class='{$nfw_button_formats_icon}'></i>";
    }

    $large = $center_before = $center_after = "";
    if( $nfw_button_formats_large == "large" ){
        $large = " btn-large";
    }

    if( $nfw_button_formats_center == "center" ){
        $center_before = "<div class='text-center'>";
        $center_after = "</div>";
    }

    $link_details['target'] = esc_attr( $link_details['target'] );
    $link_details['url'] = esc_url( $link_details['url'] );
    $link_details['title'] = esc_html( $link_details['title'] );

    if( $link_details['target'] !== '' ){
        $newtab = "target='{$link_details['target']}'";
    }

    return "{$center_before}<a class='btn{$large} {$nfw_button_formats_color}' href='{$link_details['url']}'  {$newtab}>{$button_formats_icon}{$link_details['title']}</a>{$center_after}";
}

add_shortcode( 'nfw_button_formats', 'nfw_button_formats_func' );
add_action( 'init', 'nfw_integrate_vc_button_formats' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_button_formats(){
    vc_map(
            array(
                "name" => __( "Button Formats", 'gold_domain' ),
                "base" => "nfw_button_formats",
                "category" => __( "Gold Elements", 'gold_domain' ),
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "params" => array(
                    array(
                        "type" => "vc_link",
                        "heading" => __( "Link title &amp; URL", 'gold_domain' ),
                        "param_name" => "nfw_button_formats_link",
                        "description" => __( "Specify the link pointing to another page", 'gold_domain' )
                    ),
                    array(
                        "type" => "nfw_icomoon_icons_param",
                        "heading" => __( "Select icon", 'gold_domain' ),
                        "param_name" => "nfw_button_formats_icon",
                        "description" => __( "Select the icon you want", 'gold_domain' )
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Button Color', 'gold_domain' ),
                        'param_name' => 'nfw_button_formats_color',
                        'value' => array(
                            __( 'Default', 'gold_domain' ) => 'accent-color-1',
                            __( 'Color 2', 'gold_domain' ) => 'accent-color-2',
                            __( 'Color 3', 'gold_domain' ) => 'accent-color-3',
                            __( 'Color 4', 'gold_domain' ) => 'accent-color-4'
                        ),
                        'description' => __( 'Specify the color of the button', 'gold_domain' )
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Button Size', 'gold_domain' ),
                        'param_name' => 'nfw_button_formats_large',
                        'value' => array(__( 'Large', 'gold_domain' ) => 'large')
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __( 'Button Position', 'gold_domain' ),
                        'param_name' => 'nfw_button_formats_center',
                        'value' => array(__( 'Center', 'gold_domain' ) => 'center')
                    )
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM TEAM MEMBER ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// the short code function for the team member elements
function nfw_team_member_func( $atts, $content = null ){ // New function parameter $content is added!
    extract( shortcode_atts( array(
        'nfw_team_member_name' => '',
        'nfw_team_member_job' => '',
        'nfw_team_member_image' => '',
        'nfw_team_member_color' => 'accent-color-1',
        'nfw_team_member_pinteres' => '',
        'nfw_team_member_facebook' => '',
        'nfw_team_member_twitter' => '',
        'nfw_team_member_google_plus' => ''
                    ), $atts ) );

    $team_memeber_info = "";
    $content = wp_kses_post( $content );

    if( trim( $content ) != '' ){
        $team_memeber_info = "{$content}";
    }
    $nfw_team_member_name = esc_html( $nfw_team_member_name );
    $nfw_team_member_job = esc_html( $nfw_team_member_job );
    $nfw_team_member_color = esc_attr( $nfw_team_member_color );

    $youtube = $pinteres = $facebook = $twitter = "";

    if( $nfw_team_member_image == null || trim( $nfw_team_member_image ) == '' ){
        $image_url = "";
    } else{
        $nfw_team_member_image = filter_var( $nfw_team_member_image, FILTER_SANITIZE_NUMBER_INT );
        $image_data = wp_get_attachment_image_src( $nfw_team_member_image, 'nfw_image_200_200' );
        $image_url = esc_url( $image_data[0] );
    }

    if( filter_var( $nfw_team_member_google_plus, FILTER_VALIDATE_URL ) ){
        $nfw_team_member_google_plus = esc_url( $nfw_team_member_google_plus );
        $googleplus = "<a class='googleplus-icon social-icon' href='{$nfw_team_member_google_plus}' target='_blank'>
                                <i class='fa fa-google-plus'></i>
                            </a>";
    }

    if( filter_var( $nfw_team_member_pinteres, FILTER_VALIDATE_URL ) ){
        $nfw_team_member_pinteres = esc_url( $nfw_team_member_pinteres );
        $pinteres = "<a class = 'pinterest-icon social-icon' href = '{$nfw_team_member_pinteres}' target='_blank'>
                            <i class = 'fa fa-pinterest'></i>
                        </a>";
    }

    if( filter_var( $nfw_team_member_facebook, FILTER_VALIDATE_URL ) ){
        $nfw_team_member_facebook = esc_url( $nfw_team_member_facebook );
        $facebook = "<a class = 'facebook-icon social-icon' href = '{$nfw_team_member_facebook}' target='_blank'>
                            <i class = 'fa fa-facebook'></i>
                        </a>";
    }

    if( filter_var( $nfw_team_member_twitter, FILTER_VALIDATE_URL ) ){
        $nfw_team_member_twitter = esc_url( $nfw_team_member_twitter );
        $twitter = "<a class = 'twitter-icon social-icon' href = '{$nfw_team_member_twitter}' target='_blank'>
                            <i class = 'fa fa-twitter'></i>
                        </a>";
    }

    $output = "<div class='team-member {$nfw_team_member_color}'>
                    	
                        <span>
                        	<img src='{$image_url}' alt=''>
                        </span>
                        
                        <h2>{$nfw_team_member_name}</h2>
                        <h6>{$nfw_team_member_job}</h6>
                        
                        <p>{$team_memeber_info}</p>
                        
                        <div class='social-media'>
                        
                            {$googleplus}
                            {$pinteres}
                            {$facebook}
                            {$twitter}
                                    
                        </div>
                        
                    </div>";
    return $output;
}

add_shortcode( 'nfw_team_member', 'nfw_team_member_func' );
add_action( 'init', 'nfw_integrate_vc_team_member' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_team_member(){
    vc_map(
            array(
                "name" => __( "Team Member", 'gold_domain' ),
                "base" => "nfw_team_member",
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "category" => __( "Gold Elements", 'gold_domain' ),
                "params" => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => __( 'Image', 'gold_domain' ),
                        'param_name' => 'nfw_team_member_image',
                        'description' => __( 'Add the image of the team member', 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Name", 'gold_domain' ),
                        "param_name" => "nfw_team_member_name",
                        "value" => __( "Specify the name of the member", 'gold_domain' ),
                        "description" => __( "Sample name", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Job Title", 'gold_domain' ),
                        "param_name" => "nfw_team_member_job",
                        "value" => __( "Job Title", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Google Plus", 'gold_domain' ),
                        "param_name" => "nfw_team_member_google_plus",
                        "value" => "",
                        "description" => __( "Add an optional link to Google Plus profile page, leave blank if not available", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Pinteres", 'gold_domain' ),
                        "param_name" => "nfw_team_member_pinteres",
                        "value" => "",
                        "description" => __( "Add an optional link to Pinteres profile page, leave blank if not available", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Facebook", 'gold_domain' ),
                        "param_name" => "nfw_team_member_facebook",
                        "value" => "",
                        "description" => __( "Add an optional link to Facebook profile page, leave blank if not available", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Twitter", 'gold_domain' ),
                        "param_name" => "nfw_team_member_twitter",
                        "value" => "",
                        "description" => __( "Add an optional link to Twitter profile page, leave blank if not available", 'gold_domain' )
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __( 'Color', 'gold_domain' ),
                        'param_name' => 'nfw_team_member_color',
                        'value' => array(
                            __( 'Default', 'gold_domain' ) => 'accent-color-1',
                            __( 'Color 2', 'gold_domain' ) => 'accent-color-2',
                            __( 'Color 3', 'gold_domain' ) => 'accent-color-3',
                            __( 'Color 4', 'gold_domain' ) => 'accent-color-4'
                        ),
                        'description' => __( 'Specify the color', 'gold_domain' )
                    ),
                    array(
                        "type" => "textarea_html",
                        "heading" => __( "Member details", 'gold_domain' ),
                        "param_name" => "content",
                    )
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM TWITTER ELEMENTS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function nfw_twitter_func( $atts ){
    extract( shortcode_atts( array(
        'nfw_twitter_id' => '',
        'nfw_twitter_count' => '',
        'nfw_twitter_link' => ''
                    ), $atts ) );

    $nfw_twitter_id = esc_js( $nfw_twitter_id );

    if( preg_match( "/[^0-9]/", $nfw_twitter_count ) ){
        $nfw_twitter_count = 1;
    }

    $link_details = vc_build_link( $nfw_twitter_link );
    $newtab = "";
    $link_details['target'] = esc_attr( $link_details['target'] );
    $link_details['url'] = esc_url( $link_details['url'] );
    $link_details['title'] = esc_html( $link_details['title'] );

    if( $link_details['target'] !== '' ){
        $newtab = "target='{$link_details['target']}'";
    }

    $unique = 'tweet-' . uniqid();
    $unique_js = esc_js( $unique );
    $unique_at = esc_attr( $unique );
    return "<div class='nfw_twitter_user'><i class='fa fa-twitter'></i><a href='{$link_details['url']}' {$newtab}>{$link_details['title']}</a></div>"
            . "<div class='nfw_twitter_posts' id='{$unique_at}'></div>"
            . "<script type='text/javascript'>if(typeof twitterFetcher != 'undefined'){twitterFetcher.fetch('{$nfw_twitter_id}', '{$unique_js}', {$nfw_twitter_count}, false, false, false);}</script>";
}

add_shortcode( 'nfw_twitter', 'nfw_twitter_func' );

add_action( 'init', 'nfw_integrate_vc_twitter' );

// integrates the custom element in the visual composer
function nfw_integrate_vc_twitter(){
    vc_map(
            array(
                "name" => __( "Twitter Posts", 'gold_domain' ),
                "base" => "nfw_twitter",
                "icon" => get_template_directory_uri() . '/framework/admin/images/icons/theme_options_menu_icon.png',
                "category" => __( "Gold Elements", 'gold_domain' ),
                "params" => array(
                    array(
                        "type" => "vc_link",
                        "heading" => __( "User", 'gold_domain' ),
                        "param_name" => "nfw_twitter_link",
                        "description" => __( "Specify the user and the link", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Twitter ID", 'gold_domain' ),
                        "param_name" => "nfw_twitter_id",
                        "value" => "",
                        "description" => __( "Specify the Twitter ID", 'gold_domain' )
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => __( "Number of tweets", 'gold_domain' ),
                        "param_name" => "nfw_twitter_count",
                        "value" => "",
                        "description" => __( "Specify the number of tweets", 'gold_domain' )
                    )
                )
            )
    );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       VC CUSTOM nfw 
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Creates the full-width row checkbox option
$attributes_full_width = array(
    'type' => 'checkbox',
    'heading' => __( 'Spread full width', 'gold_domain' ),
    'param_name' => 'nfw_row_full_width',
    'description' => __( 'Full Width', 'gold_domain' ),
    'value' => array
        (__( 'Yes', 'gold_domain' ) => 'yes')
);
vc_add_param( 'vc_row', $attributes_full_width );

// Creates the parallax checkbox option
$parallax_checkbox = array(
    'type' => 'checkbox',
    'heading' => __( 'Add parallax effect to this section', 'gold_domain' ),
    'param_name' => 'nfw_row_parallax',
    'description' => __( 'Parallax', 'gold_domain' ),
    'value' => array(__( 'Parallax', 'gold_domain' ) => 'yes')
);
vc_add_param( 'vc_row', $parallax_checkbox );

// Creates the background image option
$row_image = array(
    'type' => 'attach_image',
    'heading' => __( 'Image', 'gold_domain' ),
    'param_name' => 'nfw_row_image',
    'value' => '',
    'description' => __( 'Select image from media library.', 'gold_domain' )
);
vc_add_param( 'vc_row', $row_image );

// Created the full width section container
function vc_theme_before_vc_row( $atts, $content = null ){
    extract( shortcode_atts( array(
        'nfw_row_full_width' => '',
        'nfw_row_parallax' => '',
        'nfw_row_image' => ''
                    ), $atts ) );
    $before = $image_url = "";

    if( trim( $nfw_row_image ) != '' ){
        $image = wp_get_attachment_image_src( $nfw_row_image, 'full' );
        $image_url = esc_url( $image[0] );
    }

    if( $image_url != "" ){
        if( $nfw_row_parallax == 'yes' ){
            // The parallax option was checked and an image was selected
            $before.= "<div class='nfw-full-width-section parallax' style='background-image:url({$image_url});'>
				<div class='nfw-full-width-section-content'>";
        } else{
            // An image was selected as background for the row
            $before.= "<div class='nfw-full-width-section' style='background-image:url({$image_url});'>
            <div class='nfw-full-width-section-content'>";
        }
    } else if( $nfw_row_full_width == 'yes' ){
        // Only the full width option was selected
        $before.= "<div class='nfw-full-width-section'>";
    }

    return $before;
}

// closes the full width section container
function vc_theme_after_vc_row( $atts, $content = null ){
    extract( shortcode_atts( array(
        'nfw_row_full_width' => '',
        'nfw_row_parallax' => '',
        'nfw_row_image' => ''
                    ), $atts ) );

    $after = "";

    if( $nfw_row_image != "" ){
        $after = "</div></div>";
    } else if( $nfw_row_full_width == 'yes' ){
        $after = "</div>";
    }

    return $after;
}

// Changes vc rows and columns classes
function nfw_custom_css_classes_for_vc_row_clumn( $class_string, $tag ){
    if( $tag == 'vc_row' || $tag == 'vc_row_inner' ){
        $class_string = str_replace( 'vc_row-fluid', '', $class_string );
        $class_string = str_replace( 'vc_inner ', 'nfw-row-inner', $class_string );
        $class_string = str_replace( 'wpb_row', 'nfw-row nfw-row-alt', $class_string );
        $class_string = str_replace( 'vc_row ', '', $class_string );
    }
    return $class_string;
}

// Filter to Replace default css class for vc_row shortcode and vc_column
add_filter( 'vc_shortcodes_css_class', 'nfw_custom_css_classes_for_vc_row_clumn', 10, 2 );

$font_attributes = array('type' => 'colorpicker',
    'heading' => __('Font color', 'gold_domain'),
    'param_name' => 'font_color',
    'description' => __('Select a font color for this row', 'gold_domain')
);
vc_add_param('vc_row', $font_attributes);

vc_remove_param( "vc_row", "full_height" );
vc_remove_param( "vc_row", "full_width" );
vc_remove_param( "vc_row", "video_bg" );
vc_remove_param( "vc_row", "parallax" );
vc_remove_param( "vc_row", "el_id" );
vc_remove_param( "vc_row", "content_placement" );
vc_remove_param( "vc_row", "video_bg_url" );
vc_remove_param( "vc_row", "video_bg_parallax" );
vc_remove_param( "vc_row", "parallax_image" );

vc_remove_param( "vc_row", "gap" );
vc_remove_param( "vc_row", "columns_placement" );
vc_remove_param( "vc_row", "equal_height" );
vc_remove_param( "vc_row", "parallax_speed_video" );
vc_remove_param( "vc_row", "parallax_speed_bg" );
// Generates the form inputs related to icomoon icons
function nfw_icomoon_icons_param_settings_field( $settings, $value ){
    $dependency = vc_generate_dependencies_attributes( $settings );
    // Gets the list of icons
    $icons_list = nfw_icons_listing();

    return '<div class="nfw_fa_icons_param_block">
            <div class="nfw_icon_container"><span class="' . esc_attr( $value ) . '" id="nfw_selected_icon"></span></div>
            <input id="nfw_icon_input" name="' . esc_attr( $settings ['param_name'] )
            . '" class="icon_input wpb_vc_param_value wpb-textinput '
            . esc_attr( $settings['param_name'] ) . ' ' . esc_attr( $settings['type'] ) . '_field" type="text" value="'
            . esc_attr( $value ) . '" ' . esc_html( $dependency ) . ' onchange="nfw_selected_icon_display()" />
            <button class="vc_btn vc_btn-primary" onclick="nfw_icon_clear_selection()" >'.__( 'Clear', 'gold_domain' ).'</button>
            <br><div id="nfw_icons_list">'
            . $icons_list
            . '</div></div>';
}

vc_add_shortcode_param( 'nfw_icomoon_icons_param', 'nfw_icomoon_icons_param_settings_field' );

// Generates the form inputs related to font awesome icons
function nfw_fa_icons_param_settings_field( $settings, $value ){
    $dependency = vc_generate_dependencies_attributes( $settings );
    return '<div class="nfw_fa_icons_param_block">
            <i class="' . esc_attr( $value ) . '" id="nfw_selected_icon"></i>
            <input id="nfw_icon_input" name="' . esc_attr( $settings ['param_name'] )
            . '" class="wpb_vc_param_value wpb-textinput '
            . esc_attr( $settings['param_name'] ) . ' ' . esc_attr( $settings['type'] ) . '_field" type="text" value="'
            . esc_attr( $value ) . '" ' . esc_html($dependency) . ' size="10" />
            <br><div id="nfw_icons_list_fa">
            <i class="fa fa-facebook" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-twitter" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-dribbble" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-pinterest" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-google-plus" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-tumblr" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-instagram" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-rss" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-linkedin" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-skype" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-flickr" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-vimeo-square" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-github" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-youtube" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-windows" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-dropbox" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-xing" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-adn" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-android" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-apple" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-behance" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-bitbucket" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-bitcoin" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-codepen" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-css3" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-delicious" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-deviantart" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-digg" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-drupal" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-empire" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-foursquare" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-git" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-gittip" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-hacker-news" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-html5" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-joomla" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-jsfiddle" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-linux" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-maxcdn" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-openid" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-pagelines" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-pied-piper" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-qq" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-rebel" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-reddit" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-renren" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-share" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-slack" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-soundcloud" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-spotify" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-stack-exchange" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-stack-overflow" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-steam" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-stumbleupon" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-tencent-weibo" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-trello" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-vine" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-vk" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-wechat" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-weibo" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-wordpress" onclick="nfw_icon_change(this)"></i>
            <i class="fa fa-yahoo" onclick="nfw_icon_change(this)"></i>
            </div></div>';
}

vc_add_shortcode_param( 'nfw_fa_icons_param', 'nfw_fa_icons_param_settings_field' );
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                       END OF VISUAL COMPOSER RELATED OPERATIONS
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////