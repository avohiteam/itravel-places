<?php

wp_enqueue_script( 'jquery-ui-accordion' );
$output = $title = $interval = $el_class = $collapsible = $active_tab = '';
//
extract( shortcode_atts( array(
    'title' => '',
    'interval' => 0,
    'el_class' => '',
    'collapsible' => 'no',
    'active_tab' => '1',
    'nfw_accordion_toggles_container_type' => '1'
                ), $atts ) );

$container_class = 'accordion';
if( $nfw_accordion_toggles_container_type == 2 ){
    $container_class = 'toggle';
}
$keyword="nfwaccordiontoggleelementsclass";
$content_alt=preg_replace("/($keyword)/i", $container_class , wpb_js_remove_wpautop($content));

$el_class = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_accordion wpb_content_element ' . $el_class . ' not-column-inherit', $this->settings['base'], $atts );

$output .= "\n\t" . '<div class="' . esc_attr($css_class) . '" data-collapsible=' . esc_attr($collapsible) . ' data-active-tab="' . esc_attr($active_tab) . '">'; //data-interval="'.$interval.'"
$output .= "\n\t\t" . '<div class="' . $container_class . '">';
$output .= wpb_widget_title( array('title' => $title, 'extraclass' => 'wpb_accordion_heading') );

$output .= "\n\t\t\t" . wpb_js_remove_wpautop( $content_alt );
$output .= "\n\t\t" . '</div> ' . $this->endBlockComment( '.wpb_wrapper' );
$output .= "\n\t" . '</div> ' . $this->endBlockComment( '.wpb_accordion' );

echo $output;
