<?php

$output = $title = $interval = $el_class = '';
extract( shortcode_atts( array(
    'title' => '',
    'nfw_vctabs_container_orientation' => '1',
    'nfw_vctabs_icon' => '',
    'nfw_vctabs_color' => 'accent-color-1',
    'interval' => 0,
    'el_class' => ''
                ), $atts ) );

wp_enqueue_script( 'jquery-ui-tabs' );

$el_class = $this->getExtraClass( $el_class );

$element = 'wpb_tabs';
if( 'vc_tour' == $this->shortcode )
    $element = 'wpb_tour';

// Extract tab titles
preg_match_all( '/vc_tab([^\]]+)/i', $content, $matches, PREG_OFFSET_CAPTURE );
$tab_titles = array();
/**
 * vc_tabs
 *
 */
if( isset( $matches[1] ) ){
    $tab_titles = $matches[1];
}
$tabs_nav = '';
$tabs_nav .= '<ul class="tabs-menu fixed">';

foreach( $tab_titles as $tab ){
    $tab_atts = shortcode_parse_atts( $tab[0] );

    if( !isset( $tab_atts['nfw_vctabs_color'] ) ){
        $tab_atts['nfw_vctabs_color'] = '';
    }

    if( !isset( $tab_atts['nfw_vctabs_icon'] ) ){
        $tab_atts['nfw_vctabs_icon'] = $tab_icon = '';
    } else{
        $tab_icon = '<span><i class="' . esc_attr( $tab_atts['nfw_vctabs_icon'] ) . '"></i></span>';
    }

    if( isset( $tab_atts['title'] ) ){
        $tabs_nav .= '<li class="' . esc_attr( $tab_atts['nfw_vctabs_color'] ) . '">'
                . '<a href="#tab-' . ( isset( $tab_atts['tab_id'] ) ? $tab_atts['tab_id'] : sanitize_title( $tab_atts['title'] ) ) . '">'
                . $tab_icon . wp_kses_post( $tab_atts['title'] ) . '</a></li>';
    }
}
$tabs_nav .= '</ul>' . "\n";

$tab_container_class = 'tabs-container';
if( $nfw_vctabs_container_orientation == 2 ){
    $tab_container_class = 'vertical-tabs-container';
}

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, trim( $element . ' wpb_content_element ' . $el_class ), $this->settings['base'], $atts );

$output .= "\n\t" . '<div class="' . esc_attr( $css_class ) . '" data-interval="' . esc_attr( $interval ) . '">';
$output .= "\n\t\t" . '<div class="' . esc_attr( $tab_container_class ) . '">';
$output .= wpb_widget_title( array('title' => $title, 'extraclass' => $element . '_heading') );
$output .= "\n\t\t\t" . $tabs_nav;
$output .= "\n\t\t" . '<div class="tabs">';
$output .= "\n\t\t\t" . wpb_js_remove_wpautop( $content );
if( 'vc_tour' == $this->shortcode ){
    $output .= "\n\t\t\t" . '<div class="wpb_tour_next_prev_nav vc_clearfix"> <span class="wpb_prev_slide"><a href="#prev" title="'
            . __( 'Previous tab', 'gold_domain' ) . '">' . __( 'Previous tab', 'gold_domain' ) . '</a></span> <span class="wpb_next_slide"><a href="#next" title="'
            . __( 'Next tab', 'gold_domain' ) . '">' . __( 'Next tab', 'gold_domain' ) . '</a></span></div>';
}
$output .= "\n\t\t" . '</div> ' . $this->endBlockComment( '.wpb_wrapper' );
$output .= "\n\t" . '</div></div> ' . $this->endBlockComment( $element );

echo $output;
