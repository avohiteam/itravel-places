<?php
/*
  Template Name: Front Page
 */
get_header();
if( have_posts() ){
    // Current page sidebar related meta
    $sidebar_position = get_post_meta( get_the_ID(), 'nfw_sidebar_position', true );

    // checks if a static page was selected as front page
    if( 'page' == get_option( 'show_on_front' ) ){

        // Start the Loop.
        while( have_posts() ){
            the_post();

            $post_id = get_the_ID();

            $page_header_toggle = get_post_meta( $post_id, 'nfw_header_toggle', true );
            if( $page_header_toggle == 'on'):
                // Sets the Page header
                $page_header_title = get_post_meta( $post_id, 'nfw_header_title', true );
                ?>
                <div id="page-header">

                    <div class="nfw-row">
                        <div class="nfw-span12">

                            <h3><?php echo esc_html( $page_header_title ); ?></h3>

                        </div>
                    </div>

                </div>
                <?php
            endif;

            $content = $post->post_content;
            $content_with_html = apply_filters( "the_content", $content );

            $before = $before_content = $after_content = $after = "";

            if( !empty( $sidebar_position ) && $sidebar_position != 'none' ){
                // Prepares the markup for content with sidebar
                $before = '<div class="nfw-row">';
                $before_content = '<div class="nfw-span9">';
                $after_content = $after = '</div>';
            } else if( strpos( $content, '[/vc_row]' ) === false ){
                // Prepares the markup for content with no visual composer elements
                $before = '<div class="nfw-row">';
                $before_content = '<div class="nfw-span12">';
                $after_content = $after = '</div>';
            }

            echo $before;

            if( !empty( $sidebar_position ) ){
                if( $sidebar_position == 'left' ){
                    get_sidebar();
                }
            }

            echo $before_content;
            echo $content_with_html;
            echo $after_content;

            if( !empty( $sidebar_position ) ){
                if( $sidebar_position == 'right' ){
                    get_sidebar();
                }
            }

            echo $after;
        }
    } else{
        get_template_part( 'index' );
    }
} else{
    get_template_part( 'content', 'none' );
}
get_footer();
