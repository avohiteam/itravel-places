<?php
/**
 * Template Name: 404
 * The template for displaying 404 pages (Not Found)
 */
global $nfw_theme_options;

$nfw_404_selection = "";
if( isset( $nfw_theme_options['nfw-404-selection'] ) ){
    $nfw_404_selection = $nfw_theme_options['nfw-404-selection'];
}

// Verifies if a certain a custom page was selected to be displayed as the 404 page
if( $nfw_404_selection == 'default' || $nfw_404_selection == '' || get_permalink( $nfw_404_selection ) == '' ){
get_header();
    // Display the default 404 page content
    ?>
    <br><br>
    <div class="nfw-row">
        <div class="nfw-span12">

            <div class="text-center">
                <header>
                    <h1><?php _e( 'Not Found', 'gold_domain' ); ?></h1>
                </header>
            </div>

            <div class="text-center">
                <p><?php _e( 'Nothing was found at this location.', 'gold_domain' ); ?></p>

                <?php get_search_form(); ?>
            </div>

        </div>
    </div>
    <?php
    get_footer();
} else{
   wp_redirect(get_page_link($milo_404_selection));
    }