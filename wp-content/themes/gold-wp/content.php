<?php
/**
 * The template for displaying posts with no or standard format
 */
?>
<div <?php post_class( "blog-post" ); ?>>

    <div class="blog-post-thumb">

        <?php
        if( has_post_thumbnail() ){
            // Blog sidebar meta
            $page_for_posts = get_option( 'page_for_posts' );
            $sidebar_position = get_post_meta( $page_for_posts, 'nfw_sidebar_position', true );

            if( !empty( $sidebar_position ) && $sidebar_position != 'none' ){
                // Sets a smaller thumbnail if there is a sidebar
                the_post_thumbnail( 'nfw_image_870_430' );
            } else{
                the_post_thumbnail();
            }
        }
        ?>

    </div>

    <div class="blog-post-title">

        <h3>
            <a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( get_the_title() ); ?></a>
        </h3>

    </div>

    <div class="blog-post-info">

        <strong><?php echo esc_html( get_the_time( get_option( 'date_format' ) ) ); ?></strong><br class="hidden-desktop">

        <i class="ifc-ball_point_pen"></i>
        <?php _e( "Posted by", "gold_domain" ); ?>
        <strong><?php the_author_posts_link(); ?></strong><br class="hidden-desktop">

        <i class="ifc-quote"></i>
        <a href="<?php echo esc_url( get_permalink() ); ?>"><?php
            echo esc_html( get_comments_number() );
            _e( " comments", "gold_domain" );
            ?></a><br class="hidden-desktop">


        <?php $categories = get_the_category();

        $max_categ = 4;
        $count = 0;
        $set = 0;

        foreach( $categories as $category ){
            if( $count == 0 && $category->name != '' ){
                echo '<i class="ifc-document"></i><em>';
                $set = 1;
            }
            if( $count < $max_categ ){
				if( $count > 0 ){
					echo ",";
				}
                echo ' <a href="' . esc_url( get_category_link( $category->term_id ) ) . '">';
                echo esc_html( $category->name );
                echo '</a>';
			   $count++;
            }
        }

        if( $set == 1 ){
            echo '</em>';
        } ?>

    </div>

    <p><?php echo wp_kses_post( get_the_excerpt() ); ?></p>

    <a class="btn" href="<?php echo esc_url( get_permalink() ); ?>"> <?php _e( "Read more", "gold_domain" ); ?></a>

</div>