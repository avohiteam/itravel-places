<?php
/**
 * The template for displaying Search Results pages
 *
 */
get_header();
?>
<div id="page-header">

    <div class="nfw-row">
        <div class="nfw-span12">

            <h3><?php printf( __( 'The search for %s returned:', 'gold_domain' ), esc_html( get_search_query() ) ); ?></h3>

        </div>
    </div>

</div>

<div class="nfw-row">
    <?php
    // Gets the blog page related meta for sidebar
    $page_for_posts = get_option( 'page_for_posts' );
    $sidebar_position = get_post_meta( $page_for_posts, 'nfw_sidebar_position', true );

    if( !empty( $sidebar_position ) ){
        if( $sidebar_position == 'left' ){
            get_sidebar();
        }
    }
    if( !empty( $sidebar_position ) && $sidebar_position != 'none' ){
        // There is a sidebar, posts will be fitted in a span9
        if( have_posts() ) :
            ?>
            <div class="nfw-span9">   
                <?php
                while( have_posts() ) : the_post();
                    global $post;
                    if( $post->post_type == 'post' ){
                        get_template_part( 'content', 'post' );
                    } else{
                        get_template_part( 'content', 'alt' );
                    }
                endwhile;
                ?>
            </div>
            <?php
        else :
            ?>
            <div class="nfw-span9">
                <?php
                // If no content, include the "No posts found" template.
                get_template_part( 'content', 'none' );
                ?>
            </div>
        <?php
        endif;
    } else{
        // There is no sidebar, posts will be fitted in a span12
        if( have_posts() ) :
            ?>
            <!-- First Column-->
            <div class="nfw-span12">   
                <?php
                while( have_posts() ) : the_post();
                    global $post;
                    if( $post->post_type == 'post' ){
                        get_template_part( 'content', 'post' );
                    } else{
                        get_template_part( 'content', 'alt' );
                    }
                endwhile;
                ?>
            </div>

            <?php
        else :
            ?>
            <div class="nfw-span12">
                <?php
                // If no content, include the "No posts found" template.
                get_template_part( 'content', 'none' );
                ?>
            </div>
        <?php
        endif;
    }

    if( !empty( $sidebar_position ) ){
        if( $sidebar_position == 'right' ){
            get_sidebar();
        }
    }
    ?>
</div>

<?php
if( have_posts() ) :
    ?>
    <div class="nfw-row">
        <div class="nfw-span12">
            <?php
            if( empty( $sidebar_position ) || $sidebar_position == 'none' || $sidebar_position == 'left' ):
                ?>
                <div class="nfw-span12">
                    <?php
                else :
                    ?>
                    <div class="nfw-span9">  
                        <br class="clear">
                    <?php
                    endif;
                    if( function_exists( "nfw_pagination" ) ) :
                        nfw_pagination();
                    endif;
                    ?>
                </div>
            </div>

            <?php
        endif;
        get_footer();
        