<?php
/**
 * The Header template for nfw theme
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php esc_attr_e( bloginfo( 'charset' ) ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> 
	<meta name="google-site-verification" content="lfN6_zrz8oCm0ifeCd7nIk6JJwq4sRKHyIOuzhvzO68" />

        <?php
        global $nfw_theme_options;
        ?>

        <!-- /// Favicons ////////  -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php
        if( isset( $nfw_theme_options['nfw-appleicon-selector'] ) ){
            echo esc_url( $nfw_theme_options['nfw-appleicon-selector']['url'] );
        }
        ?>">
        <link rel="shortcut icon" href="<?php
        if( isset( $nfw_theme_options['nfw-favicon-selector'] ) ){
            echo esc_url( $nfw_theme_options['nfw-favicon-selector']['url'] );
        }
        ?>"> 

        <?php
        wp_head();
        ?>
	
<!--google analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-100505446-1', 'auto');
		ga('send', 'pageview');
	</script>


    </head>

    <body <?php body_class( 'sticky-header' ); ?>>

        <noscript>
        <div class="alert warning">
            <i class="fa fa-times-circle"></i><?php _e( 'You seem to have Javascript disabled. This website needs javascript in order to function properly!', 'gold_domain' ); ?>
        </div>
        </noscript>

        <!--[if lte IE 8]>
         <div class="alert error">
                You are using an <strong>outdated</strong> browser. Please 
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">upgrade your browser</a> 
            to improve your experience.
                </div>
    <![endif]-->

        <div id="wrap">

            <div id="header">

                <!-- /// HEADER  //////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

                <div class="nfw-row">
                    <div class="nfw-span3">

                        <!-- // Logo // -->
                        <a href="<?php echo esc_url( home_url() ); ?>" id="logo">
                            <img src="<?php
                            if( isset( $nfw_theme_options['nfw-header-logo'] ) ){
                                echo esc_url( $nfw_theme_options['nfw-header-logo']['url'] );
                            }
                            ?>" alt="" class="responsive-img">
                        </a>

                    </div>

                    <div class="nfw-span9">

                        <!-- // Mobile Menu Trigger // -->
                        <a href="#" id="mobile-menu-trigger">
                            <i class="fa fa-bars"></i>
                        </a>

                        <?php
                        if( isset( $nfw_theme_options['nfw-header-search-toggle'] ) ) :
                            if( $nfw_theme_options['nfw-header-search-toggle'] == 1 ):

                                $nfw_search_style = 1;
                                if( isset( $nfw_theme_options['nfw-header-search-style'] ) ){
                                    $nfw_search_style = $nfw_theme_options['nfw-header-search-style'];
                                }

                                if( $nfw_search_style == 1 ):
                                    ?>
                                    <div id="popupsearch" class="nfw-search-popup-box mfp-hide">
                                        <form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="searchpopup" method="get" role="search">
                                            <input id="s" name="s" value="" type="text" placeholder="<?php _e( 'To search, click and type', 'gold_domain' ); ?>" autofocus>
                                            <input class="btn accent-color-1" id="searchsubmit_second" type="submit" value="<?php _e( 'Search', 'gold_domain' ); ?>">
                                        </form>
                                    </div>

                                    <a id="nfw-search-button" class="open-popup-search" href="#popupsearch"></a>

                                <?php else : ?>
                                    <!-- // Custom search // -->
                                    <form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="custom-search-form" id="custom-search-form" method="get" role="search">
                                        <input id="s" name="s" value="" type="text" autofocus>
                                    </form>

                                    <a id="custom-search-button" href="#"></a>
                                <?php
                                endif;
                            endif;
                        endif;

                        // Wordpress navigation menu
                        if( has_nav_menu( 'nfw-gold-menu' ) ){
                            wp_nav_menu( array(
                                'theme_location' => 'nfw-gold-menu',
                                'container' => false,
                                'fallback_cb' => false,
                                'items_wrap' => '<ul id="menu" class="sf-menu fixed">%3$s</ul>',
                                'walker' => new nfw_Nav_Menu_Walker()
                                    )
                            );
                        }
                        ?>

                    </div>
                </div>	

                <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

            </div>
            <div id="content">