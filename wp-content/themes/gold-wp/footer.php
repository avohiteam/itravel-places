<?php
/**
 * The template for displaying the footer
 */
?>
</div><!-- content / End -->

<!-- /// FOOTER     ///////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- Footer Start -->
<div id="footer">
    <div class="nfw-full-width-section">  
  <div class="nfw-row nfw-row-alt  vc_custom_1416830039843" style=" margin-bottom: 0px !important; padding-top: 30px !important; background-color: #f3e686 !important;">
    <div class="nfw-span12 ">
      <div class="wpb_wrapper">
        <div class="nfw-row nfw-row-alt nfw-row-inner">
          <div class="nfw-span8 ">
            <div class="wpb_wrapper">

              <div class="wpb_text_column wpb_content_element ">
                <div class="wpb_wrapper">
                  <h2>Do You Need More Info? Get in Touch</h2>
                  <p>There’s nothing we like more than someone who has as much enthusiasm as us! Give us a call and we’ll tell you exactly how our products can fit into your requirement.</p>
                  <h2><i class="ifc-phone1"></i> +91 9900250883</h2>

                </div> 
              </div> 
            </div> 
          </div> 

          <div class="nfw-span4  vc_custom_1416829940103 margin_topng_yellowfotr">
            <div class="wpb_wrapper">

              <div class="wpb_single_image wpb_content_element vc_custom_1416319450742 vc_align_right">
                <div class="wpb_wrapper">

                  <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="255" height="280" src="http://fomax.co.in/itravel/wp-content/uploads/2017/06/getin.png" class="vc_single_image-img attachment-full" alt=""></div>
                </div> 
              </div> 
            </div> 
          </div> 
        </div>
      </div> 
    </div> 
  </div>
</div> 
    <div class="footer-top">

        <div class="nfw-row">

            <?php
            global $nfw_theme_options;

            // Footer layout options
            $nfw_footer_top_layout = $nfw_footer_middle_switch = $nfw_footer_middle_layout = $nfw_footer_bottom_layout = "";
            if( isset( $nfw_theme_options['nfw-footer-top-layout'] ) ){
                $nfw_footer_top_layout = $nfw_theme_options['nfw-footer-top-layout'];
            }
            if( isset( $nfw_theme_options['nfw-footer-middle-switch'] ) ){
                $nfw_footer_middle_switch = $nfw_theme_options['nfw-footer-middle-switch'];
            }
            if( isset( $nfw_theme_options['nfw-footer-middle-layout'] ) ){
                $nfw_footer_middle_layout = $nfw_theme_options['nfw-footer-middle-layout'];
            }
            if( isset( $nfw_theme_options['nfw-footer-bottom-layout'] ) ){
                $nfw_footer_bottom_layout = $nfw_theme_options['nfw-footer-bottom-layout'];
            }

            // Sets widget area classes for top footer
            $top_layout_class = "nfw-span3";

            if( $nfw_footer_top_layout == 3 ){
                $top_layout_class = "nfw-span4";
            } elseif( $nfw_footer_top_layout == 2 ){
                $top_layout_class = "nfw-span6";
            } elseif( $nfw_footer_top_layout == 1 ){
                $top_layout_class = "nfw-span12";
            }
            ?>

            <?php
            // 1 widget area for top footer
            if( $nfw_footer_top_layout >= 1 ):
                ?>
                <div class="<?php echo $top_layout_class; ?>" id="footer-top-widget-area-1">

                    <?php
                    if( function_exists( 'dynamic_sidebar' ) ){

                        if( is_active_sidebar( 'nfw-footer-top-sidebar-1' ) ){
                            dynamic_sidebar( 'nfw-footer-top-sidebar-1' );
                        }
                    }
                    ?>

                </div>
            <?php endif; ?>

            <?php
            // 2 widget areas for top footer
            if( $nfw_footer_top_layout >= 2 ):
                ?>
                <div class="<?php echo $top_layout_class; ?>" id="footer-top-widget-area-2">

                    <?php
                    if( function_exists( 'dynamic_sidebar' ) ){

                        if( is_active_sidebar( 'nfw-footer-top-sidebar-2' ) ){
                            dynamic_sidebar( 'nfw-footer-top-sidebar-2' );
                        }
                    }
                    ?>

                </div>
            <?php endif; ?>

            <?php
            // 3 widget areas for top footer
            if( $nfw_footer_top_layout >= 3 ):
                ?>
                <div class="<?php echo $top_layout_class; ?>" id="footer-top-widget-area-3">

                    <?php
                    if( function_exists( 'dynamic_sidebar' ) ){

                        if( is_active_sidebar( 'nfw-footer-top-sidebar-3' ) ){
                            dynamic_sidebar( 'nfw-footer-top-sidebar-3' );
                        }
                    }
                    ?>

                </div>
            <?php endif; ?>

            <?php
            // 4 widget areas for top footer
            if( $nfw_footer_top_layout == 4 ):
                ?>
                <div class="<?php echo $top_layout_class; ?>" id="footer-top-widget-area-4">

                    <?php
                    if( function_exists( 'dynamic_sidebar' ) ){

                        if( is_active_sidebar( 'nfw-footer-top-sidebar-4' ) ){
                            dynamic_sidebar( 'nfw-footer-top-sidebar-4' );
                        }
                    }
                    ?>

                </div>
            <?php endif; ?>

        </div>

    </div>

    <?php if( $nfw_footer_middle_switch == 1 ): ?>
        <div class="footer-middle">
            <div class="nfw-row">

                <?php
                // Sets widget area classes for middle footer
                $middle_layout_class = "nfw-span3";

                if( $nfw_footer_middle_layout == 3 ){
                    $middle_layout_class = "nfw-span4";
                } elseif( $nfw_footer_middle_layout == 2 ){
                    $middle_layout_class = "nfw-span6";
                } elseif( $nfw_footer_middle_layout == 1 ){
                    $middle_layout_class = "nfw-span12";
                }
                ?>

                <?php
                // 1 widget area for middle footer
                if( $nfw_footer_middle_layout >= 1 ):
                    ?>
                    <div class="<?php echo $middle_layout_class; ?>" id="footer-middle-widget-area-1">

                        <?php
                        if( function_exists( 'dynamic_sidebar' ) ){
                            if( is_active_sidebar( 'nfw-footer-middle-sidebar-1' ) ){
                                dynamic_sidebar( 'nfw-footer-middle-sidebar-1' );
                            }
                        }
                        ?>

                    </div>
                <?php endif; ?>

                <?php
                // 2 widget areas for middle footer
                if( $nfw_footer_middle_layout >= 2 ):
                    ?>
                    <div class="<?php echo $middle_layout_class; ?>" id="footer-middle-widget-area-2">

                        <?php
                        if( function_exists( 'dynamic_sidebar' ) ){

                            if( is_active_sidebar( 'nfw-footer-middle-sidebar-2' ) ){
                                dynamic_sidebar( 'nfw-footer-middle-sidebar-2' );
                            }
                        }
                        ?>

                    </div>
                <?php endif; ?>

                <?php
                // 3 widget areas for middle footer
                if( $nfw_footer_middle_layout >= 3 ):
                    ?>
                    <div class="<?php echo $middle_layout_class; ?>" id="footer-middle-widget-area-3">

                        <?php
                        if( function_exists( 'dynamic_sidebar' ) ){

                            if( is_active_sidebar( 'nfw-footer-middle-sidebar-3' ) ){
                                dynamic_sidebar( 'nfw-footer-middle-sidebar-3' );
                            }
                        }
                        ?>

                    </div>
                <?php endif; ?>

                <?php
                // 4 widget areas for middle footer
                if( $nfw_footer_middle_layout == 4 ):
                    ?>
                    <div class="<?php echo $middle_layout_class; ?>" id="footer-middle-widget-area-4">

                        <?php
                        if( function_exists( 'dynamic_sidebar' ) ){

                            if( is_active_sidebar( 'nfw-footer-middle-sidebar-4' ) ){
                                dynamic_sidebar( 'nfw-footer-middle-sidebar-4' );
                            }
                        }
                        ?>

                    </div>
                <?php endif; ?>

            </div>
        </div>
    <?php endif; ?>

    <div class="footer-bottom">
        <div class="nfw-row">

            <?php
            // Sets widget area classes for bottom footer
            $bottom_layout_class = "nfw-span3";

            if( $nfw_footer_bottom_layout == 3 ){
                $bottom_layout_class = "nfw-span4";
            } elseif( $nfw_footer_bottom_layout == 2 ){
                $bottom_layout_class = "nfw-span6";
            } elseif( $nfw_footer_bottom_layout == 1 ){
                $bottom_layout_class = "nfw-span12";
            }
            ?>

            <?php
            // 1 widget area for bottom footer
            if( $nfw_footer_bottom_layout >= 1 ):
                ?>
                <div class="<?php echo $bottom_layout_class; ?>" id="footer-bottom-widget-area-1">

                    <?php
                    if( function_exists( 'dynamic_sidebar' ) ){
                        if( is_active_sidebar( 'nfw-footer-bottom-sidebar-1' ) ){
                            dynamic_sidebar( 'nfw-footer-bottom-sidebar-1' );
                        }
                    }
                    ?>

                </div>
            <?php endif; ?>

            <?php
            // 2 widget areas for bottom footer
            if( $nfw_footer_bottom_layout >= 2 ):
                ?>
                <div class="<?php echo $bottom_layout_class; ?>" id="footer-bottom-widget-area-2">

                    <?php
                    if( function_exists( 'dynamic_sidebar' ) ){

                        if( is_active_sidebar( 'nfw-footer-bottom-sidebar-2' ) ){
                            dynamic_sidebar( 'nfw-footer-bottom-sidebar-2' );
                        }
                    }
                    ?>

                </div>
            <?php endif; ?>

            <?php
            // 3 widget areas for bottom footer
            if( $nfw_footer_bottom_layout >= 3 ):
                ?>
                <div class="<?php echo $bottom_layout_class; ?>" id="footer-bottom-widget-area-3">

                    <?php
                    if( function_exists( 'dynamic_sidebar' ) ){

                        if( is_active_sidebar( 'nfw-footer-bottom-sidebar-3' ) ){
                            dynamic_sidebar( 'nfw-footer-bottom-sidebar-3' );
                        }
                    }
                    ?>

                </div>
            <?php endif; ?>

            <?php
            // 4 widget areas for bottom footer
            if( $nfw_footer_bottom_layout == 4 ):
                ?>
                <div class="<?php echo $bottom_layout_class; ?>" id="footer-bottom-widget-area-4">

                    <?php
                    if( function_exists( 'dynamic_sidebar' ) ){

                        if( is_active_sidebar( 'nfw-footer-bottom-sidebar-4' ) ){
                            dynamic_sidebar( 'nfw-footer-bottom-sidebar-4' );
                        }
                    }
                    ?>

                </div>
            <?php endif; ?>

        </div>
    </div>

    <!-- //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

</div><!-- end #footer -->

</div><!-- wrapper / End -->

<!-- Back to top navigation -->
<a id="back-to-top" href="#">
    <i class="ifc-up4"></i>
</a>

<?php
wp_footer();
?>

</body>
</html>