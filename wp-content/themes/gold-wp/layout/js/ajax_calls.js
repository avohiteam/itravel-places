'use strict';

// ajax related functions and calls
jQuery(document).ready(function ($) {

    // The ajax call was triggered by a portfolio Load More button
    $(".nfw_load_more_trigger").click(function () {

        // Stores the relevant data from the fields in variables
        var count_increment = $(this).attr("data-count_increment");
        var count_total = $(this).attr("data-count_total");
        var custom_post_type = $(this).attr("data-custom_post_type");
        var target_container = $(this).attr("data-target_container");
        var nonce = $(this).attr("data-nonce");

        $.ajax({
            type: "post",
            dataType: "json",
            url: nfw_ajax.ajaxurl,
            data: {action: "nfw_custom_posts_isotope", count_total: count_total, custom_post_type: custom_post_type, nonce: nonce},
            success: function (response) {
                // Clears the content of the portfolio
                var container = $("#" + target_container);
                // Clear the portfolio content and insert the elements
                container.html('');
                container.html(response.list_elements);
                $("#" + target_container).imagesLoaded(function () {

                    // Reload Isotope for the portfolio
                    jQuery("#" + target_container).isotope('reloadItems').isotope({sortBy: 'original-order'});
                });
            },
            error: function (response) {
                console.log(response);
            }

        });

        // Updates the count of portfolio elements
        count_total = parseInt(count_total, 10) + parseInt(count_increment, 10);
        $(this).attr("data-count_total", count_total);

        return false;
    });


    // The ajax call was triggered by a classic portfolio type
    $(".nfw_portfolio_page_trigger").click(function () {

        // Stores the relevant data from the fields in variables
        var count_increment = $(this).attr("data-count_increment");
        var count_page = $(this).attr("data-count_page");
        var custom_post_type = $(this).attr("data-custom_post_type");
        var target_container = $(this).attr("data-target_container");
        var nonce = $(this).attr("data-nonce");

        // Changes page index class and id
        var current_li_element = document.getElementById(target_container + "_current")
        current_li_element.className = "";
        current_li_element.id = "";

        // Marks the new page index
        var parentElem = this.parentElement;
        parentElem.id = target_container + "_current";
        parentElem.className = "current";

        $.ajax({
            type: "post",
            dataType: "json",
            url: nfw_ajax.ajaxurl,
            data: {action: "nfw_posts_portfolio_classic",
                count_page: count_page,
                count_increment: count_increment,
                custom_post_type: custom_post_type,
                nonce: nonce},
            success: function (response) {
                // Posts the response content
                $("#" + target_container).html(response.list_elements);
            },
            error: function (response) {
                console.log(response);
            }

        });

        return false;
    });

    // The ajax call was triggered by a portfolio that has pagination
    $(".nfw_portfolio_symmetric_page_trigger").click(function () {

        // Stores the relevant data from the fields in variables
        var count_increment = $(this).attr("data-count_increment");
        var count_page = $(this).attr("data-count_page");
        var custom_post_type = $(this).attr("data-custom_post_type");
        var target_container = $(this).attr("data-target_container");
        var nonce = $(this).attr("data-nonce");

        // Changes page index class and id
        var current_li_element = document.getElementById(target_container + "_current")
        current_li_element.className = "";
        current_li_element.id = "";

        // Marks the new page index
        var parentElem = this.parentElement;
        parentElem.id = target_container + "_current";
        parentElem.className = "current";

        $.ajax({
            type: "post",
            dataType: "json",
            url: nfw_ajax.ajaxurl,
            data: {action: "nfw_posts_portfolio_symmetric",
                count_page: count_page,
                count_increment: count_increment,
                custom_post_type: custom_post_type,
                nonce: nonce},
            success: function (response) {
                // Posts the response content
                $("#" + target_container).html(response.list_elements);
            },
            error: function (response) {
                console.log(response);
            }

        });

        return false;
    });

});