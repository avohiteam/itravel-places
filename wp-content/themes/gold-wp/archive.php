<?php
/**
 * The template for displaying Archive pages
 */
get_header();
$post_id = get_option( 'page_for_posts' );
?>
<div id="page-header">

    <div class="nfw-row">
        <div class="nfw-span12">

            <h3><?php
                if( is_day() ) :
                    echo __( 'Viewing posts from : ', 'gold_domain' ) . esc_html( get_the_date() );

                elseif( is_month() ) :
                    echo __( 'Viewing posts from : ', 'gold_domain' ) . esc_html( get_the_date( _x( 'F Y', 'monthly archives date format', 'gold_domain' ) ) );

                elseif( is_year() ) :
                    echo __( 'Viewing posts from : ', 'gold_domain' ) . esc_html( get_the_date( _x( 'Y', 'yearly archives date format', 'gold_domain' ) ) );

                else :
                    _e( 'Archives!', 'gold_domain' );

                endif;
                ?></h3>

        </div>
    </div>

</div>

<div class="nfw-row">
    <?php
    $sidebar_position = get_post_meta( $post_id, 'nfw_sidebar_position', true );

    if( !empty( $sidebar_position ) ){
        if( $sidebar_position == 'left' ){
            get_sidebar();
        }
    }

    if( !empty( $sidebar_position ) && $sidebar_position != 'none' ){
        // There is a sidebar, posts will be fitted in a span9
        if( have_posts() ) :
            ?> 
            <div class="nfw-span9">   
                <?php
                while( have_posts() ) : the_post();
                    get_template_part( 'content', get_post_format() );
                endwhile;
                if( function_exists( "nfw_pagination" ) ){
                    nfw_pagination();
                }
                ?>
            </div>

            <?php
        else :
            ?>
            <div class="nfw-span9">
                <?php
                // If no content, include the "No posts found" template.
                get_template_part( 'content', 'none' );
                ?>
            </div>
        <?php
        endif;
    } else{
        // there is no sidebar, posts will be fitted in a span12
        if( have_posts() ) :
            ?>

            <div class="nfw-span12">   
                <?php
                while( have_posts() ) : the_post();
                    get_template_part( 'content', get_post_format() );
                endwhile;
                if( function_exists( "nfw_pagination" ) ){
                    nfw_pagination();
                }
                ?>
            </div>

            <?php
        else :
            ?>
            <div class="nfw-span12">
                <?php
                // If no content, include the "No posts found" template.
                get_template_part( 'content', 'none' );
                ?>
            </div>
        <?php
        endif;
    }

    if( !empty( $sidebar_position ) ){
        if( $sidebar_position == 'right' ){
            get_sidebar();
        }
    }
    ?>
</div> 
<?php
get_footer();
