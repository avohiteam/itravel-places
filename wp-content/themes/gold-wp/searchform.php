<?php
/*
 * Search form template
 */
?>
<form action="<?php echo esc_url(home_url( '/' )); ?>" class="searchform" id="searchform" method="get" name="searchform">
    <div>
        <label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'gold_domain' ); ?></label> 
        <input name="s" type="text" placeholder="<?php _e( 'enter keyword here', 'gold_domain' ); ?>" autofocus> 
        <input id="searchsubmit" type="submit" value="">
    </div>
</form>
